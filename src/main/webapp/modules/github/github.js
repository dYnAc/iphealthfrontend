App.register.controller('GithubController',function($rootScope, $http, $modal,
		$scope,	$route, $location, toaster,	WebService, Popup, ModuleProvider) {

	$scope.module = {
		title : 'GitHub Module'
	};

	$scope.hideConnect = false;
	$scope.showRepositoryNotificationsContent = false;
	$scope.showRepositoryIssuesContent = false;
	$scope.showCurrentRepository = "";
	$scope.showCurrentOwner = "";
	
	$scope.config = function() {

		var code = getUrlParam("code");
		if (code == '') {
			$scope.hideConnect = false;
			WebService.invokeGETRequest('github/config', {}, function(data) {
				$scope.githubResult = angular
						.fromJson(data);
				if ($scope.githubResult.authCode == "ALREADY_AUTH") {
					window.location = "/#/github/repositories/";
				}
			},
			function(data, status, headers,	config) {
				toaster.pop('error', "Uh oh", "Could not get github config");
			});
		} else {
			$scope.hideConnect = true;
			var params = {
				"authCode" : code
			};
;
			WebService.invokeGETRequest('github/redirect',	params,	function(data) {
				$scope.githubResult = angular.fromJson(data);
				if ($scope.githubResult.accessToken
						.indexOf("Error") > -1) {
					alert("Error " + $scope.githubResult.accessToken);
				} else {
					window.location = "/#/github/repositories/";
				}
			},
			function(data, status, headers, config) {
				toaster.pop('error', "Uh oh", "Could not redirect to github");
			});
		}
	};

	$scope.connect = function() {

		// build the URL for API Authorization
		window.open($scope.githubResult.authUrl, "_self",
				"GitHub", false);
	};

	$scope.repositories = function() {

		WebService.invokeGETRequest('github/repositories', {}, function(data) {
			$scope.githubRepositories = angular
					.fromJson(data);

			if ($scope.githubRepositories.length == 1) {
				if ($scope.githubRepositories[0].name == "NO_AUTH_TOKEN") {
					window.location = "/#/github";
				} else if ($scope.githubRepositories[0].name
						.indexOf("Error") > -1) {
					alert($scope.githubRepositories[0].name);
				}
			}
		},
		function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not get repositories config");
		});
	};
	
	$scope.notifications = function(owner, name) {

		$scope.showRepositoryNotificationsContent = false;
		$scope.showRepositoryIssuesContent = false;
		$scope.showCurrentRepository = '';
		$scope.showCurrentOwner = '';
		
		if(owner == '' || name == '')
			return;
		
		var params = {
				"owner" : owner,
				"name": name
		};
		
		WebService.invokeGETRequest('github/notifications', params, function(data) {
			$scope.githubRepositoryNotifications = angular
					.fromJson(data);

			if($scope.githubRepositoryNotifications.notifications == null){
				toaster.pop('sucess', "Uh oh", "There are no notifications for this repository");
				return;
			}
			
			if ($scope.githubRepositoryNotifications.notifications.length == 1) {
				if ($scope.githubRepositoryNotifications.notifications[0].reason == "NO_AUTH_TOKEN") {
					window.location = "/#/github";
				} else if ($scope.githubRepositoryNotifications.notifications[0].reason
						.indexOf("Error") > -1) {
					alert($scope.githubRepositoryNotifications.notifications[0].reason);
				}
			}
			
			$scope.showRepositoryNotificationsContent = true;
			$scope.showRepositoryIssuesContent = false;
			$scope.showCurrentRepository = name;
			$scope.showCurrentOwner = owner;
		},
		function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not get notifications config");
		});
	};
	
	$scope.issues = function(owner, name) {

		$scope.showRepositoryNotificationsContent = false;
		$scope.showRepositoryIssuesContent = false;
		$scope.showCurrentRepository = '';
		$scope.showCurrentOwner = '';
		
		if(owner == '' || name == '')
			return;
		
		var params = {
				"owner" : owner,
				"name": name
		};
		
		WebService.invokeGETRequest('github/issues', params, function(data) {
			$scope.githubRepositoryIssues = angular
					.fromJson(data);
			
			if($scope.githubRepositoryIssues == null){
				toaster.pop('sucess', "Uh oh", "There are no issues for this repository");
				return;
			}

			if ($scope.githubRepositoryIssues.length == 1) {
				if ($scope.githubRepositoryIssues[0].title == "NO_AUTH_TOKEN") {
					window.location = "/#/github";
				} else if ($scope.githubRepositoryIssues[0].title
						.indexOf("Error") > -1) {
					alert($scope.githubRepositoryIssues[0].title);
				}
			}
			
			$scope.showRepositoryNotificationsContent = false;
			$scope.showRepositoryIssuesContent = true;
			$scope.showCurrentRepository = name;
			$scope.showCurrentOwner = owner;
		},
		function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not get issues config");
		});
	};
	
	$scope.createRepository = function(){
		
		var templateCreateRepository = {
			'size': 'lg',
			'url': 'modules/github/createRepositoryPopUp.html',
			'controller':'GithubPopupController',
			'data': {
				'repository': {
					"name": null,
					"description": null,
					"homepage":null
				},
			},
		};
		
		$scope.popUpView(templateCreateRepository).then(function(result){
			// Refresh data
			$scope.githubRepositories = result;
			if ($scope.githubRepositories.length == 1) {
				if ($scope.githubRepositories[0].name == "NO_AUTH_TOKEN") {
					window.location = "/#/github";
				} else if ($scope.githubRepositories[0].name
						.indexOf("Error") > -1) {
					alert($scope.githubRepositories[0].name);
				}
			}
		});
	};
	
	$scope.createIssue = function(owner, name){
		
		if(owner == '' || name == '')
			return;
		
		var templateCreateIssue = {
			'size': 'lg',
			'url': 'modules/github/createIssuePopUp.html',
			'controller':'GithubPopupController',
			'data': {
				'issue': {
					"owner": owner,
					"name": name,
					"title": null,
					"body": null,
					"assignee":null,
					"edit":false
				},
			},
		};
		
		$scope.popUpView(templateCreateIssue).then(function(result){
			// Refresh data
			$scope.githubRepositoryIssues = result;
			
			if($scope.githubRepositoryIssues == null){
				toaster.pop('sucess', "Uh oh", "There are no issues for this repository");
				return;
			}

			if ($scope.githubRepositoryIssues.length == 1) {
				if ($scope.githubRepositoryIssues[0].title == "NO_AUTH_TOKEN") {
					window.location = "/#/github";
				} else if ($scope.githubRepositoryIssues[0].title
						.indexOf("Error") > -1) {
					alert($scope.githubRepositoryIssues[0].title);
				}
			}
			
			$scope.showRepositoryNotificationsContent = false;
			$scope.showRepositoryIssuesContent = true;
			$scope.showCurrentRepository = name;
			$scope.showCurrentOwner = owner;
		});
	};
	
	$scope.editIssue = function(owner, name, id, title, body, assignee){
		
		if(owner == '' || name == '' || id == '' || title == ''
			|| body == '' || assignee == '')
			return;
		
		var templateCreateIssue = {
			'size': 'lg',
			'url': 'modules/github/createIssuePopUp.html',
			'controller':'GithubPopupController',
			'data': {
				'issue': {
					"owner": owner,
					"name": name,
					"id": id,
					"title": title,
					"body": body,
					"assignee":assignee,
					"edit":true
				},
			},
		};
		
		$scope.popUpView(templateCreateIssue).then(function(result){
			// Refresh data
			$scope.githubRepositoryIssues = result;
			
			if($scope.githubRepositoryIssues == null){
				toaster.pop('sucess', "Uh oh", "There are no issues for this repository");
				return;
			}

			if ($scope.githubRepositoryIssues.length == 1) {
				if ($scope.githubRepositoryIssues[0].title == "NO_AUTH_TOKEN") {
					window.location = "/#/github";
				} else if ($scope.githubRepositoryIssues[0].title
						.indexOf("Error") > -1) {
					alert($scope.githubRepositoryIssues[0].title);
				}
			}
			
			$scope.showRepositoryNotificationsContent = false;
			$scope.showRepositoryIssuesContent = true;
			$scope.showCurrentRepository = name;
			$scope.showCurrentOwner = owner;
		});
	};
	
	$scope.popUpView = function(template){
		var open = $modal.open({
			templateUrl : template['url'],
			controller : template['controller'],
			size:template['size'],
			resolve : {
				data : function() {
					return template['data'];
				}
			}
		});
		
		return open.result;
	};

	// Simple function to read URL parameters
	function getUrlParam(name) {

		name = name.replace(/[\[]/, "\\\[").replace(/[\]]/,
				"\\\]");
		var regexS = "[\\?&]" + name + "=([^&#]*)";
		var regex = new RegExp(regexS);
		var results = regex.exec(window.location.href);

		if (results == null)
			return "";
		else
			return results[1];
	}
});

App.register.controller('GithubPopupController', function($scope, $http, $modalInstance, 
		WebService, toaster, data) {
	
	$scope.repository = data ? data["repository"]: null;
	$scope.issue = data ? data["issue"]: null;
		
	$scope.add = function(addType){		
		var apiName = null;
		var params = null;
		
		switch(addType){
		 	case "repository":
		 		apiName = 'github/createRepository';
		 		
		 		params = {
		 			"name": $scope.repository.name,
		 			"description": $scope.repository.description,
		 			"homepage": $scope.repository.homepage
		 		};
		 		break;
		 	case "issue":
		 		if(!$scope.issue.edit){
		 			apiName = 'github/createIssue';
		 			
		 			params = {
		 				"owner": $scope.issue.owner,
			 			"name": $scope.issue.name,
			 			"title": $scope.issue.title,
			 			"body": $scope.issue.body,
			 			"assignee": $scope.issue.assignee
			 		};
		 		}
		 		else{
		 			apiName = 'github/modifyIssue';
		 		
			 		params = {
		 				"owner": $scope.issue.owner,
			 			"name": $scope.issue.name,
			 			"id": $scope.issue.id,
			 			"title": $scope.issue.title,
			 			"body": $scope.issue.body,
			 			"assignee": $scope.issue.assignee
			 		};
		 		}
		 		break;
		};	
				
		WebService.invokePOSTRequest(apiName, params, function(data) {
			var result = angular.fromJson(data);
			
			toaster.pop('success', "Successful process " + addType);	
			$modalInstance.close(result);
		}, function(data) {
			toaster.pop('error', "Uo, ho!", data);
		});		
	};
	
	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	};
});
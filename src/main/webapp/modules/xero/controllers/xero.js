App.register.controller('XeroApiController', function($q, $http, $filter, $routeParams, $rootScope, $location, $scope, $modal, $timeout, FeatureSettings, Keyboard, Session, safeApply, toaster, Popup, WebService, ScriptLoader, Workflow, ngTableParams) {
	
	
	/*
	 * Change to module id and feature id of the features you want to test out
	 */
	$scope.module = {
			title: "Xero",
			display_invoice: false,
			display_account: false,
			display_bankSummary:false,
			display_conn_config:false,
	};
	
	$scope.displayControl = function(name){
		$scope.module["display_conn_config"] = false;
		$scope.module["display_invoice"] = false;
		$scope.module["display_account"] = false;
		$scope.module["display_bankSummary"] = false;
		$scope.module["display_initial"] = false;
		$scope.module["display_bankTrans"] = false;
		$scope.module[name] = true;
	};
	
	
	$scope.invoice ={"invoiceID": null};
	$scope.accountEnablePay = [];
	$scope.auth = {
			'key':null,
			'secret':null,
	};
	
	$scope.uploadFile = function(){
		var params = new FormData();
		params.append('file', file.files[0]);
		params.append('key', $scope.auth['key']);
		params.append('secret', $scope.auth['secret']);
		
		WebService.invokePOSTRequest('xero/connConfig', params, function(data) {
			toaster.pop('success', "Congras", "Upload successfully!");
		}, function(data) {
			toaster.pop('error', "Time out", "Could not connect to Xero!");
		},
		null,null,null, true);
	};
	
	$scope.getAllBankTrans = function(){
		$scope.bankTrans = [];
		
		WebService.invokePOSTRequest('xero/getAllBankTrans', {}, function(data) {
			var result = angular.fromJson(data)["accounts"]["bankTransactions"];
			
			if (result.length > 0){
				angular.forEach(result, function(theBankTran,index){
					if (theBankTran["status"] == 'AUTHORISED' && (theBankTran["contact"] != null)){
						var singleData = {};
						singleData["date"] = theBankTran["date"].substring(0,10);
						singleData["contact"] = theBankTran["contact"]["name"];
						singleData["reference"] = theBankTran["reference"];
						
						if ( theBankTran["type"] == 'RECEIVE'){
							singleData["receive"] = theBankTran["total"];
						}else{
							singleData["spend"] = theBankTran["total"];
						}
						
						singleData["accountCode"] = theBankTran["bankAccount"]["code"];
						singleData["accountName"] = theBankTran["bankAccount"]["name"];
						singleData["transactionID"] = theBankTran["bankTransactionID"];
						singleData["type"] = "TRANSACTION";
						
						//push
						$scope.bankTrans.push(singleData);
					}
					
				});
				
				//get payments and merge to bankTrans
				$scope.getAllPayments().then(function(){
					//get transfers which is part of bank transactions
					$scope.getAllTransfers().then(function(){
						if ($scope.transfers.length > 0){
							angular.forEach($scope.transfers, function(transfer,index){
								if (transfer["fromBankAccount"]["name"] == "Business Savings Account" ||
									transfer["fromBankAccount"]["name"] == "Business Bank Account"){
									
									var singleData = {};
									
									// set data
									singleData["date"] = transfer["date"].substring(0,10);
									singleData["contact"] = "Bank Transfer to" + transfer["toBankAccount"]["name"];
									
									// Transfer means spent money
									singleData["spend"] = transfer["amount"];
									
									// set account code, bcoz the data passed from Xero exclude this data
									// it's not worth to get the code of account via API
									if (transfer["fromBankAccount"]["name"] == "Business Savings Account"){
										singleData["accountCode"] = '091';
									}else{
										singleData["accountCode"] = '090';
									}
									
									singleData["accountName"] = transfer["fromBankAccount"]["name"];
									singleData["transferID"] = transfer["bankTransferID"];
									singleData["type"] = "TRANSFER";
									
									//push
									$scope.bankTrans.push(singleData);
								}
							});
						}
						
					});
					
					/*
					 * payment is part of bank transactions
					 * push payment into bank transaction array
					 */
					angular.forEach($scope.payments, function(payment,index){
						if (payment["status"] == 'AUTHORISED' && (payment["account"]["code"] =='090' || payment["account"]["code"] == '091')){
							var singleData = {};
							
							singleData["date"] = payment["date"].substring(0,10);
							singleData["contact"] = payment["invoice"]["contact"]["name"];
							singleData["reference"] = payment["reference"];
							
							if (payment["invoice"]["type"] == 'ACCREC'){
								singleData["receive"] = payment["amount"];
							}else{
								singleData["spend"] = payment["amount"];
							}
							
							singleData["accountCode"] = payment["account"]["code"];
							singleData["accountName"] = payment["account"]["name"];
							singleData["paymentID"] = payment["paymentID"];
							singleData["type"] = "PAYMENT";
							
							//push
							$scope.bankTrans.push(singleData);
						}
					});
					
					$scope.displayControl("display_bankTrans");
					
					//data list pagination
					$scope.pagination($scope.bankTrans);
				});
				
			}
			
		}, function(data) {
			toaster.pop('error', "Time out", "Could not connect to Xero!");
		});
		
	};

	$scope.getAllPayments = function(){
		var deferred = $q.defer();
		
		WebService.invokePOSTRequest('xero/getAllPayments', {}, function(data) {
			$scope.payments =  angular.fromJson(data)['payments']['payments'];
			deferred.resolve($scope.payments);
			
		}, function(data) {
			toaster.pop('error', "Time out", "Could not connect to Xero!");
		});
		
		return deferred.promise;
	};
	

	$scope.getAllTransfers = function(){
		var deferred = $q.defer();
		
		WebService.invokePOSTRequest('xero/getAllTransfers', {}, function(data) {
			$scope.transfers=  angular.fromJson(data)["accounts"]["bankTransfers"];
			deferred.resolve($scope.transfers);
		}, function(data) {
			toaster.pop('error', "Time out", "Could not connect to Xero!");
		});
		
		return deferred.promise;
	};
	
	$scope.getSingleBankTran = function(bankTransID){
		var deferred = $q.defer();
		var params = {"bankTransID": bankTransID,};
		
		WebService.invokePOSTRequest('xero/getSingleBankTran', params, function(data) {
			$scope.theBankTran =  angular.fromJson(data);
			deferred.resolve($scope.theBankTran);
		}, function(data) {
			toaster.pop('error', "Time out", "Could not connect to Xero!");
		});
		
		return deferred.promise;
	};
	
	$scope.getSinglePayment = function(paymentID){
		var deferred = $q.defer();
		var params = {"paymentID": paymentID,};
		
		WebService.invokePOSTRequest('xero/getSinglePayments', params, function(data) {
			$scope.thePayment =  angular.fromJson(data)["payments"]["payments"];
			deferred.resolve($scope.thePayment);
		}, function(data) {
			toaster.pop('error', "Time out", "Could not connect to Xero!");
		});
		
		return deferred.promise;
	};
	
	$scope.getSingleTransfer = function(transferID){
		var deferred = $q.defer();
		var params = {"transferID": transferID,};
		
		WebService.invokePOSTRequest('xero/getSingleTransfer', params, function(data) {
			$scope.theTransfer =  angular.fromJson(data);
			deferred.resolve($scope.theTransfer);
		}, function(data) {
			toaster.pop('error', "Time out", "Could not connect to Xero!");
		});
		
		return deferred.promise;
	};
	
	$scope.bankTransEdit = function(type, bankTran){
		switch(type){
			case "addTransfer":
				var templateAddTransfer = {
					'size': null,
					'url': 'modules/xero/bank/XeroAddBankTransferPopUp.html',
					'controller':'ViewInvoiceController',
					'data': {
						'bankTransfer': {
							"FromBankAccount": {"Code": null},
							"ToBankAccount": {"Code": null},
							"Amount":null,
							"Date":null,
							"Reference": null,
						},
					},
				};
				
				$scope.popUpView(templateAddTransfer).then(function(result){
					// Refresh data
					$scope.getAllBankTrans();
				});
				
				break;
			case "add":
				var templateAddTransaction = {
					'size':'lg',
					'url': 'modules/xero/bank/XeroAddBankTransactionPopUp.html',
					'controller':'ViewInvoiceController',
					'data': {
						'bankTransaction': {
							"Type": null,
							"Date": null,
							"Reference": null,
							"LineAmountTypes": null,
							"Total": null,
							"CurrencyCode": 'AUD',
							"bankAccount": {
								"Code": null,
							},
						},
					},
				};
				
				$scope.getAccounts().then(function(){
					templateAddTransaction['data']['accounts'] = $scope.accounts;
					
					$scope.getAllContacts().then(function(){
						templateAddTransaction['data']['contacts'] = $scope.contacts;
						
						$scope.getAllItems().then(function(){
							templateAddTransaction['data']['items'] = $scope.items;
							
							// Pop up a new window
							$scope.popUpView(templateAddTransaction).then(function(result){
								// Refresh data
								$scope.getAllBankTrans();
							});
						});
					});
				});
						
				break;
				
			case "view":
				var templateTrans = {
					'size':'lg',
					'controller':'ViewInvoiceController',
					'data': {
						'payment': null,
						'transaction': null,
						'transfer': null,
					},
					'url': null,
				};
				
				if (bankTran["type"] == "PAYMENT"){
					templateTrans['url'] = 'modules/xero/bank/XeroBankPaymentDetailPopUp.html';
					$scope.getSinglePayment(bankTran['paymentID']).then(function(){
						templateTrans['data']['payment'] = $scope.thePayment;
						// Pop-up window shows the data
						// Have to wait the data retrieve finish
						$scope.popUpView(templateTrans);
					});

				}else if(bankTran["type"] == "TRANSACTION"){
					templateTrans['url'] = 'modules/xero/bank/XeroBankTransactionDetailPopUp.html';
					$scope.getSingleBankTran(bankTran['transactionID']).then(function(){
						templateTrans['data']['transaction'] = $scope.theBankTran;
						// Pop-up window shows the data
						// Have to wait the data retrieve finish
						$scope.popUpView(templateTrans);
					});
						
				}else{
					templateTrans['url'] = 'modules/xero/bank/XeroBankTransferDetailPopUp.html';
					$scope.getSingleTransfer(bankTran['transferID']).then(function(){
						templateTrans['data']['transfer'] = $scope.theTransfer;
						templateTrans['size'] = null;
						// Pop-up window shows the data
						// Have to wait the data retrieve finish
						$scope.popUpView(templateTrans);
					});
						
				}
				// exit after display
				break;
				
			default:
				break;
		};
	};

	$scope.popUpView = function(template){
		var open = $modal.open({
			templateUrl : template['url'],
			controller : template['controller'],
			size:template['size'],
			resolve : {
				data : function() {
					return template['data'];
				}
			}
		});
		
		return open.result;
	};
	
	$scope.connXero = function(){
		var deferred = $q.defer();
		
		WebService.invokePOSTRequest('xero/connXero', {}, function(data) {
			var result =  angular.fromJson(data);
			
			// 001 - the private key file can't be found
			switch(result["responseCode"]){
				case "001":
					toaster.pop('error', "Private Key", result["responseBody"]); 
					$scope.module["title"] = $scope.module["title"] + " configuration";
					$scope.displayControl("display_conn_config");
					break;
			
				case "400":
					$scope.displayControl("display_conn_config");
					break;
			}
			
		});
		
		return deferred.promise;
	};
	
	$scope.getAccounts = function(ifDisplay) {
		var deferred = $q.defer();
		
		WebService.invokePOSTRequest('xero/getAccounts', {}, function(data) {
			$scope.accounts = angular.fromJson(data)["accounts"]["accounts"];
	
			if($scope.accounts.length > 0){
				if (ifDisplay){
					$scope.displayControl("display_account");
					//data list pagination
					$scope.pagination($scope.accounts);
				}
				
				//filter enable payment accounts out
				$scope.accountEnablePay = [];
				angular.forEach($scope.accounts, function(account, index){
					if (account["enablePaymentsToAccount"] == "true"|| account["type"] == "BANK"){
						switch(account["type"]){
							case "BANK":
								account["type"] = "Bank";
								break;
							case "CURRLIAB":
								account["type"] = "Liabilities";
								break;
							case "EQUITY":
								account["type"] = "Equity";
								break;
						}
						$scope.accountEnablePay.push(account);
					}
				});
				
			}
			
			deferred.resolve($scope.accounts);
			
		}, function(data) {
			toaster.pop('error', "Time out", "Could not connect to Xero!");
		});
		
		return deferred.promise;
	};
	
	$scope.accountView = function(account){
		if (account['type'] == 'Bank'){
			var url = 'modules/xero/account/XeroAccountBankDetailPopUp.html';
		}else{
			var url = 'modules/xero/account/XeroAccountOtherDetailPopUp.html';
		}
		
		var templateAccount = {
			'size':'sm',
			'controller':'ViewInvoiceController',
			'data': {
				"theAccount": account,
			},
			
			'url': url,
		};	
		
		$scope.popUpView(templateAccount);
		
	};
	
	$scope.getBankSummary = function() {
		WebService.invokePOSTRequest('xero/getBankSummary', {}, function(data){
			$scope.bankSummary = angular.fromJson(data);
			toaster.pop('error', "Time out", data);
			if($scope.bankSummary.length > 0){
				$scope.displayControl("display_bankSummary");
				
				//$scope.pagination($scope.bankSummary);
			}
		}, function(data) {
			
			toaster.pop('error', "Time out", "Could not retrieve data from Xero!");
		});
	};

	$scope.getCreditNotes = function(){
		var deferred = $q.defer();
		
		WebService.invokePOSTRequest('xero/getAllCreditNotes', {}, function(data) {
			$scope.creditNotes = angular.fromJson(data)['creditNotes']['creditNotes'];
			deferred.resolve($scope.creditNotes);
		}, function(data) {
			toaster.pop('error', "Time out", "Could not retrieve data from Xero!");
		});
		
		return deferred.promise;
	};
	
	$scope.getSingleCreditNotes = function(creditNoteID){
		var deferred = $q.defer();
		
		WebService.invokePOSTRequest('xero/getSingleCreditNote', {'creditNoteID':creditNoteID}, function(data) {
			$scope.theCreditNote = angular.fromJson(data)['creditNotes']['creditNotes'][0];
			deferred.resolve($scope.creditNotes);
		}, function(data) {
			toaster.pop('error', "Time out", "Could not retrieve data from Xero!");
		});
		
		return deferred.promise;
	};
	
	$scope.getInvoices = function() {
		var deferred = $q.defer();
		
		WebService.invokePOSTRequest('xero/getInvoices', {}, function(data) {
			$scope.invoices = angular.fromJson(data)["invoices"]["invoices"];
			
			if ($scope.invoices.length > 0){
				// retrieve creditnotes;
				$scope.getCreditNotes().then(function(){
					//Add into invoices
					angular.forEach($scope.creditNotes, function(creditNote,index){
						$scope.invoices.push(creditNote);
					});
					
					$scope.displayControl("display_invoice");
					
					//data list pagination
					$scope.pagination($scope.invoices);
				});
			} 
			
			deferred.resolve($scope.invoices);
		}, function(data) {
			toaster.pop('error', "Time out", "Could not retrieve data from Xero!");
		});
		
		return deferred.promise;
	};
	
	//update inovice's status
	$scope.updateStatusOfInvoice = function(newStatus,invoice){
		var params = {
				"invoiceID": invoice['invoiceID'], 
				"newStatus": newStatus,
				"creditNoteID": invoice['creditNoteID'],
		};
		
		var apiName = invoice['type'].substring(6,invoice['type'].length) == 'CREDIT' ? 'xero/updateStatusOfCreditNote' : 'xero/updateStatusOfInvoice';
		
		WebService.invokePOSTRequest(apiName, params, function(data) {
			var result = angular.fromJson(data);
			
			//pop up the result
			if (result["responseCode"] == "200"){
				toaster.pop('success', "Successfully updated invoice's status", "Successfully updated invoice's status");
				
				//update status if success post to Xero
				$scope.updataStatusOfInvoiceLocal(newStatus, invoice);
		
				// reload table
				$scope.tableParams.reload();
				
			} else{
				toaster.pop('error', result["responseCode"], result["responseBody"]);
			} 
			
		}, function(data) {
			toaster.pop('error', "Time out", "Could not updated invoice");
		});
	};
	
	$scope.updataStatusOfInvoiceLocal = function(newStatus, invoice){
		toaster.pop('error', "Time out",invoice);
		var going = true;
		var type = invoice['type'].substring(6,invoice['type'].length);
		
		angular.forEach($scope.invoices, function(theInvoice,index){
			if(going){
				
				switch (type) {
					case "CREDIT":
						if(theInvoice["creditNoteID"] == invoice['creditNoteID']){
							$scope.invoices[index]["status"] = newStatus;
							going=false;
						}
						break;
					default:
						if(theInvoice["invoiceID"] == invoice['invoiceID']){
							$scope.invoices[index]["status"] = newStatus;
							going=false;
						}
						break;
				}
			}
		});
	};
	
	$scope.getSingleInvoice = function(invoiceId) {
		var deferred = $q.defer();
		
		WebService.invokePOSTRequest('xero/getSingleInvoice', {'invoiceID': invoiceId}, function(data) {
			$scope.invoice = angular.fromJson(data)["invoices"]["invoices"][0];
			deferred.resolve($scope.invoice);
			
		}, function(data) {
			toaster.pop('error', "Time out", "Could not retrieve data from Xero!");
		});
		
		return deferred.promise;
	};
	
	$scope.getAllContacts = function(){
		var deferred = $q.defer();
		
		WebService.invokePOSTRequest('xero/getAllContacts', {}, function(data) {
			$scope.contacts = angular.fromJson(data)['accounts']['contacts'];
			deferred.resolve($scope.contacts);
			
		}, function(data) {
			toaster.pop('error', "Time out", "Could not retrieve data from Xero!");
		});
		
		return deferred.promise;
	};

	$scope.getAllItems = function(){
		var deferred = $q.defer();
		
		WebService.invokePOSTRequest('xero/getAllItems', {}, function(data) {
			$scope.items = angular.fromJson(data)['accounts']['item'];
			deferred.resolve($scope.items);
			
		}, function(data) {
			toaster.pop('error', "Time out", "Could not retrieve data from Xero!");
		});
		
		return deferred.promise;
	};
	
	$scope.invoiceEdit = function(edit, data){
		switch (edit){
			case "addInvoice":
				var num1 = 0;
				var num2 = 0;

				angular.forEach($scope.invoices, function(invoice,index){
					if (invoice['type'].substring(0,6) == 'ACCREC'){
					
						if (invoice['type'].substring(6,invoice['type'].length) == 'CREDIT'){
							num2 = parseInt(invoice['creditNoteNumber'].substring(3,invoice['creditNoteNumber'].length), 10);
						}else{
							num2 = parseInt(invoice['invoiceNumber'].substring(3,invoice['invoiceNumber'].length), 10);
						}
						
						if(num2 > num1){
							num1 = num2;
						}
					}
				});
				
				var templateAddInvoice = {
					'size':'lg',
					'url': 'modules/xero/invoice/XeroInvoceAddFormPopUp.html',
					'controller':'ViewInvoiceController',
					'data': {
						'invoice': {
							"Type": null,
							"isCredit": 'void',
							"Date": null,
							"DueDate": null,
							"Reference": null,
							"InvoiceNumber": 'ORC' + (num1 + 1),
							"CreditNoteNumber": 'ORC' + (num1 + 1),
							"LineAmountTypes": null,
							"Total": null,
							"CurrencyCode": null,
							"brandingThemeID": null,
						},
					},
				};
				
				$scope.getAccounts().then(function(){
					templateAddInvoice['data']['accounts'] = $scope.accounts;
					
					$scope.getAllContacts().then(function(){
						templateAddInvoice['data']['contacts'] = $scope.contacts;
						
						$scope.getAllItems().then(function(){
							templateAddInvoice['data']['items'] = $scope.items;
							
							// Pop up a new window
							$scope.popUpView(templateAddInvoice).then(function(result){
								// Refresh data
								$scope.getInvoices();
							});
						});
					});
				});
						
				break;
				
			case "view":
				var promise = data['type'].substring(6,data['type'].length) == 'CREDIT' ? $scope.getSingleCreditNotes(data['creditNoteID']) : $scope.getSingleInvoice(data['invoiceID']);
				
				promise.then(function(){
					var templateView = {
						'url': 'modules/xero/invoice/XeroInvoicesDetailPopup.html',
						'controller': 'ViewInvoiceController',
						'size': 'lg',
						'data':{
							'invoice': data['type'].substring(6,data['type'].length) == 'CREDIT' ? $scope.theCreditNote: $scope.invoice,
						}
					};
				
					$scope.popUpView(templateView);
				});	
				break;
			
			case "addPay":
				var templateAddPay = {
					'url': 'modules/xero/invoice/XeroPaymentAddPopup.html',
					'controller': 'ViewInvoiceController',
					'size': null,
					'data':{
						'invoice': data,
						'accountEnablePay':$scope.accountEnablePay,
					}
				};
				
				if ($scope.accountEnablePay.length > 0){
					$scope.popUpView(templateAddPay).then(function(data){
						$scope.updataStatusOfInvoiceLocal('PAID', data);
					});
					
				} else{
					$scope.getAccounts(false).then(function(){
						templateAddPay['data']['accountEnablePay'] = $scope.accountEnablePay;
						$scope.popUpView(templateAddPay).then(function(data){
							$scope.updataStatusOfInvoiceLocal('PAID', data);
						});;
					});
				}
				break;
		}
	};
	
	$scope.viewInvoice = function(){
		var data = {
				'invoice': $scope.invoice,
				'accountEnablePay': $scope.accountEnablePay
		};
		
		$modal.open({
			templateUrl : 'modules/xero/invoice/XeroInvoicesDetailPopup.html',
			controller : 'ViewInvoiceController',
			size:'lg',
			resolve : {
				data : function() {
					return data;
				},
			},
		}).result.then(function() {
			$scope.tableParams.reload();
		}, function() {
		});
	};
	
	$scope.addCreditNote = function(){
		
	};
	
	$scope.pagination = function(data){

		$scope.tableParams = new ngTableParams({
		        page: 1,           // show first page
		        count: 10           // count per page
		}, {
		    	total: data.length, // length of data
		        getData: function($defer, params) {
		        	
		        	// solve nested object filter problem
		        	var filters = {};

		            angular.forEach(params.filter(), function(value, key) {
		                if (key.indexOf('.') === -1) {
		                    filters[key] = value;
		                    return;
		                }

		                var createObjectTree = function (tree, properties, value) {
		                    if (!properties.length) {
		                        return value;
		                    }

		                    var prop = properties.shift();

		                    if (!prop || !/^[a-zA-Z]/.test(prop)) {
		                        throw new Error('invalid nested property name for filter');
		                    }

		                    tree[prop] = createObjectTree({}, properties, value);

		                    return tree;
		                };

		                var filter = createObjectTree({}, key.split('.'), value);

		                angular.extend(filters, filter);
		            });

		            // Sort data
		            var orderedData = params.sorting ? $filter('orderBy')(data, params.orderBy()) : data;
		        	
		            // use build-in angular filter
                    orderedData = params.filter() ? $filter('filter')(orderedData, filters) : orderedData;
                    
                    $scope.dataPiece = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                    
                    params.total(orderedData.length); // set total for recalc pagination
                    $defer.resolve($scope.dataPiece);
                    
		        }
		});
	};
	
});

App.register.controller('ViewInvoiceController', function($scope, $http, $modalInstance, WebService, toaster, data) {
	$scope.display={
			'invoiceTypeOption':true,
			'formHeaderBill':false,
			'formHeaderSale':false,
			'currentDisplay':null,
	};
	
	$scope.displayChange = function(display){
		$scope.display['currentDisplay'] = display ? display : $scope.display['currentDisplay'];
		
		if ($scope.invoice['Type'] && $scope.invoice['isCredit'] != 'void'){
			$scope.display["invoiceTypeOption"] = false;
			$scope.display["formHeaderBill"] = false;
			$scope.display["formHeaderSale"] = false;
			$scope.display[$scope.display['currentDisplay']] = true;
		}
	};

	$scope.invoice = data ? data["invoice"]: null;
	$scope.accountInView = data ? data['accountEnablePay'] : null;
	$scope.accounts = data ? data['accounts']: null;
	$scope.contacts = data ? data['contacts']: null;
	$scope.items = data ? data['items']: null;
	$scope.theAccount = data ? data["theAccount"]: null;
	$scope.bankTransaction = data ? data["bankTransaction"]: null;
	$scope.bankTransfer = data ? data["bankTransfer"]: null;
	
	//for display
	if (data != null && data['transaction'] != null) {
		$scope.bankTransaction = data['transaction']["accounts"]["bankTransactions"][0];
	}
	
	if (data != null && data['payment'] != null){
		$scope.bankPayment = data['payment'][0];
	}
	
	//for display
	if (data != null && data['transfer'] != null){
		$scope.bankTransfer = data['transfer']['accounts']['bankTransfers'][0];
	}
	
	
	if (data != null && data['invoice'] != null && data['payments']){
		var subAmount = 0;
		angular.forEach($scope.invoice['payments']['payments'], function(payment,index){
			subAmount = subAmount + Number(payment['amount']);
		});
		
		$scope.payment = {
				"invoiceID": $scope.invoice["invoiceID"],
				"creditNoteID":$scope.invoice["creditNoteID"],
				"accountID": null,
				"amount": Number(Number($scope.invoice["total"] - subAmount).toFixed(2)),
				"paidDate":new Date(),
				"reference":null,
				"type":$scope.invoice['type'],
		};
	}
	
	$scope.lineItems = [
			{"Description": null},
			{"Description": null},
			{"Description": null},
	];
	
	$scope.bankAccounts = [
	         {"Name":'Business Bank Account', "Code":'090'},
	         {"Name":'Business Savings Account', "Code":'091'},
	];
	
	$scope.bankTransactionTypes = [
	         {"group":'RECEIVE', 'typeCode':'RECEIVE-OVERPAYMENT', 'name':'Overpayment'},
	         {"group":'RECEIVE', 'typeCode':'RECEIVE-PREPAYMENT', 'name':'Prepayment'},
	         {"group":'RECEIVE', 'typeCode':'RECEIVE', 'name':'Direct Payment'},
	         {"group":'SPEND', 'typeCode':'SPEND-OVERPAYMENT', 'name':'Overpayment'},
	         {"group":'SPEND', 'typeCode':'SPEND-PREPAYMENT', 'name':'Prepayment'},
	         {"group":'SPEND', 'typeCode':'SPEND', 'name':'Direct Payment'},
	];
	
	$scope.contact = {"Name": null};
	
	$scope.removeLineItem = function(index){
		$scope.lineItems.splice(index,1);
	};

	$scope.addNewLine = function(){
			var lineItem = {"Description":null};
			$scope.lineItems.push(lineItem);
	};
	
	$scope.add = function(addType){
		var lineItems =[];
		var apiName = null;
		
		switch(addType){
			case "Invoice":
				apiName = 'xero/addInvoice';
		 		break;
		 	case "CreditNote":
		 		apiName = 'xero/addCreditNote';
		 		break;
		 	case "BankTransaction":
		 		apiName = 'xero/addBankTransaction';
		 		break;
		 	case "BankTransfer":
		 		apiName = 'xero/addBankTransfer';
		 		break;
		};
		
		angular.forEach($scope.lineItems, function(lineItem, index){
			if (lineItem["Description"] != null){
				lineItems.push(lineItem);
			}
		});
		
		if ($scope.invoice){
			// if type equals 'ACCPAY', set invoice number to null
			$scope.invoice['InvoiceNumber'] = $scope.invoice['Type'] == 'ACCPAY' ? null : $scope.invoice['InvoiceNumber'];
			// if create credit note, reset type
			$scope.invoice['Type'] = $scope.invoice['isCredit'] == 'CREDIT' ? $scope.invoice['Type'] + $scope.invoice['isCredit']: $scope.invoice['Type'];
		}

		var params = {
			"invoice": JSON.stringify($scope.invoice),
			
			// only bank transaction
			"bankTransaction": JSON.stringify($scope.bankTransaction),
			
			"bankAccount": $scope.bankTransaction ? JSON.stringify($scope.bankTransaction["bankAccount"]): null,
			
			// For both of bank transaction and invoice
			"creditNote": JSON.stringify($scope.invoice),
			"contact": JSON.stringify($scope.contact),
			"lineItems": JSON.stringify(lineItems.length == 0 ? null : lineItems),
			
			// bank transfer
			"bankTransfer": $scope.bankTransfer,
			"fromBankAccount": $scope.bankTransfer ? JSON.stringify($scope.bankTransfer['FromBankAccount']) : null,
			"toBankAccount": $scope.bankTransfer ? JSON.stringify($scope.bankTransfer['ToBankAccount']) : null,
		};
		
		WebService.invokePOSTRequest(apiName, params, function(data) {
			var result = angular.fromJson(data);
			
			toaster.pop('success', "Successfully added payment", "Successfully added new " + addType);	
			$modalInstance.close(result['responseBody']);
		}, function(data) {
			toaster.pop('error', "Uo, ho!", data);
			//$modalInstance.dismiss('cancel');
		});
		
	};
	
	$scope.addPayment = function(){
		WebService.invokePOSTRequest('xero/addPayment',$scope.payment, function(data) {
			var result = angular.fromJson(data);
			
			if(result['responseCode'] == '200'){
				toaster.pop('success', "Successfully added payment", "Successfully added payment");
			}else{
				toaster.pop('error', "Uh oh", result['responseBody']);
			}	
			$modalInstance.close($scope.payment);
			
		}, function(data) {
			toaster.pop('error', "Uh oh", "Could not connect to Xero!");
			$modalInstance.dismiss('cancel');
		}, function() {
		});

	};
	
	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	};
	
	$scope.uploadfile = function(file){
		WebService.invokePOSTRequest('xero/postAttachment', file, function(data) {
			var result = angular.fromJson(data);
			toaster.pop('error', "Uh oh",result);
		}, function(data) {
			toaster.pop('error', "Uh oh", "Could not connect to Xero!");
		}, null, null, null, true);
	};

});
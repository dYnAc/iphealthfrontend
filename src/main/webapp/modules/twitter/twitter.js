App.register.controller('TwitterController', function($rootScope, $http,
		$scope, $route, $location, toaster, WebService, Popup, ModuleProvider) {

	$scope.module = {
		title : 'Twitter Module'
	};

	$scope.hideConnect = false;
	$scope.tweet = {
		status : ""
	};

	$scope.query = {
		params : ""
	};

	$scope.config = function() {

		var token = getUrlParam("token");
		var verifier = getUrlParam("verifier");
		if (token == '' && verifier == '') {
			$scope.hideConnect = false;
			WebService.invokeGETRequest('twitter/config', {}, function(data) {
				$scope.twitterResult = angular.fromJson(data);
				if ($scope.twitterResult.authCode == "ALREADY_AUTH") {
					window.location = "/#/twitter/tweets/";
				}
			}, function(data, status, headers, config) {
				toaster.pop('error', "Uh oh", "Could not get twitter config");
			});
		} else {
			$scope.hideConnect = true;
			var params = {
				"token" : token,
				"verifier" : verifier
			};

			WebService.invokeGETRequest('twitter/redirect', params, function(
					data) {
				$scope.twitterResult = angular.fromJson(data);
				if ($scope.twitterResult.accessToken.indexOf("Error") > -1) {
					alert("Error " + $scope.twitterResult.accessToken);
				} else {
					window.location = "/#/twitter/tweets/";
				}
			}, function(data, status, headers, config) {
				alert('error' + "Uh oh" + "Could not redirect to twitter");
			});
		}
	};

	$scope.connect = function() {

		// build the URL for API Authorization
		window.open($scope.twitterResult.authUrl, "_self", "Twitter", false);
	};

	$scope.tweets = function() {

		WebService.invokeGETRequest('twitter/tweets', {}, function(data) {
			if (data != "" && data != null) {
				$scope.twitterStatuses = angular.fromJson(data);
			} else {
				$scope.twitterStatuses = null;
			}

			if ($scope.twitterStatuses == null) {
				toaster.pop('error', "Uh oh", "No twitter tweets");
			}
		}, function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not get tweets");
		});
	};

	$scope.postTweet = function() {
		if ($scope.tweet.status == '') {
			return;
		}

		var params = {
			"status" : $scope.tweet.status
		};

		WebService.invokePOSTRequest('twitter/tweet', params, function(data) {
			$scope.postTweetStatus = data;
			$scope.tweet.status = "";

			if ($scope.postTweetStatus == "NO_AUTH_TOKEN") {
				window.location = "/#/twitter";
			} else if ($scope.postTweetStatus.indexOf("Error") > -1) {
				alert("Error " + $scope.postTweetStatus);
			} else {
				toaster.pop('Post a tweet sucessful', "Yeah",
						"Your status has been updated succesfully");

				$scope.tweets();
			}
		}, function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not post tweet");
		});
	};
	
	$scope.searchTweets = function() {
		if ($scope.query.params == '') {
			return;
		}

		var re = /(#[a-zA-Z0-9]+)(,\s#[a-zA-Z0-9]+,?)*/g;
		if (!re.test($scope.query.params)) {
			toaster.pop('error', "Uh oh", "Invalid format");
			return;
		}

		var params = {
			"query" : $scope.query.params
		};

		WebService.invokePOSTRequest('twitter/tweetsQuery', params, function(
				data) {
			if (data != "" && data != null) {
				$scope.twitterResults = angular.fromJson(data);
			} else {
				$scope.twitterResults = null;
			}

			if ($scope.twitterResults == null) {
				toaster.pop('error', "Uh oh",
						"Could not get query tweets. Invalid format, "
								+ "or not data available");
			}
		}, function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not search tweets");
		});
	};

	// Simple function to read URL parameters
	function getUrlParam(name) {

		name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
		var regexS = "[\\?&]" + name + "=([^&#]*)";
		var regex = new RegExp(regexS);
		var results = regex.exec(window.location.href);

		if (results == null)
			return "";
		else
			return results[1];
	}
});
App.register.controller('PivotalTrackerController', function($rootScope, $http, $scope, $modal,
		$route, $location, toaster, WebService, Popup, ModuleProvider, ngTableParams, $q) {

	$scope.module = {
		title : 'Pivotal Tracker Module'
	};

	$scope.hideConnect = false;
	$scope.showProjects = true;
	$scope.allStories = true;
	$scope.currentProjectName = "";
	$scope.pivotalTracker = {
		apiKey : ""
	};
	
	$scope.query = {
		id : "",
		params : ""
	};
	
	$scope.config = function() {

		$scope.hideConnect = false;
		WebService.invokeGETRequest('pivotaltracker/config', {}, function(data) {
			$scope.pivotalTrackerResult = data;
			if ($scope.pivotalTrackerResult == "ALREADY_AUTH") {
				window.location = "/#/pivotaltracker/projects";
			}
		}, function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not get pivotal tracker config");
		});
	};

	$scope.connect = function() {

		if($scope.pivotalTracker.apiKey == null || $scope.pivotalTracker.apiKey == ''){			
			return
		}
		
		$scope.hideConnect = true;
		
		var params = {
			"apiKey" : $scope.pivotalTracker.apiKey
		};

		WebService.invokeGETRequest('pivotaltracker/redirect', params, function(data) {
			$scope.pivotalTrackerResult = data;
			if ($scope.pivotalTrackerResult.indexOf("Error") > -1) {
				alert("Error " + $scope.pivotalTrackerResult);
			} else {
				window.location = "/#/pivotaltracker/projects/";
			}
		}, function(data, status, headers, config) {
			toaster.pop('error', "Uh oh",
					"Could not redirect to pivotal tracker");
		});
	};

	$scope.projects = function() {
		
		$scope.showProjects = true;
		WebService.invokeGETRequest('pivotaltracker/projects', {}, function(data) {
			$scope.pivotalProjects = angular.fromJson(data);

			if ($scope.pivotalProjects.length == 0){
				toaster.pop('sucess', "Uh oh", "There are not projects");
				return;
			}
			
			if ($scope.pivotalProjects[0].name != null) {
				if ($scope.pivotalProjects[0].name == "NO_AUTH_TOKEN") {
					window.location = "/#/pivotaltracker";
				} else if ($scope.pivotalProjects[0].name.indexOf("Error") > -1){
					alert("Error " + $scope.pivotalProjects[0].name);
				}
			}
		}, function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not get projects config");
		});
	};
	
	$scope.notifications = function() {
		
		$scope.showProjects = true;
		WebService.invokeGETRequest('pivotaltracker/notifications', {}, function(data) {
			$scope.pivotalNotifications = angular.fromJson(data);

			if ($scope.pivotalNotifications.data.length == 0){
				toaster.pop('sucess', "Uh oh", "There are not notifications at the moment");
				return;
			}
			
			if ($scope.pivotalNotifications.data[0].name != null) {
				if ($scope.pivotalNotifications.data[0].name == "NO_AUTH_TOKEN") {
					window.location = "/#/pivotaltracker";
				} else if ($scope.pivotalNotifications.data[0].name.indexOf("Error") > -1){
					alert("Error " + $scope.pivotalNotifications.data[0].name);
				}
			}
		}, function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not get projects config");
		});
	};
	
	$scope.redirecProjects = function() {
		
		window.location = "/#/pivotaltracker/projects";
	};
	
	$scope.redirecStoriesPerProject = function(id, name) {
		
		window.location = "/#/pivotaltracker/stories?id=" + id + "&name=" + name;
	};
	
	$scope.showAllStories = function() {
		
		window.location = "/#/pivotaltracker/stories";
	};
	
	$scope.stories = function() {

		var id = getUrlParam("id");
		var name = getUrlParam("name");
		
		if(id == '' || id == undefined){
			id = -1;
			name = "All Projects";
			$scope.allStories = true;
		}
		else{
			$scope.allStories = false;
		}
		
		var params = {
			"id" : id
		};
		
		WebService.invokeGETRequest('pivotaltracker/stories', params, function(data) {
			$scope.pivotalStories = angular.fromJson(data);

			if ($scope.pivotalStories.length == 0){
				toaster.pop('sucess', "Uh oh", "There are not stories");
				return;
			}
			
			if ($scope.pivotalStories[0].name != null) {
				if ($scope.pivotalStories[0].name == "NO_AUTH_TOKEN") {
					window.location = "/#/pivotaltracker";
				} else if ($scope.pivotalStories[0].name.indexOf("Error") > -1){
					alert("Error " + $scope.pivotalStories[0].name);
					return;
				}
			}
							
			$scope.currentProjectName = decodeURI(name);
			$scope.currentProjectId = id;
			// data list pagination
			$scope.pagination($scope.pivotalStories);
		}, function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not get stories config");
		});
		
		if($scope.tableParams != undefined){
			if($scope.tableParams.count() == 5)
				$scope.tableParams.count(10);
			else
				$scope.tableParams.count(5);
		}
	};
	
	$scope.queryStories = function(byId) {
		
		if(byId && $scope.query.id == ''){
			$scope.stories();
			return;
		}else if(!byId && $scope.query.params == ''){
			$scope.stories();
			return;
		}
		
		if ($scope.query.id != ''){
			var params = {
				"storyId" : $scope.query.id,
				"projectId" : $scope.currentProjectId
			};
			
			WebService.invokeGETRequest('pivotaltracker/storiesFilterId', params, function(data) {
				$scope.pivotalStories = angular.fromJson(data);
				
				if ($scope.pivotalStories.length == 0){
					toaster.pop('sucess', "Uh oh", "There are not stories with that id");
					return;
				}
				
				if ($scope.pivotalStories[0].name != null) {
					if ($scope.pivotalStories[0].name == "NO_AUTH_TOKEN") {
						window.location = "/#/pivotaltracker";
					} else if ($scope.pivotalStories[0].name.indexOf("Error") > -1){
						alert("Error " + $scope.pivotalStories[0].name);
						return;
					}
				}
								
				// data list pagination
				$scope.pagination($scope.pivotalStories);
			}, function(data, status, headers, config) {
				toaster.pop('error', "Uh oh", "Could not get stories config");
			});
		}
		else if ($scope.query.params != ''){
			var params = {
				"projectId" : $scope.currentProjectId,
				"query" :  $scope.query.params
			};
			
			WebService.invokeGETRequest('pivotaltracker/storiesFilterQuery', params, function(data) {
				$scope.pivotalStories = angular.fromJson(data);

				if ($scope.pivotalStories.length == 0){
					toaster.pop('sucess', "Uh oh", "There are not stories with that filter");
					return;
				}
				
				if ($scope.pivotalStories[0].name != null) {
					if ($scope.pivotalStories[0].name == "NO_AUTH_TOKEN") {
						window.location = "/#/pivotaltracker";
					} else if ($scope.pivotalStories[0].name.indexOf("Error") > -1){
						alert("Error " + $scope.pivotalStories[0].name);
						return;
					}
				}
								
				// data list pagination
				$scope.pagination($scope.pivotalStories);
			}, function(data, status, headers, config) {
				toaster.pop('error', "Uh oh", "Could not get stories config");
			});
		}
				
		if($scope.tableParams.count() == 5)
			$scope.tableParams.count(10);
		else
			$scope.tableParams.count(5);
	};
	
	$scope.showInfo = function(sId, name){
		
		if(sId == '' || name == '' || $scope.currentProjectId == '')
			return;
		
		var templateShowStoryInfo = {
			'size': 'lg',
			'url': 'modules/pivotaltracker/showDetailStoryInfoPopUp.html',
			'controller':'PivotalTrackerPopupController',
			'data': {
				'story': {
					"pId": $scope.currentProjectId,
					"sId": sId,
					"name": name,
					"comment": null,
					"label" : null,
					"task" : null
				},
			},
		};
		
		$scope.popUpView(templateShowStoryInfo).then(function(result){
			$scope.stories();
		});
	};
	
	$scope.deleteStory= function(sId){

		if(sId == undefined)
			return;
		
		params = {
 			"projectId": $scope.currentProjectId,
 			"storyId": sId
 		};
		
		WebService.invokePOSTRequest("pivotaltracker/storyInfoDelete", params, function(data) {
			$scope.pivotalStories = angular.fromJson(data);
			
			if ($scope.pivotalStories.length == 0){
				toaster.pop('sucess', "Uh oh", "There are not stories");
				return;
			}
			
			if ($scope.pivotalStories[0].name != null) {
				if ($scope.pivotalStories[0].name == "NO_AUTH_TOKEN") {
					window.location = "/#/pivotaltracker";
				} else if ($scope.pivotalStories[0].name.indexOf("Error") > -1){
					alert("Error " + $scope.pivotalStories[0].name);
					return;
				}
			}
							
			// data list pagination
			$scope.pagination($scope.pivotalStories);			
			toaster.pop('success', "Successful story deleted");
		}, function(data) {
			toaster.pop('error', "Uo, ho!", data);
		});	
	};
	
	$scope.createStory= function(){

		if($scope.currentProjectId == '')
			return;
		
		var templateCreateStoryInfo = {
			'size': 'lg',
			'url': 'modules/pivotaltracker/createStoryInfoPopUp.html',
			'controller':'PivotalTrackerCreatePopupController',
			'data': {
				'story': {
					"pId": $scope.currentProjectId,					
					"name": null,
					"estimate" : null
				},
			},
		};
		
		$scope.popUpView(templateCreateStoryInfo).then(function(result){
			$scope.pivotalStories = result;
			
			if ($scope.pivotalStories.length == 0){
				toaster.pop('sucess', "Uh oh", "There are not stories");
				return;
			}
			
			if ($scope.pivotalStories[0].name != null) {
				if ($scope.pivotalStories[0].name == "NO_AUTH_TOKEN") {
					window.location = "/#/pivotaltracker";
				} else if ($scope.pivotalStories[0].name.indexOf("Error") > -1){
					alert("Error " + $scope.pivotalStories[0].name);
					return;
				}
			}
							
			// data list pagination
			$scope.pagination($scope.pivotalStories);			
			toaster.pop('success', "Successful story created");
		});
	};
	
	$scope.pagination = function(data) {
		
		$scope.tableParams = new ngTableParams({
			page : 1, // show first page
			count : 5 // count per page
		}, {
			total : data.length, // length of data
			getData : function($defer, params) {

				$scope.dataPiece = data.slice((params.page() - 1)
						* params.count(), params.page() * params.count());

				params.total(data.length); // set total for recalc pagination
				$defer.resolve($scope.dataPiece);
			}
		});
	};
	
	$scope.popUpView = function(template){
		var open = $modal.open({
			templateUrl : template['url'],
			controller : template['controller'],
			size:template['size'],
			resolve : {
				data : function() {
					return template['data'];
				}
			}
		});
		
		return open.result;
	};

	// Simple function to read URL parameters
	function getUrlParam(name) {

		name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
		var regexS = "[\\?&]" + name + "=([^&#]*)";
		var regex = new RegExp(regexS);
		var results = regex.exec(window.location.href);

		if (results == null)
			return "";
		else
			return results[1];
	}
});

App.register.controller('PivotalTrackerPopupController', function($scope, $http, $modalInstance, 
		WebService, toaster, data) {
	
	$scope.story = data ? data["story"]: null;
		
	$scope.initialize = function (type){
		switch(type){
			case "storyInfo":
				params = {
		 			"projectId": $scope.story.pId,
		 			"storyId": $scope.story.sId
		 		};
				
				WebService.invokeGETRequest("pivotaltracker/storyFullInfo", params, function(data) {
					$scope.storyInfo = angular.fromJson(data);
				}, function(data) {
					toaster.pop('error', "Uo, ho!", data);
				});		
				
				break;
		}
	};
	
	$scope.checkDescription = function(data) {
		   
		if(data == '')
			return;
		
		params = {
 			"projectId": $scope.story.pId,
 			"storyId": $scope.story.sId,
 			"description": data
 		};
		
		WebService.invokePOSTRequest("pivotaltracker/storyDescriptionUpdate", params, function(data) {
			$scope.storyInfo.description = data;								
			toaster.pop('success', "Successful description modified");
		}, function(data) {
			toaster.pop('error', "Uo, ho!", data);
		});	
	};
			
	$scope.addLabel= function(){

		if($scope.story.label == '')
			return;
		
		params = {
 			"projectId": $scope.story.pId,
 			"storyId": $scope.story.sId,
 			"label": $scope.story.label 
 		};
		
		WebService.invokePOSTRequest("pivotaltracker/storyLabelAdd", params, function(data) {
			$scope.storyInfo.labels = angular.fromJson(data);
			
			if ($scope.storyInfo.labels.length == 0){
				toaster.pop('sucess', "Uh oh", "There are not labels");
				return;
			}
			
			if ($scope.storyInfo.labels[0].name != null) {
				if ($scope.storyInfo.labels[0].name == "NO_AUTH_TOKEN") {
					window.location = "/#/pivotaltracker";
				} else if ($scope.storyInfo.labels[0].name.indexOf("Error") > -1){
					alert("Error " + $scope.storyInfo.labels[0].name);
					return;
				}
			}
			
			toaster.pop('success', "Successful label added");
			$scope.story.label = "";
		}, function(data) {
			toaster.pop('error', "Uo, ho!", data);
		});	
	};
	
	$scope.checkLabel = function(data, lId) {
		   
		if(data == '' || lId == undefined)
			return;
				
		params = {
 			"projectId": $scope.story.pId,
 			"storyId": $scope.story.sId,
 			"labelId": lId,
 			"label": data
 		};
		
		WebService.invokePOSTRequest("pivotaltracker/storyLabelUpdate", params, function(data) {
			$scope.storyInfo.labels = angular.fromJson(data);
					
			if ($scope.storyInfo.labels.length == 0){
				toaster.pop('sucess', "Uh oh", "There are not labels");
				return;
			}
			
			if ($scope.storyInfo.labels[0].name != null) {
				if ($scope.storyInfo.labels[0].name == "NO_AUTH_TOKEN") {
					window.location = "/#/pivotaltracker";
				} else if ($scope.storyInfo.labels[0].name.indexOf("Error") > -1){
					alert("Error " + $scope.storyInfo.labels[0].name);
					return;
				}
			}
			
			toaster.pop('success', "Successful label updated");
		}, function(data) {
			toaster.pop('error', "Uo, ho!", data);
		});	
	};
	
	$scope.deleteLabel= function(lId){

		if(lId == undefined)
			return;
		
		params = {
 			"projectId": $scope.story.pId,
 			"storyId": $scope.story.sId,
 			"labelId": lId 
 		};
		
		WebService.invokePOSTRequest("pivotaltracker/storyLabelDelete", params, function(data) {
			$scope.storyInfo.labels = angular.fromJson(data);
			
			if ($scope.storyInfo.labels.length == 0){
				toaster.pop('sucess', "Uh oh", "There are not labels");
				return;
			}
			
			if ($scope.storyInfo.labels[0].name != null) {
				if ($scope.storyInfo.labels[0].name == "NO_AUTH_TOKEN") {
					window.location = "/#/pivotaltracker";
				} else if ($scope.storyInfo.labels[0].name.indexOf("Error") > -1){
					alert("Error " + $scope.storyInfo.labels[0].name);
					return;
				}
			}
			
			toaster.pop('success', "Successful label deleted");
		}, function(data) {
			toaster.pop('error', "Uo, ho!", data);
		});	
	};
	
	$scope.addTask= function(){

		if($scope.story.task == '')
			return;
		
		params = {
 			"projectId": $scope.story.pId,
 			"storyId": $scope.story.sId,
 			"description": $scope.story.task
 		};
		
		WebService.invokePOSTRequest("pivotaltracker/storyTaskAdd", params, function(data) {
			$scope.storyInfo.tasks = angular.fromJson(data);
			
			if ($scope.storyInfo.tasks.length == 0){
				toaster.pop('sucess', "Uh oh", "There are not labels");
				return;
			}
			
			if ($scope.storyInfo.tasks[0].name != null) {
				if ($scope.storyInfo.tasks[0].name == "NO_AUTH_TOKEN") {
					window.location = "/#/pivotaltracker";
				} else if ($scope.storyInfo.tasks[0].name.indexOf("Error") > -1){
					alert("Error " + $scope.storyInfo.tasks[0].name);
					return;
				}
			}
			
			toaster.pop('success', "Successful task added");
			$scope.story.task = "";
		}, function(data) {
			toaster.pop('error', "Uo, ho!", data);
		});	
	};
	
	$scope.checkTask = function(data, tId, complete) {
		   
		if(data == '' || tId == undefined || complete == undefined )
			return;
				
		params = {
 			"projectId": $scope.story.pId,
 			"storyId": $scope.story.sId,
 			"taskId": tId,
 			"description": data,
 			"complete": complete
 		};
				
		WebService.invokePOSTRequest("pivotaltracker/storyTaskUpdate", params, function(data) {
			$scope.storyInfo.tasks = angular.fromJson(data);
			
			if ($scope.storyInfo.tasks.length == 0){
				toaster.pop('sucess', "Uh oh", "There are not labels");
				return;
			}
			
			if ($scope.storyInfo.tasks[0].name != null) {
				if ($scope.storyInfo.tasks[0].name == "NO_AUTH_TOKEN") {
					window.location = "/#/pivotaltracker";
				} else if ($scope.storyInfo.tasks[0].name.indexOf("Error") > -1){
					alert("Error " + $scope.storyInfo.tasks[0].name);
					return;
				}
			}
			
			toaster.pop('success', "Successful task updated");
		}, function(data) {
			toaster.pop('error', "Uo, ho!", data);
		});	
	};
	
	$scope.deleteTask= function(tId){

		if(tId == undefined)
			return;
		
		params = {
 			"projectId": $scope.story.pId,
 			"storyId": $scope.story.sId,
 			"taskId": tId 
 		};
		
		WebService.invokePOSTRequest("pivotaltracker/storyTaskDelete", params, function(data) {
			$scope.storyInfo.tasks = angular.fromJson(data);
			
			if ($scope.storyInfo.tasks.length == 0){
				toaster.pop('sucess', "Uh oh", "There are not labels");
				return;
			}
			
			if ($scope.storyInfo.tasks[0].name != null) {
				if ($scope.storyInfo.tasks[0].name == "NO_AUTH_TOKEN") {
					window.location = "/#/pivotaltracker";
				} else if ($scope.storyInfo.tasks[0].name.indexOf("Error") > -1){
					alert("Error " + $scope.storyInfo.tasks[0].name);
					return;
				}
			}
			
			toaster.pop('success', "Successful task deleted");
		}, function(data) {
			toaster.pop('error', "Uo, ho!", data);
		});	
	};
	
	$scope.addComment= function(){

		if($scope.story.comment == '')
			return;
		
		params = {
 			"projectId": $scope.story.pId,
 			"storyId": $scope.story.sId,
 			"comment": $scope.story.comment
 		};
		
		WebService.invokePOSTRequest("pivotaltracker/storyCommentAdd", params, function(data) {
			$scope.storyInfo.comments = angular.fromJson(data);
					
			if ($scope.storyInfo.comments.length == 0){
				toaster.pop('sucess', "Uh oh", "There are not comments");
				return;
			}
			
			if ($scope.storyInfo.comments[0].name != null) {
				if ($scope.storyInfo.comments[0].name == "NO_AUTH_TOKEN") {
					window.location = "/#/pivotaltracker";
				} else if ($scope.storyInfo.comments[0].name.indexOf("Error") > -1){
					alert("Error " + $scope.storyInfo.comments[0].name);
					return;
				}
			}
			
			toaster.pop('success', "Successful comment added");
			$scope.story.comment = "";
		}, function(data) {
			toaster.pop('error', "Uo, ho!", data);
		});	
	};
	
	$scope.checkComment = function(data, cId) {
		   
		if(data == '' || cId == undefined)
			return;
				
		params = {
 			"projectId": $scope.story.pId,
 			"storyId": $scope.story.sId,
 			"commentId": cId,
 			"comment": data
 		};
				
		WebService.invokePOSTRequest("pivotaltracker/storyCommentUpdate", params, function(data) {
			$scope.storyInfo.comments = angular.fromJson(data);
			
			if ($scope.storyInfo.comments.length == 0){
				toaster.pop('sucess', "Uh oh", "There are not comments");
				return;
			}
			
			if ($scope.storyInfo.comments[0].name != null) {
				if ($scope.storyInfo.comments[0].name == "NO_AUTH_TOKEN") {
					window.location = "/#/pivotaltracker";
				} else if ($scope.storyInfo.comments[0].name.indexOf("Error") > -1){
					alert("Error " + $scope.storyInfo.comments[0].name);
					return;
				}
			}
			
			toaster.pop('success', "Successful comment updated");
		}, function(data) {
			toaster.pop('error', "Uo, ho!", data);
		});	
	};	
	
	$scope.deleteComment= function(cId){

		if(cId == undefined)
			return;
		
		params = {
 			"projectId": $scope.story.pId,
 			"storyId": $scope.story.sId,
 			"commentId": cId
 		};
		
		WebService.invokePOSTRequest("pivotaltracker/storyCommentDelete", params, function(data) {
			$scope.storyInfo.comments = angular.fromJson(data);
					
			if ($scope.storyInfo.comments.length == 0){
				toaster.pop('sucess', "Uh oh", "There are not comments");
				return;
			}
			
			if ($scope.storyInfo.comments[0].name != null) {
				if ($scope.storyInfo.comments[0].name == "NO_AUTH_TOKEN") {
					window.location = "/#/pivotaltracker";
				} else if ($scope.storyInfo.comments[0].name.indexOf("Error") > -1){
					alert("Error " + $scope.storyInfo.comments[0].name);
					return;
				}
			}
			
			toaster.pop('success', "Successful comment deleted");
		}, function(data) {
			toaster.pop('error', "Uo, ho!", data);
		});	
	};
		
	$scope.cancel = function() {
		$modalInstance.close("REFRESH");
	};
});

App.register.controller('PivotalTrackerCreatePopupController', function($scope, 
		$http, $modalInstance, WebService, toaster, data) {
	
	$scope.story = data ? data["story"]: null;
		
	$scope.addStory = function(){		
	
 		params = {
 			"projectId": $scope.story.pId,
 			"name": $scope.story.name,
 			"estimate" : $scope.story.estimate
 		};
				
		WebService.invokePOSTRequest("pivotaltracker/storyInfoAdd", params, function(data) {
			var result = angular.fromJson(data);					
			$modalInstance.close(result);
		}, function(data) {
			toaster.pop('error', "Uo, ho!", data);
		});		
	};
	
	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	};
});
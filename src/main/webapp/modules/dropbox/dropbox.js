App.register.controller('DropboxController',function($rootScope, $http, $modal,
		$scope,	$route, $location, toaster,	WebService, Popup, ModuleProvider) {

	$scope.module = {
		title : 'Dropbox Module'
	};

	$scope.hideConnect = false;
	$scope.showCurrentPath = "/";
	$scope.isRoot = true;

	$scope.config = function() {

		var code = getUrlParam("code");
		if (code == '') {
			$scope.hideConnect = false;
			WebService.invokeGETRequest('dropbox/config', {}, function(data) {
				$scope.dropboxResult = angular
						.fromJson(data);
				if ($scope.dropboxResult.authCode == "ALREADY_AUTH") {
					window.location = "/#/dropbox/files/";
				}
			},
			function(data, status, headers,	config) {
				toaster.pop('error', "Uh oh", "Could not get dropbox config");
			});
		} else {
			$scope.hideConnect = true;
			var params = {
				"authCode" : code
			};

			WebService.invokeGETRequest('dropbox/redirect',	params,	function(data) {
				$scope.dropboxResult = angular
						.fromJson(data);
				if ($scope.dropboxResult.accessToken
						.indexOf("Error") > -1) {
					alert("Error " + $scope.dropboxResult.accessToken);
				} else {
					window.location = "/#/dropbox/files/";
				}
			},
			function(data, status, headers, config) {
				toaster.pop('error', "Uh oh", "Could not redirect to dropbox");
			});
		}
	};

	$scope.connect = function() {

		// build the URL for API Authorization
		window.open($scope.dropboxResult.authUrl, "_self",
				"Dropbox", false);
	};

	$scope.files = function(path, isFile) {

		if (path != "" && isFile == false) {
			var params = {
				"path" : path
			};

			WebService.invokeGETRequest('dropbox/files', params, function(data) {
				$scope.dropboxFiles = angular
						.fromJson(data);
				
				if ($scope.dropboxFiles.length == 1) {
					if ($scope.dropboxFiles[0].error == "NO_AUTH_TOKEN") {
						window.location = "/#/dropbox";
					} else if ($scope.dropboxFiles[0].error
							.indexOf("Error") > -1) {
						alert($scope.dropboxFiles[0].error);
						return;
					}
				}

				$scope.showCurrentPath = decodeURIComponent(path);

				if ($scope.showCurrentPath != "/") {
					$scope.isRoot = false;
				} else if ($scope.showCurrentPath == "/") {
					$scope.isRoot = true;
				}
			},
			function(data, status, headers, config) {
				toaster.pop('error', "Uh oh", "Could not get files config");
			});
		} else if (path != "" && isFile == true) {
			var params = {
				"path" : path
			};

			WebService.invokeGETRequest('dropbox/file',	params,	function(data) {
				var a = window.document.createElement('a');
				a.href = window.URL.createObjectURL(new Blob(
						[ data ],
						{
							type : 'application/octet-stream'
						}));

				var name = decodeURIComponent(path);
				var index = name.lastIndexOf("/");

				name = name.substring(index + 1);

				a.download = name;

				// Append anchor to body.
				document.body.appendChild(a);
				a.click();

				// Remove anchor from body
				document.body.removeChild(a);

				$scope.files($scope.showCurrentPath, false);

			},
			function(data, status, headers,	config) {
				toaster.pop('error', "Uh oh", "Could not get file config");
			}, null, null, "blob");
		}
	};
	
	$scope.back = function(){
		var path = getPreviousPath($scope.showCurrentPath);
		$scope.files(path, false);
	};

	$scope.continueFileUpload = function() {
		var formData = new FormData();
		formData.append("file", file.files[0]);
		formData.append("path", $scope.showCurrentPath);

		WebService.invokePOSTRequestFile('dropbox/file', formData, function(data) {
			$scope.fileUploadStatus = data;

			if ($scope.fileUploadStatus == "NO_AUTH_TOKEN") {
				window.location = "/#/dropbox";
			} else if ($scope.fileUploadStatus
					.indexOf("Error") > -1) {
				alert($scope.fileUploadStatus);
			} else {
				toaster.pop('Upload sucessful', "Yeah",
						    "The file has been upload succesfully");

				$scope.files($scope.showCurrentPath, false);
			}
		},
		function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not upload file config");
		});
	};

	$scope.deleteFile = function(path) {

		if (path != "") {
			Popup.show('Delete File or Folder',
						'Are you sure you want to delete this file or folder?',
						'yesno')
			.then(function(result) {
				if (result == 'yes') {

					var params = {
						"path" : path
					};

					WebService.invokePOSTRequest('dropbox/deleteFileFolder', params, function(data) {
						$scope.fileDeleteStatus = data;

						if ($scope.fileDeleteStatus == "NO_AUTH_TOKEN") {
							window.location = "/#/dropbox";
						} else if ($scope.fileDeleteStatus
								.indexOf("Error") > -1) {
							alert($scope.fileDeleteStatus);
						} else {
							toaster.pop('Delete sucessful',
										"Yeah",
										"The file has been deleted succesfully");

							$scope.files($scope.showCurrentPath, false);
						}
					},
					function(data, status, headers,	config) {
						toaster.pop('error', "Uh oh",
									"Could not delete file config");
					});
				}
			});
		}
	};

	// Get previous path
	function getPreviousPath(path) {
		var previousPath = decodeURIComponent(path);
		var index = previousPath.lastIndexOf("/");

		previousPath = previousPath.replace(previousPath
				.substring(index + 1), "");
		index = previousPath.lastIndexOf("/");

		if (index > 0)
			previousPath = previousPath.substring(0,
					previousPath.length - 1);

		return previousPath;
	}
	
$scope.createFolder = function(){
		
		var templateCreateFolder = {
			'size': 'lg',
			'url': 'modules/dropbox/createFolderPopUp.html',
			'controller':'DropboxPopupController',
			'data': {
				'folder': {
					"path": $scope.showCurrentPath,
					"name": null
				},
			},
		};
		
		$scope.popUpView(templateCreateFolder).then(function(result){
			// Refresh data
			$scope.dropboxFiles = result;
	
			if ($scope.dropboxFiles.length == 1) {
				if ($scope.dropboxFiles[0].error == "NO_AUTH_TOKEN") {
					window.location = "/#/dropbox";
				} else if ($scope.dropboxFiles[0].error
						.indexOf("Error") > -1) {
					alert($scope.dropboxFiles[0].error);
					return;
				}
			}
		});
	};
	
	$scope.popUpView = function(template){
		var open = $modal.open({
			templateUrl : template['url'],
			controller : template['controller'],
			size:template['size'],
			resolve : {
				data : function() {
					return template['data'];
				}
			}
		});
		
		return open.result;
	};

	// Simple function to read URL parameters
	function getUrlParam(name) {

		name = name.replace(/[\[]/, "\\\[").replace(/[\]]/,
				"\\\]");
		var regexS = "[\\?&]" + name + "=([^&#]*)";
		var regex = new RegExp(regexS);
		var results = regex.exec(window.location.href);

		if (results == null)
			return "";
		else
			return results[1];
	}
});

App.register.controller('DropboxPopupController', function($scope, $http, $modalInstance, 
		WebService, toaster, data) {
	
	$scope.folder = data ? data["folder"]: null;
		
	$scope.add = function(addType){		
		var apiName = null;
		var params = null;
		
		switch(addType){
		 	case "folder":
		 		apiName = 'dropbox/createFolder';
		 		
		 		params = {
		 			"path": $scope.folder.path,
		 			"name": $scope.folder.name
		 		};
		 		break;
		};	
				
		WebService.invokePOSTRequest(apiName, params, function(data) {
			var result = angular.fromJson(data);
			
			toaster.pop('success', "Successful process " + addType);	
			$modalInstance.close(result);
		}, function(data) {
			toaster.pop('error', "Uo, ho!", data);
		});		
	};
	
	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	};
});
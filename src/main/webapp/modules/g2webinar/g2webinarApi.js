App.register.controller('G2WebinarAPIController', function($q, $routeParams, $rootScope, $location, $scope, $modal, $timeout, FeatureSettings, Keyboard, Session, safeApply, toaster, Popup, WebService, ScriptLoader, Workflow) {

	/*
	 * Change to module id and feature id of the features you want to test out
	 */
	$scope.module = {
			title : 'G2W API',
			meetingTitle: null,
			webinarTitle: null,
			if_mt_brief: false,
			if_mt_detail: false,
			if_join_form: false,
			if_invite_form: false,
	};
	
	$scope.user = {
			"mail": null,
			"pwd": null,
	};
	
	$scope.user = {};
	
	$scope.oauth = {};
	$scope.registrants = {};
	
	$scope.registrant = {
			"firstName": null,
			"lastName": null,
			"mail": null,
			"webinarKey": null
	};
	
	$scope.meetings = {};
	$scope.meeting = {};
	$scope.userInvitedMeetings = [];

	$scope.getOAuth = function(){
		var params = {"uid": 0};
		
		WebService.invokePOSTRequest('g2webinar/getOAuth', params, function(data){
			$scope.oauth=angular.fromJson(data);
		});
	};
	
	$scope.signG2Webinar = function(){
		WebService.invokePOSTRequest('g2webinar/oAuthConfig', $scope.user, function(data){
			$scope.oauth = angular.fromJson(data);
			
			if (!$scope.oauth["status"]){
				toaster.pop('error', "Uh oh", "Invaid username/password");
			}
				
		});
	};
	
	$scope.getSingleMeeting = function(webinarKey){
		//Reset the content of array meeting
		$scope.meeting = {};
		
		angular.forEach($scope.meetings, function(meeting, index){
			angular.forEach(meeting, function(value,index){
				
				if (value.webinarKey == webinarKey){
					$scope.meeting = value;
					
					$scope.displayMeetingDetail();
				}
				
			});
		});
	};
	
	$scope.listMeetings = function(){
		/*
		 * Clear current content of meetings
		 */
		$scope.meetings = [];
		
		WebService.invokePOSTRequest('g2webinar/listMeetings', {}, function(data) {
			$scope.meetings.push(angular.fromJson(data));
			
			if ($scope.meetings.length > 0){
				$scope.displayMeetingBrief();
				$scope.modult["webinarTitle"] = "My webinars";
			}else{
				$scope.modult["webinarTitle"] = "No upcoming webinars";
			}
			
		}, function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not get Meetings");
		});

	};
	
	$scope.getRegistrants = function(webinarKey){
		
		$scope.getSingleMeeting(webinarKey);
	
		WebService.invokePOSTRequest('g2webinar/getRegistrants', $scope.meeting, function(data) {
			$scope.registrants = angular.fromJson(data);
		}, function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not get Registrants");
		});
		
	};
	
	$scope.displayMeetingBrief = function(){
		$scope.module["if_mt_brief"] = true;
		$scope.module["if_mt_detail"] = false;
		$scope.module["if_join_form"] = false;
		$scope.module["if_invite_form"] = false;
	};
	
	$scope.displayMeetingDetail = function(){
		$scope.module["if_mt_brief"] = false;
		$scope.module["if_mt_detail"] = true;
		$scope.module["if_join_form"] = false;
		$scope.module["if_invite_form"] = false;
	};
	
	$scope.joinMeeting = function(webinarKey){
		WebService.invokePOSTRequest('g2webinar/joinMeeting', $scope.registrant, function(data) {
			var data ={};
			data = angular.fromJson(data);
			$scope.registrant["joinUrl"] = data["joinUrl"]
			
			toaster.pop('success', "Successfully joined meeting", "Successfully joined meeting");
		}, function(data) {
			toaster.pop('error', "Uh oh", "Could not join meeting");
			
		}, function() {
		});
		
		$scope.displayMeetingBrief();
	};
	
	$scope.getUserInvitedMeetings = function(){
		
		WebService.invokePOSTRequest('g2webinar/getUserInvitedMeeting', {}, function(data) {
			$scope.userInvitedMeetingsTmp = angular.fromJson(data);
			
			angular.forEach($scope.userInvitedMeetingsTmp, function(invitedMeeting, index){
				//According to webinarKey, get detail of such meeting
				WebService.invokePOSTRequest('g2webinar/getSingleMeeting',invitedMeeting , function(data) {
					$scope.meetingSingleTemp = angular.fromJson(data);
					
					//store information for join
					$scope.meetingSingleTemp["id"] = invitedMeeting["id"];
					$scope.meetingSingleTemp["organiserUid"] = invitedMeeting["organiserUid"];
					$scope.meetingSingleTemp["joinUrl"] = invitedMeeting["joinUrl"];
					$scope.meetingSingleTemp["ifJoined"] = invitedMeeting["ifJoined"];
					
					$scope.userInvitedMeetings.push($scope.meetingSingleTemp);	
					
					if ($scope.userInvitedMeetings.length > 0){
						$scope.module["meetingTitle"] = "My Meetings";
					}else{
						$scope.module["meetingTitle"] = "No Invited Meetings";
					}
					
				});
			});
			
		});
		
		$scope.displayMeetingBrief();
	};
	
	$scope.inviteUserIntoMeeting = function(){
		$scope.userInvitedMeetings["uid"] = $scope.userInvitedMeetings["userName"]["id"]

		WebService.invokePOSTRequest('g2webinar/inviteUserIntoMeeting', $scope.userInvitedMeetings, function(data) {
			toaster.pop('success', "Successfully invited user", "Successfully invited the user");
		}, function(data) {
			toaster.pop('error', "Uh oh", "Could not invite user");
		}, function() {
		});
		
		$scope.displayMeetingBrief();
	};
	
	$scope.showJoinMeetingForm = function(id, organiserUid, webinarKey, subject){
		$scope.module["if_join_form"] = true;
		$scope.module["if_mt_brief"] = false;
		$scope.module["if_mt_detail"] = false;
		$scope.module["if_invite_form"] = false;
		$scope.registrant["webinarKey"] = webinarKey;
		$scope.registrant["id"] = id;
		$scope.registrant["organiserUid"] = organiserUid;
		$scope.meeting["subject"] = subject;
	};
	
	$scope.showInvitationForm = function(organiserId, webinarKey, subject){
		$scope.module["if_join_form"] = false;
		$scope.module["if_mt_brief"] = false;
		$scope.module["if_mt_detail"] = false;
		$scope.module["if_invite_form"] = true;
		
		$scope.userInvitedMeetings = {};

		$scope.userInvitedMeetings["organiserId"] = organiserId;
		$scope.userInvitedMeetings["webinarKey"] = webinarKey;
		$scope.userInvitedMeetings["subject"] = subject;
	};
	
	$scope.getAllUser = function(){
		WebService.invokePOSTRequest('g2webinar/getAllUser', {}, function(data) {
			$scope.users = angular.fromJson(data);
		});
	}
	
});

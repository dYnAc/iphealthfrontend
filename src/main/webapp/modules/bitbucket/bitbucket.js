App.register.controller('BitbucketController',function($rootScope, $http, $modal,
		$scope,	$route, $location, toaster,	WebService, Popup, ModuleProvider) {

	$scope.module = {
		title : 'BitBucket Module'
	};

	$scope.hideConnect = false;
	$scope.showRepositoryEventsContent = false;
	$scope.showRepositoryIssuesContent = false;
	$scope.showCurrentRepository = "";
	$scope.showCurrentOwner = "";
	
	$scope.config = function() {

		var token = getUrlParam("token");
		var verifier = getUrlParam("verifier");
		if (token == '' && verifier == '') {
			$scope.hideConnect = false;
			WebService.invokeGETRequest('bitbucket/config', {}, function(data) {
				$scope.bitbucketResult = angular
						.fromJson(data);
				if ($scope.bitbucketResult.authCode == "ALREADY_AUTH") {
					window.location = "/#/bitbucket/repositories/";
				}
			},
			function(data, status, headers,	config) {
				toaster.pop('error', "Uh oh", "Could not get bitbucket config" + data);
			});
		} else {
			$scope.hideConnect = true;
			var params = {
				"token" : token,
				"verifier" : verifier
			};

			WebService.invokeGETRequest('bitbucket/redirect',	params,	function(data) {
				$scope.bitbucketResult = angular.fromJson(data);
				if ($scope.bitbucketResult.accessToken
						.indexOf("Error") > -1) {
					alert("Error " + $scope.bitbucketResult.accessToken);
				} else {
					window.location = "/#/bitbucket/repositories/";
				}
			},
			function(data, status, headers, config) {
				toaster.pop('error', "Uh oh", "Could not redirect to bitbucket");
			});
		}
	};

	$scope.connect = function() {

		// build the URL for API Authorization
		window.open($scope.bitbucketResult.authUrl, "_self",
				"BitBucket", false);
	};

	$scope.repositories = function() {

		WebService.invokeGETRequest('bitbucket/repositories', {}, function(data) {
			$scope.bitbucketRepositories = angular
					.fromJson(data);
			
			if ($scope.bitbucketRepositories.length == 1) {
				if($scope.bitbucketRepositories[0].error != null){
					if ($scope.bitbucketRepositories[0].error == "NO_AUTH_TOKEN") {
						window.location = "/#/bitbucket";
					} else if ($scope.bitbucketRepositories[0].error
							.indexOf("Error") > -1) {
						alert($scope.bitbucketRepositories[0].error);
					}
				}
			}
		},
		function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not get repositories config");
		});
	};
	
	$scope.events = function(owner, name) {

		$scope.showRepositoryEventsContent = false;
		$scope.showRepositoryIssuesContent = false;
		$scope.showCurrentRepository = '';
		$scope.showCurrentOwner = '';
		
		if(owner == '' || name == '')
			return;
		
		var params = {
				"owner" : owner,
				"name": name
		};
		
		WebService.invokeGETRequest('bitbucket/events', params, function(data) {
			$scope.bitbucketRepositoryEvents = angular
					.fromJson(data);

			if($scope.bitbucketRepositoryEvents.events == null){
				toaster.pop('sucess', "Uh oh", "There are no events for this repository");
				return;
			}
			
			if ($scope.bitbucketRepositoryEvents.events.length == 1) {
				if ($scope.bitbucketRepositoryEvents.events[0].error == "NO_AUTH_TOKEN") {
					window.location = "/#/github";
				} else if ($scope.bitbucketRepositoryEvents.events[0].error
						.indexOf("Error") > -1) {
					alert($scope.bitbucketRepositoryEvents.events[0].error);
				}
			}
			
			$scope.showRepositoryEventsContent = true;
			$scope.showRepositoryIssuesContent = false;
			$scope.showCurrentRepository = name;
			$scope.showCurrentOwner = owner;
		},
		function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not get files config");
		});
	};
	
	$scope.issues = function(owner, name) {

		$scope.showRepositoryEventsContent = false;
		$scope.showRepositoryIssuesContent = false;
		$scope.showCurrentRepository = '';
		$scope.showCurrentOwner = '';
		
		if(owner == '' || name == '')
			return;
		
		var params = {
				"owner" : owner,
				"name": name
		};
		
		WebService.invokeGETRequest('bitbucket/issues', params, function(data) {
			$scope.bitbucketRepositoryIssues = angular
					.fromJson(data);
			
			if($scope.bitbucketRepositoryIssues == null){
				toaster.pop('sucess', "Uh oh", "There are no issues for this repository");
				return;
			}

			if ($scope.bitbucketRepositoryIssues.length == 1) {
				if($scope.bitbucketRepositoryIssues[0].error != null){
					if ($scope.bitbucketRepositoryIssues[0].error == "NO_AUTH_TOKEN") {
						window.location = "/#/github";
					} else if ($scope.bitbucketRepositoryIssues[0].error
							.indexOf("Error") > -1) {
						alert($scope.bitbucketRepositoryIssues[0].error);
					}
				}
			}
			
			$scope.showRepositoryEventsContent = false;
			$scope.showRepositoryIssuesContent = true;
			$scope.showCurrentRepository = name;
			$scope.showCurrentOwner = owner;
		},
		function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not get issues config");
		});
	};
	
	$scope.createRepository = function(){
		
		var templateCreateRepository = {
			'size': 'lg',
			'url': 'modules/bitbucket/createRepositoryPopUp.html',
			'controller':'BitbucketPopupController',
			'data': {
				'repository': {
					"name": null,
					"description": null,
					"is_private":false
				},
			},
		};
		
		$scope.popUpView(templateCreateRepository).then(function(result){
			// Refresh data
			$scope.bitbucketRepositories = result;
			if ($scope.bitbucketRepositories.length == 1) {
				if ($scope.bitbucketRepositories[0].error == "NO_AUTH_TOKEN") {
					window.location = "/#/github";
				} else if ($scope.bitbucketRepositories[0].error
						.indexOf("Error") > -1) {
					alert($scope.bitbucketRepositories[0].error);
				}
			}
		});
	};
	
	$scope.createIssue = function(owner, name){
		
		if(owner == '' || name == '')
			return;
		
		var templateCreateIssue = {
			'size': 'lg',
			'url': 'modules/bitbucket/createIssuePopUp.html',
			'controller':'BitbucketPopupController',
			'data': {
				'issue': {
					"owner": owner,
					"name": name,
					"title": null,
					"content": null,
					"responsible":null,
					"edit":false
				},
			},
		};
		
		$scope.popUpView(templateCreateIssue).then(function(result){
			// Refresh data
			$scope.bitbucketRepositoryIssues = result;
			
			if($scope.bitbucketRepositoryIssues == null){
				toaster.pop('sucess', "Uh oh", "There are no issues for this repository");
				return;
			}

			if ($scope.bitbucketRepositoryIssues.length == 1) {
				if($scope.bitbucketRepositoryIssues[0].error != null){
					if ($scope.bitbucketRepositoryIssues[0].error == "NO_AUTH_TOKEN") {
						window.location = "/#/github";
					} else if ($scope.bitbucketRepositoryIssues[0].error
							.indexOf("Error") > -1) {
						alert($scope.bitbucketRepositoryIssues[0].error);
					}
				}
			}
			
			$scope.showRepositoryEventsContent = false;
			$scope.showRepositoryIssuesContent = true;
			$scope.showCurrentRepository = name;
			$scope.showCurrentOwner = owner;
		});
	};
	
	$scope.editIssue = function(owner, name, id, title, content, responsible){
		
		if(owner == '' || name == '' || id == '' || title == ''
			|| content == '' || responsible == '')
			return;
		
		var templateCreateIssue = {
			'size': 'lg',
			'url': 'modules/bitbucket/createIssuePopUp.html',
			'controller':'BitbucketPopupController',
			'data': {
				'issue': {
					"owner": owner,
					"name": name,
					"id": id,
					"title": title,
					"content": content,
					"responsible":responsible,
					"edit":true
				},
			},
		};
		
		$scope.popUpView(templateCreateIssue).then(function(result){
			// Refresh data
			$scope.bitbucketRepositoryIssues = result;
			
			if($scope.bitbucketRepositoryIssues == null){
				toaster.pop('sucess', "Uh oh", "There are no issues for this repository");
				return;
			}

			if ($scope.bitbucketRepositoryIssues.length == 1) {
				if($scope.bitbucketRepositoryIssues[0].error != null){
					if ($scope.bitbucketRepositoryIssues[0].error == "NO_AUTH_TOKEN") {
						window.location = "/#/github";
					} else if ($scope.bitbucketRepositoryIssues[0].error
							.indexOf("Error") > -1) {
						alert($scope.bitbucketRepositoryIssues[0].error);
					}
				}
			}
			
			$scope.showRepositoryEventsContent = false;
			$scope.showRepositoryIssuesContent = true;
			$scope.showCurrentRepository = name;
			$scope.showCurrentOwner = owner;
		});
	};
	
	$scope.popUpView = function(template){
		var open = $modal.open({
			templateUrl : template['url'],
			controller : template['controller'],
			size:template['size'],
			resolve : {
				data : function() {
					return template['data'];
				}
			}
		});
		
		return open.result;
	};

	// Simple function to read URL parameters
	function getUrlParam(name) {

		name = name.replace(/[\[]/, "\\\[").replace(/[\]]/,
				"\\\]");
		var regexS = "[\\?&]" + name + "=([^&#]*)";
		var regex = new RegExp(regexS);
		var results = regex.exec(window.location.href);

		if (results == null)
			return "";
		else
			return results[1];
	}
});

App.register.controller('BitbucketPopupController', function($scope, $http, $modalInstance, 
		WebService, toaster, data) {
	
	$scope.repository = data ? data["repository"]: null;
	$scope.issue = data ? data["issue"]: null;
		
	$scope.add = function(addType){		
		var apiName = null;
		var params = null;
		
		switch(addType){
		 	case "repository":
		 		apiName = 'bitbucket/createRepository';
		 		
		 		params = {
		 			"name": $scope.repository.name,
		 			"description": $scope.repository.description,
		 			"is_private": $scope.repository.is_private
		 		};
		 		break;
		 	case "issue":
		 		if(!$scope.issue.edit){
		 			apiName = 'bitbucket/createIssue';
		 			
		 			params = {
		 				"owner": $scope.issue.owner,
			 			"name": $scope.issue.name,
			 			"title": $scope.issue.title,
			 			"content": $scope.issue.content,
			 			"responsible": $scope.issue.responsible
			 		};
		 		}
		 		else{
		 			apiName = 'bitbucket/modifyIssue';
		 		
			 		params = {
		 				"owner": $scope.issue.owner,
			 			"name": $scope.issue.name,
			 			"id": $scope.issue.id,
			 			"title": $scope.issue.title,
			 			"content": $scope.issue.content,
			 			"responsible": $scope.issue.responsible
			 		};
		 		}
		 		break;
		};	
				
		WebService.invokePOSTRequest(apiName, params, function(data) {
			var result = angular.fromJson(data);
			
			toaster.pop('success', "Successful process " + addType);	
			$modalInstance.close(result);
		}, function(data) {
			toaster.pop('error', "Uo, ho!", data);
		});		
	};
	
	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	};
});
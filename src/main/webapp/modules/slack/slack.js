App.register.controller('SlackController', function($rootScope, $http, $scope,
		$route, $location, toaster, WebService, Popup, ModuleProvider, ngTableParams) {

	$scope.module = {
		title : 'Slack Module'
	};

	$scope.hideConnect = false;
	$scope.showChannels = true;
	$scope.slack = {
		comment : ""
	};
	
	$scope.query = {
		params : ""
	};

	$scope.config = function() {

		var code = getUrlParam("code");
		if (code == '') {
			$scope.hideConnect = false;
			WebService.invokeGETRequest('slack/config', {}, function(data) {
				$scope.slackResult = angular.fromJson(data);
				if ($scope.slackResult.authCode == "ALREADY_AUTH") {
					window.location = "/#/slack/channels/";
				}
			}, function(data, status, headers, config) {
				toaster.pop('error', "Uh oh", "Could not get slack config");
			});
		} else {
			$scope.hideConnect = true;
			var params = {
				"authCode" : code
			};

			WebService.invokeGETRequest('slack/redirect', params, function(data) {
				$scope.slackResult = angular.fromJson(data);
				if ($scope.slackResult.accessToken
						.indexOf("Error") > -1) {
					alert("Error "
							+ $scope.slackResult.accessToken);
				} else {
					window.location = "/#/slack/channels/";
				}
			}, function(data, status, headers, config) {
				toaster.pop('error', "Uh oh",
						"Could not redirect to slack");
			});
		}
	};

	$scope.connect = function() {

		// build the URL for API Authorization
		window.open($scope.slackResult.authUrl, "_self", "Asana", false);
	};

	$scope.channels = function() {

		var id = getUrlParam("id");
		if (id == '') {
			$scope.showChannels = true;
			WebService.invokeGETRequest('slack/channels', {}, function(data) {
				$scope.slackChannels = angular.fromJson(data);

				if ($scope.slackChannels.ok != null) {
					if ($scope.slackChannels.ok == "NO_AUTH_TOKEN") {
						window.location = "/#/slack";
					} else {
						alert("Error " + $scope.slackChannels.ok);
					}
				}
			}, function(data, status, headers, config) {
				toaster.pop('error', "Uh oh", "Could not get channels config");
			});
		} else {
			$scope.showChannels = true;			
			$scope.channel(id);			
		}
	};
	
	$scope.postComment = function() {
		if ($scope.slack.comment == '' || $scope.currentChannelId == '') {
			return;
		}

		var params = {
			"guid" : $scope.currentChannelId,
			"comment" : $scope.slack.comment
		};

		WebService.invokePOSTRequest('slack/channel', params, function(data) {
			$scope.postChannelComment = data;
			$scope.slack.comment = "";

			if ($scope.postChannelComment == "NO_AUTH_TOKEN") {
				window.location = "/#/slack";
			} else if ($scope.postChannelComment.indexOf("Error") > -1) {
				alert("Error " + $scope.postChannelComment);
			} else {
				toaster.pop('Post a comment to channel sucessful', "Yeah",
						"Your comment has been added succesfully");

				$scope.channel($scope.currentChannelId);
			}
		}, function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not post tweet");
		});
	};

	$scope.channel = function(id) {
		var params = {
			"guid" : id
		};
		
		WebService.invokeGETRequest('slack/channel', params,
				function(data) {
					$scope.slackChannel = angular.fromJson(data);
					$scope.showChannels = false;
					if ($scope.slackChannel.ok != null) {
						if ($scope.slackChannel.ok == "NO_AUTH_TOKEN") {
							window.location = "/#/slack";
						} else {
							alert("Error " + $scope.slackChannel.ok);
						}
						return;
					}
					
					var name = getUrlParam("name");
					$scope.currentChannelName = name;
					$scope.currentChannelId = id;
				}, function(data, status, headers, config) {
					toaster.pop('error', "Uh oh",
							"Could not get channel config" + data);
				});
	};

	$scope.teamMembers = function() {

		WebService.invokeGETRequest('slack/teamMembers', {}, function(data) {
			$scope.slackTeamMembers = angular.fromJson(data);

			if ($scope.slackTeamMembers.ok != null) {
				if ($scope.slackTeamMembers.ok == "NO_AUTH_TOKEN") {
					window.location = "/#/slack";
				} else {
					alert("Error " + $scope.slackTeamMembers.ok);
				}
				
				return;
			}
			
			// data list pagination
			$scope.pagination($scope.slackTeamMembers.members);
		}, function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not get team members config");
		});
	};

	$scope.files = function() {		
		WebService.invokeGETRequest('slack/files', {}, function(data) {
			$scope.slackFiles = angular.fromJson(data);

			if ($scope.slackFiles.ok != null) {
				if ($scope.slackFiles.ok == "NO_AUTH_TOKEN") {
					window.location = "/#/slack";
				} else {
					alert("Error " + $scope.slackFiles.ok);
				}
				
				return;
			}
			
			// data list pagination
			$scope.pagination($scope.slackFiles.files);
		}, function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not get files config");
		});
	};
	
	$scope.continueFileUpload = function() {
		var formData = new FormData();
		formData.append("file", file.files[0]);
		formData.append("guid", $scope.currentChannelId);

		WebService.invokePOSTRequestFile('slack/file', formData, function(data) {
			$scope.fileUploadStatus = data;

			if ($scope.fileUploadStatus == "NO_AUTH_TOKEN") {
				window.location = "/#/slack";
			} else if ($scope.fileUploadStatus
					.indexOf("Error") > -1) {
				alert($scope.fileUploadStatus);
			} else {
				toaster.pop('Upload sucessful', "Yeah",
						    "The file has been upload succesfully");

				$scope.channel($scope.currentChannelId);
			}
		},
		function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not upload file config");
		});
	};
	
	$scope.searchMessages = function() {
		if ($scope.query.params == '') {
			return;
		}

		var params = {
			"query" : $scope.query.params
		};

		WebService.invokeGETRequest('slack/messagesQuery', params, function(data) {
			$scope.messagesResults = angular.fromJson(data);

			if ($scope.messagesResults.ok != null) {
				if ($scope.messagesResults.ok == "NO_AUTH_TOKEN") {
					window.location = "/#/slack";
				} else {
					alert("Error " + $scope.messagesResults.ok);
				}
				
				return;
			}
		}, function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not search messages");
		});
	};
	
	$scope.searchFiles = function() {
		if ($scope.query.params == '') {
			return;
		}

		var params = {
			"query" : $scope.query.params
		};

		WebService.invokeGETRequest('slack/filesQuery', params, function(data) {
			$scope.filesResults = angular.fromJson(data);

			if ($scope.filesResults.ok != null) {
				if ($scope.filesResults.ok == "NO_AUTH_TOKEN") {
					window.location = "/#/slack";
				} else {
					alert("Error " + $scope.filesResults.ok);
				}
				
				return;
			}
		}, function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not search files" + data);
		});
	};

	$scope.pagination = function(data) {

		$scope.tableParams = new ngTableParams({
			page : 1, // show first page
			count : 5 // count per page
		}, {
			total : -1, // length of data
			getData : function($defer, params) {

				$scope.dataPiece = data.slice((params.page() - 1)
						* params.count(), params.page() * params.count());

				params.total(data.length); // set total for recalc pagination
				$defer.resolve($scope.dataPiece);
			}
		});
	};

	// Simple function to read URL parameters
	function getUrlParam(name) {

		name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
		var regexS = "[\\?&]" + name + "=([^&#]*)";
		var regex = new RegExp(regexS);
		var results = regex.exec(window.location.href);

		if (results == null)
			return "";
		else
			return results[1];
	}
});
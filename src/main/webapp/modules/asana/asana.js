App.register.controller('AsanaController', function($rootScope, $http, $scope,
		$route, $location, toaster, WebService, Popup, ModuleProvider) {

	$scope.module = {
		title : 'Asana Module'
	};
	
	$scope.hideConnect = false;
	$scope.showTasks = true;
	$scope.comment = {
		comment : ""
	};

	$scope.config = function() {

		var code = getUrlParam("code");
		if (code == '') {
			$scope.hideConnect = false;
			WebService.invokeGETRequest('asana/config', {}, function(data) {
				$scope.asanaResult = angular.fromJson(data);
				if ($scope.asanaResult.authCode == "ALREADY_AUTH") {
					window.location = "/#/asana/tasks/";
				}
			}, function(data, status, headers, config) {
				toaster.pop('error', "Uh oh", "Could not get asana config");
			});
		} else {
			$scope.hideConnect = true;
			var params = {
				"authCode" : code
			};

			WebService.invokeGETRequest('asana/redirect', params, function(
					data) {
				$scope.asanaResult = angular.fromJson(data);
				if ($scope.asanaResult.accessToken.indexOf("Error") > -1) {
					alert("Error " + $scope.asanaResult.accessToken);
				} else {
					window.location = "/#/asana/tasks/";
				}
			}, function(data, status, headers, config) {
				toaster.pop('error', "Uh oh", "Could not redirect to asana");
			});
		}
	};

	$scope.connect = function() {

		// build the URL for API Authorization
		window.open($scope.asanaResult.authUrl, "_self", "Asana", false);
	};

	$scope.tasks = function() {

		var id = getUrlParam("id");		
		if (id == '') {
			$scope.showTasks = true;
			WebService.invokeGETRequest('asana/tasks', {}, function(data) {
				$scope.asanaTasks = angular.fromJson(data);
	
				if ($scope.asanaTasks.error != null) {
					if ($scope.asanaTasks.error == "NO_AUTH_TOKEN") {
						window.location = "/#/asana";
					} else {
						alert("Error " + $scope.asanaTasks.error);
					}
				}
			}, function(data, status, headers, config) {
				toaster.pop('error', "Uh oh", "Could not get tasks config");
			});			
		}
		else{
			$scope.showTasks = true;		
			var params = {
					"id" : id
				};
			
			WebService.invokeGETRequest('asana/task', params, function(data) {
				$scope.asanaTask = angular.fromJson(data);
				$scope.showTasks = false;
				if ($scope.asanaTask.error != null) {
					if ($scope.asanaTask.error == "NO_AUTH_TOKEN") {
						window.location = "/#/asana";
					} else {
						alert("Error " + $scope.asanaTask.error);
					}
				}
			}, function(data, status, headers, config) {
				toaster.pop('error', "Uh oh", "Could not get task config");
			});	
		}
	};
	
	$scope.displayTasks = function() {
		window.location = "/#/asana/tasks";
	};
	
	$scope.saveTask = function() {
		if ($scope.asanaTask.data.name == '') {
			return;
		}

		var params = {
			"id" : $scope.asanaTask.data.id,
			"name" : $scope.asanaTask.data.name,
			"notes" : $scope.asanaTask.data.notes
		};

		WebService.invokePOSTRequest('asana/task', params, function(data) {
			tempStories = $scope.asanaTask.stories;
			$scope.asanaTask = angular.fromJson(data);
			$scope.asanaTask.stories = tempStories;
			
			if ($scope.asanaTask.error != null) {
				if ($scope.asanaTask.error == "NO_AUTH_TOKEN") {
					window.location = "/#/asana";
				} else {
					alert("Error " + $scope.asanaTask.error);
				}
			}
			else
			{
				toaster.pop('Update sucessful', "Yeah", "The task has been update succesfully");				
			}
		}, function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not update task");
		});
	};

	$scope.sendComment = function() {
		if ($scope.comment.comment == '') {
			return;
		}
		
		var params = {
			"id" : $scope.asanaTask.data.id,
			"comment" : $scope.comment.comment
		};
		
		WebService.invokePOSTRequest('asana/taskComment', params, function(data) {
			$scope.asanaTask.stories = angular.fromJson(data);
			$scope.comment.comment = "";
			
			if ($scope.asanaTask.stories.error != null) {
				if ($scope.asanaTask.stories.error == "NO_AUTH_TOKEN") {
					window.location = "/#/asana";
				} else {
					alert("Error " + $scope.asanaTask.stories.error);
				}
			}
			else
			{
				toaster.pop('Add comment sucessful', "Yeah", "The comment has been added succesfully");
			}			
		}, function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not update task");
		});
	};
	
	// Simple function to read URL parameters
	function getUrlParam(name) {
		
		name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
		var regexS = "[\\?&]" + name + "=([^&#]*)";
		var regex = new RegExp(regexS);
		var results = regex.exec(window.location.href);

		if (results == null)
			return "";
		else
			return results[1];
	}
});
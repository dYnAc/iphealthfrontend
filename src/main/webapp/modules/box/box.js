App.register.controller('BoxController',function($rootScope, $http, $modal,
		$scope,	$route, $location, toaster,	WebService, Popup, ModuleProvider) {

	$scope.module = {
		title : 'Box Module'
	};

	$scope.hideConnect = false;
	$scope.showCurrentFolder = "/";
	$scope.currentFolderId = "0";
	$scope.previousFolder;
	$scope.isRoot = true;

	$scope.config = function() {

		var code = getUrlParam("code");
		if (code == '') {
			$scope.hideConnect = false;
			WebService.invokeGETRequest('box/config', {}, function(data) {
				$scope.boxResult = angular
						.fromJson(data);
				if ($scope.boxResult.authCode == "ALREADY_AUTH") {
					window.location = "/#/box/files/";
				}
			},
			function(data, status, headers,	config) {
				toaster.pop('error', "Uh oh", "Could not get box config");
			});
		} else {
			$scope.hideConnect = true;
			var params = {
				"authCode" : code
			};

			WebService.invokeGETRequest('box/redirect',	params,	function(data) {
				$scope.boxResult = angular
						.fromJson(data);
				if ($scope.boxResult.accessToken
						.indexOf("Error") > -1) {
					alert("Error " + $scope.boxResult.accessToken);
				} else {
					window.location = "/#/box/files/";
				}
			},
			function(data, status, headers, config) {
				toaster.pop('error', "Uh oh", "Could not redirect to box");
			});
		}
	};

	$scope.connect = function() {

		// build the URL for API Authorization
		window.open($scope.boxResult.authUrl, "_self",
				"Box", false);
	};
	
	$scope.files = function(id, name, isFile, parentId) {
		
		if (id != "" && isFile == false) {			
			var params = {
				"id" : id
			};
			
			WebService.invokeGETRequest('box/files', params, function(data) {
				$scope.boxFiles = angular
						.fromJson(data);
				
				if ($scope.boxFiles.length == 1) {
					if ($scope.boxFiles[0].error == "NO_AUTH_TOKEN") {
						window.location = "/#/box";
					} else if ($scope.boxFiles[0].error
							.indexOf("Error") > -1) {
						alert($scope.boxFiles[0].error);
						return;
					}
				}
				
				if(id != "0"){
					WebService.invokeGETRequest('box/parentFolder', params, function(data) {	
						$scope.previousFolder = angular
						.fromJson(data);
					},function(data, status, headers, config) {
						toaster.pop('error', "Uh oh", "Could not get files config");
					});				
				}
				
				$scope.showCurrentFolder = id == "0" ? "/" : name;
				$scope.currentFolderId = id;

				if ($scope.showCurrentFolder != "/") {
					$scope.isRoot = false;
				} else if ($scope.showCurrentFolder == "/") {
					$scope.isRoot = true;
				}
			},
			function(data, status, headers, config) {
				toaster.pop('error', "Uh oh", "Could not get files config");
			});
		} else if (id != "" && isFile == true) {
			var params = {
				"id" : id
			};

			WebService.invokeGETRequest('box/file',	params,	function(data) {
				var a = window.document.createElement('a');
				a.href = window.URL.createObjectURL(new Blob(
						[ data ],
						{
							type : 'application/octet-stream'
						}));

				a.download = name;

				// Append anchor to body.
				document.body.appendChild(a);
				a.click();

				// Remove anchor from body
				document.body.removeChild(a);

				$scope.files($scope.currentFolderId, $scope.showCurrentFolder, 
						false, $scope.previousFolderId);				
			},
			function(data, status, headers,	config) {
				toaster.pop('error', "Uh oh", "Could not get file config");
			}, null, null, "blob");
		}
	};
	
	$scope.back = function(){
		id = $scope.previousFolder.id;
		name = $scope.previousFolder.id == 0 ? "/" : $scope.previousFolder.name;
		$scope.files(id, name, 
				false, '');	
	};

	$scope.continueFileUpload = function() {
		var formData = new FormData();
		formData.append("file", file.files[0]);
		formData.append("id", $scope.currentFolderId);

		WebService.invokePOSTRequestFile('box/file', formData, function(data) {
			$scope.fileUploadStatus = data;

			if ($scope.fileUploadStatus == "NO_AUTH_TOKEN") {
				window.location = "/#/box";
			} else if ($scope.fileUploadStatus
					.indexOf("Error") > -1) {
				alert($scope.fileUploadStatus);
			} else {
				toaster.pop('Upload sucessful', "Yeah",
						    "The file has been upload succesfully");

				$scope.files($scope.currentFolderId, $scope.showCurrentFolder, 
						false, $scope.previousFolderId);
			}
		},
		function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not upload file config");
		});
	};

	$scope.deleteFile = function(id, isFile) {

		if (id != "") {
			Popup.show('Delete File or Folder',
						'Are you sure you want to delete this file or folder?',
						'yesno')
			.then(function(result) {
				if (result == 'yes') {

					var params = {
						"id" : id,
						"isFile" : isFile
					};

					WebService.invokePOSTRequest('box/deleteFileFolder', params, function(data) {
						$scope.fileDeleteStatus = data;

						if ($scope.fileDeleteStatus == "NO_AUTH_TOKEN") {
							window.location = "/#/box";
						} else if ($scope.fileDeleteStatus
								.indexOf("Error") > -1) {
							alert($scope.fileDeleteStatus);
						} else {
							toaster.pop('Delete sucessful',
										"Yeah",
										"The file has been deleted succesfully");

							$scope.files($scope.currentFolderId, $scope.showCurrentFolder, 
									false, $scope.previousFolderId);
						}
					},
					function(data, status, headers,	config) {
						toaster.pop('error', "Uh oh",
									"Could not delete file config");
					});
				}
			});
		}
	};
	
	$scope.createFolder = function(){
		
		var templateCreateFolder = {
			'size': 'lg',
			'url': 'modules/box/createFolderPopUp.html',
			'controller':'BoxPopupController',
			'data': {
				'folder': {
					"id": $scope.currentFolderId,
					"name": null,
					"edit": false
				},
			},
		};
		
		$scope.popUpView(templateCreateFolder).then(function(result){
			// Refresh data
			$scope.boxFiles = result;
	
			if ($scope.boxFiles.length == 1) {
				if ($scope.boxFiles[0].error == "NO_AUTH_TOKEN") {
					window.location = "/#/box";
				} else if ($scope.boxFiles[0].error
						.indexOf("Error") > -1) {
					alert($scope.boxFiles[0].error);
					return;
				}
			}
		});
	};
	
	$scope.renameFolder = function(id, name){
		
		var templateRenameFolder = {
			'size': 'lg',
			'url': 'modules/box/createFolderPopUp.html',
			'controller':'BoxPopupController',
			'data': {
				'folder': {
					"id": id,
					"name": name,
					"edit": true
				},
			},
		};
		
		$scope.popUpView(templateRenameFolder).then(function(result){
			// Refresh data
			$scope.boxFiles = result;
	
			if ($scope.boxFiles.length == 1) {
				if ($scope.boxFiles[0].error == "NO_AUTH_TOKEN") {
					window.location = "/#/box";
				} else if ($scope.boxFiles[0].error
						.indexOf("Error") > -1) {
					alert($scope.boxFiles[0].error);
					return;
				}
			}
		});
	};
	
	$scope.createSharedFolderLink = function(id, name){
		
		var templateCreateSharedFolderLink = {
			'size': 'lg',
			'url': 'modules/box/createSharedFolderLinkPopUp.html',
			'controller':'BoxPopupController',
			'data': {
				'sharedFolder': {
					"name": name,
					"id": id,
					"canDownload": false,
					"canPreview": false
				},
			},
		};
		
		$scope.popUpView(templateCreateSharedFolderLink).then(function(result){
			// Verify result	
			if (result == "NO_AUTH_TOKEN") {
				window.location = "/#/box";
			} else if (result.indexOf("Error") > -1) {
				alert($scope.boxFiles[0].error);
				return;
			}
		});
	};
	
	$scope.popUpView = function(template){
		var open = $modal.open({
			templateUrl : template['url'],
			controller : template['controller'],
			size:template['size'],
			resolve : {
				data : function() {
					return template['data'];
				}
			}
		});
		
		return open.result;
	};

	// Simple function to read URL parameters
	function getUrlParam(name) {

		name = name.replace(/[\[]/, "\\\[").replace(/[\]]/,
				"\\\]");
		var regexS = "[\\?&]" + name + "=([^&#]*)";
		var regex = new RegExp(regexS);
		var results = regex.exec(window.location.href);

		if (results == null)
			return "";
		else
			return results[1];
	}
});

App.register.controller('BoxPopupController', function($scope, $http, $modalInstance, 
		WebService, toaster, data) {
	
	$scope.folder = data ? data["folder"]: null;
	$scope.sharedFolder = data ? data["sharedFolder"]: null;
		
	$scope.add = function(addType){		
		var apiName = null;
		var params = null;
		
		switch(addType){
		 	case "folder":
		 		if(!$scope.folder.edit)
		 			apiName = 'box/createFolder';
		 		else
		 			apiName = 'box/renameFolder';
		 		
		 		params = {
		 			"id": $scope.folder.id,
		 			"name": $scope.folder.name
		 		};
		 		break;
		 	case "sharedFolder":
		 		apiName = 'box/sharedFolderLink';
		 			
		 		params = {
		 			"id": $scope.sharedFolder.id,
		 			"canDownload": $scope.sharedFolder.canDownload,
		 			"canPreview": $scope.sharedFolder.canPreview
		 		};
		 		break;
		};	
				
		WebService.invokePOSTRequest(apiName, params, function(data) {
			var result = null;
			
			if(addType != "sharedFolder")
				result = angular.fromJson(data);
			else
				result = data;
			
			toaster.pop('success', "Successful process " + addType);	
			$modalInstance.close(result);
		}, function(data) {
			toaster.pop('error', "Uo, ho!", data);
		});		
	};
	
	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	};
});
/*
 * All dynamically loaded controllers MUST be registered using App.register.controller
 */
App.register.controller('MessagingController', function($rootScope, $http, $scope, $route, $location, toaster, WebService, Popup, ModuleProvider) {
	$scope.module = {
		title : 'Messaging'
	};

	$scope.message = {
		message : ""
	};

	$scope.messages = [];

	$scope.sendMessage = function() {
		if (!$scope.message.message || $scope.message.message == '') {
			return;
		}

		var params = {
			"message" : $scope.message.message
		};

		WebService.invokePOSTRequest('messaging/send', params, function(data) {
			$scope.messages.push(angular.fromJson(data));
			$scope.message.message = "";
		}, function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not send message");
		});
	};

	$scope.listAllMessages = function() {
		WebService.invokePOSTRequest('messaging/list', {}, function(data) {
			$scope.messages = angular.fromJson(data);
		}, function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not get messages");
		});
	};
	
	$scope.deleteMessages = function() {	
		
		Popup.show('Delete Messages', 'Are you sure you want to delete all your messages?', 'yesno')
		.then(function(result) {
			if (result == 'yes') {
				WebService.invokePOSTRequest('messaging/delete', {}, function(data) {
					$scope.messages = angular.fromJson(data);
				}, function(data, status, headers, config) {
					toaster.pop('error', "Uh oh", "Could not delete messages");
				});
			}
		});	
	};

	$scope.listAllMessages();
});
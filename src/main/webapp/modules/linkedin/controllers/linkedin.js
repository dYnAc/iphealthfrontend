App.register.controller('LinkedinController', function($q, $rootScope, $http, $scope,
		$route, $location, $modal, toaster, WebService, Popup, ModuleProvider, Session) {

	$scope.module = {
		"title" : "Connecting ...",
		"oauth_url": null,
		"if_conn_configure": false, //true
		"if_functions": true,
		"if_company_updates": false,
		"if_try_again":false,
		"if_group_updates": false,
		"if_my_connections": false,
		"if_my_networks":false,
		"if_my_profile":false,
		"if_advanced_search":false,
		"limit_group":6,
		"limit_company": 6,
		"limit_discussion":5,
		"limit_companyUpdates":3,
		"limit_networkUpdates":3,
		"limit_myConnections":5,
		"limit_summary": 256,
		"limit_sug_company":6
	};
	
	$scope.displayController = function(display) {
		$scope.module['if_conn_configure'] = false;
		$scope.module['if_functions']	= false;
		$scope.module['if_company_updates']	= false;
		$scope.module['if_try_again']	= false;
		$scope.module['if_group_updates']	= false;
		$scope.module['if_my_connections']	= false;
		$scope.module['if_my_networks']	= false;
		$scope.module['if_my_profile']	= false;
		$scope.module['if_advanced_search']	= false;
		$scope.module[display] = true;
	};
	
	$scope.comment = {
			"comment":null,
	};
	
	$scope.discussion = {
			"title": null,
			"summary":null,
			"group":null,
			"type":null
	};
	
	$scope.error = function(content){
		toaster.pop('info', "Uh oh", content);
	};
	
	$scope.spc = new RegExp('&#39;', 'g');
      
	$scope.read = function(length){
		if (length == 0 || length == null){
			$scope.module['limit_summary'] = 256;
		}else{
			$scope.module['limit_summary'] = length;
		}
	};
	
	$scope.date = function(seconds){
		var diff = (Date.now() - seconds)/60/60/1000;
		var now_hour = Date.now();
		
		if (diff < 14 ){
			return diff.toFixed(0) + " hours ago";
		} else{
			return (diff/24).toFixed(0) + ((diff/24) >= 2 ? " days ":" day ") +  "ago";
		};
	};
	
	$scope.connLinkedin = function() {
		var code = getUrlParam("code");
		
		if (code == '') {
			WebService.invokeGETRequest('linkedin/connLinkedin', {}, function(data) {
				var result = angular.fromJson(data);
				
				if (result['responseCode'] == '200'){
					$scope.getMyProfile();
				/*
				 * response code = l-001, means access token retrieve from database is null 
				 */
				}else if (result['responseCode'] == 'l-001'){
					// display configuration page
					$scope.displayController('if_conn_configure');
					$scope.module['title'] = "Linkedin Configuration";
					
					toaster.pop('error', "Uh oh", "Could not get Linkedin config");
					$scope.module['oauth_url'] = result['responseBody'];
				}else{
					$scope.displayController('if_conn_configure');
					toaster.pop('error', "Uh oh",result['responseBody']);
				};
				window.location = '/#/linkedin/myLinkedin';
				
			}, function(data, status, headers, config) {
				toaster.pop('error', "Uh oh", "Could not get linkedin config");
			});
		} else {
			var params = {"code": code,};
			
			WebService.invokeGETRequest('linkedin/configLinkedin', params, function(data) {
				var result = angular.fromJson(data);
				
				if (result['responseCode'] == '200'){
					$scope.getMyProfile();
				}else{
					$scope.displayController('if_conn_configure');
					toaster.pop('error', "Uh oh",result['responseBody']);
				};
				window.location = '/#/linkedin/myLinkedin';
				
			}, function(data, status, headers, config) {
				toaster.pop('error', "Uh oh", "Could not redirect to asana");
			});
		};
		
	};

	$scope.connectOAuth = function() {
		if ($scope.module['oauth_url'] == null){
			WebService.invokeGETRequest('linkedin/getOAuthURL', {}, function(data) {
				$scope.module['oauth_url'] = data;
			}, function(data){
				toaster.pop('error', 'Uh,oh', "Can't connect to server");
			});
		}
		
		// build the URL for API Authorization
		window.open($scope.module['oauth_url'], "_self", "Linkedin", false);
	};
	
	$scope.getMyProfile = function(field){
		var deferred=  $q.defer();
		
		WebService.invokeGETRequest('linkedin/getMyProfile', {"field": (field == null ? 'standard':field)}, function(data) {
			var result = angular.fromJson(data);
			
			if (result['responseCode'] != '200'){
				if (field == null){
					$scope.module['title'] = 'Connect to linkedin';
					$scope.displayController('if_try_again');	
				}
				toaster.pop('error', result['responseCode'], result['responseBody']);
				deferred.reject(result['responseBody']);
			} else{
				// if successful
				$scope.displayController('if_functions');
				
				if (field == 'full'){
					$scope.displayController('if_my_profile');
				}
				
				$scope.module['title'] = 'My Linkedin';
				toaster.pop('success', "Uh, oh","Connect to Linkedin Successfully!");
				
				$scope.myProfile = angular.fromJson(result['responseBody'])['person'];

				replaceInfoOfUser($scope.myProfile['picture-url'], $scope.myProfile['first-name'] + ' ' +  $scope.myProfile['last-name']);
				
				deferred.resolve(result['responseBody']);
			};

		}, function(data){
			toaster.pop('error', 'Uh,oh', "Can't connect to server");
		});
		
		return deferred.promise;
	};
	
	$scope.getMyConnections = function(){
		var deferred=  $q.defer();
		
		WebService.invokeGETRequest('linkedin/getMyConnections', {}, function(data) {
			var result = angular.fromJson(data);
			
			if (result['responseCode'] != '200'){
				deferred.reject(result['responseBody']);
				toaster.pop('error', result['responseCode'], result['responseBody']);
			} else{
				$scope.module['title'] = 'My Connections';
				$scope.displayController("if_my_connections");
				$scope.connections = angular.fromJson(result['responseBody'])['connections']['person'];
				deferred.resolve(result['responseBody']);
			}
		}, function(data){
			toaster.pop('error', 'Uh,oh', "Can't connect to server");
		});
		
		return deferred.promise;
	};
	
	$scope.sendMessage = function(person){
		$scope.sendToPerson = person;
		
		if ($scope.sendToPerson['subject'] != null && $scope.sendToPerson['detail'] != null){
			var params = {
					"message" :"<mailbox-item>"
										+ "<recipients>"
											+ "<recipient>"
												+ "<person path='/people/" + $scope.sendToPerson['id'] + "' />"
											+ "</recipient>"
										+ "</recipients>"
										+ "<subject>" + $scope.sendToPerson['subject'] + "</subject>"
										+ "<body>" + $scope.sendToPerson['detail'] + "</body>"
							 + "</mailbox-item>",
			};
			
			WebService.invokeGETRequest('linkedin/sendMessage', params, function(data) {
				var result = angular.fromJson(data);
				
				if (result['responseCode'] != '200'){
					deferred.reject(result['responseBody']);
					toaster.pop('error', result['responseCode'], result['responseBody']);
				} else{
					toaster.pop('success', result['responseCode'], result['responseBody']);
				}
			}, function(data){
				toaster.pop('error', 'Uh,oh', "Can't connect to server");
			});
		}
	};
	
	$scope.sendInvitation = function(invitation){
		var params = {
				"message" :"<mailbox-item>"
									+ "<recipients>"
										+ "<recipient>"
											+ "<person path='/people/email=" + invitation['email'] + "'>"
												+ "<first-name>" + invitation['firstName'] + "</first-name>"
												+ "<last-name>" + invitation['lastName'] + "</last-name>"
											+ "</person>"
										+ "</recipient>"
									+ "</recipients>"
									+ "<subject>" + invitation['subject'] + "</subject>"
									+ "<body>" + invitation['detail'] + "</body>"
									+ "<item-content>"
								    	+ "<invitation-request>"
								    		+ "<connect-type>friend</connect-type>"
								    	+ "</invitation-request>"
								    + "</item-content>"
						 + "</mailbox-item>",
		};
	
		WebService.invokeGETRequest('linkedin/sendMessage', params, function(data) {
			var result = angular.fromJson(data);
			
			if (result['responseCode'] != '200'){
				deferred.reject(result['responseBody']);
				toaster.pop('error', result['responseCode'], result['responseBody']);
			} else{
				$scope.invitation = null;
				toaster.pop('success', result['responseCode'], result['responseBody']);
			}
		}, function(data){
			toaster.pop('error', 'Uh,oh', "Can't connect to server");
		});
	
	};
	
	$scope.getMyFollowedCompanies = function(){
		var deferred=  $q.defer();
		
		WebService.invokeGETRequest('linkedin/getMyFollowedCompanies', {}, function(data) {
			var result = angular.fromJson(data);
			
			if (result['responseCode'] != '200'){
				deferred.reject(result['responseBody']);
				toaster.pop('error', result['responseCode'], result['responseBody']);
			} else{
				$scope.companies = angular.fromJson(result['responseBody']);
				deferred.resolve(result['responseBody']);
			}
		}, function(data){
			toaster.pop('error', 'Uh,oh', "Can't connect to server");
		});
		
		return deferred.promise;
	};
	
	$scope.getMyFollowedCompaniesUpdates = function(){
		WebService.invokeGETRequest('linkedin/getFollowedCompaniesUpdates', {}, function(data) {
			var result = angular.fromJson(data);
			
			if (result['responseCode'] != '200'){
				toaster.pop('error', result['responseCode'], result['responseBody']);
			} else{
				$scope.module['title'] = 'Recent Updates';
				$scope.displayController("if_company_updates");
				$scope.data = angular.fromJson(result['responseBody']);
				$scope.companies = angular.fromJson(result['responseBody'])['companies']['company'];
				$scope.companiesUpdatesTemp = angular.fromJson(result['responseBody'])['company_updates'];
				$scope.companiesUpdates = [];
				
				for(i=0; i< $scope.companies.length; i++){
					if ($scope.companiesUpdatesTemp[i]['updates']['count'] > 0){
						for(j=0; j<$scope.companiesUpdatesTemp[i]['updates']['count']; j++){
							$scope.companiesUpdatesTemp[i]['updates']['update'][j]["company"] = $scope.companies[i];
							
							if ($scope.companiesUpdatesTemp[i]['updates']['update'][j]['update-content']['company-status-update']){
								$scope.companiesUpdatesTemp[i]['updates']['update'][j]["type"] = "share";
								$scope.companiesUpdatesTemp[i]['updates']['update'][j]['update-content']['company-status-update']['share']['comment'] = $scope.companiesUpdatesTemp[i]['updates']['update'][j]['update-content']['company-status-update']['share']['comment'].replace($scope.spc,"'"); 
								$scope.companiesUpdatesTemp[i]['updates']['update'][j]['update-content']['company-status-update']['share']['content']['description'] = $scope.companiesUpdatesTemp[i]['updates']['update'][j]['update-content']['company-status-update']['share']['content']['description'].replace($scope.spc,"'");
							}else{
								$scope.companiesUpdatesTemp[i]['updates']['update'][j]["type"] = "job";
							}
							$scope.companiesUpdates.push($scope.companiesUpdatesTemp[i]['updates']['update'][j]);
						}
					}
				}
				
			}
		}, function(data){
			toaster.pop('error', 'Uh,oh', "Can't connect to server");
		});	
	};
	
	$scope.getMyGroups = function(){
		var deferred = $q.defer();
		$scope.groups=[{"show-group-logo-in-profile":false,"group":{"id":3921821,"counts-by-category":{"total":2,"count-for-category":[{"category":{"code":"discussion"},"count":0},{"category":{"code":"job"},"count":0}]},"small-logo-url":"https://media.licdn.com/mpr/mpr/p/2/000/0bd/0a0/19e4c4c.png","name":"Australia, New Zealand Jobs & Career Network 澳大利亚、新西兰地区工作机会，职业人士联盟"},"membership-state":{"code":"member"},"key":3921821},{"show-group-logo-in-profile":false,"group":{"id":2914122,"counts-by-category":{"total":2,"count-for-category":[{"category":{"code":"discussion"},"count":12},{"category":{"code":"job"},"count":19}]},"small-logo-url":"https://media.licdn.com/mpr/mpr/p/4/000/175/2f6/1793419.png","name":"China Job Openings, Career and Opportunities 中國招聘平台、外企高薪職位"},"membership-state":{"code":"member"},"key":2914122},{"show-group-logo-in-profile":false,"group":{"id":911627,"counts-by-category":{"total":2,"count-for-category":[{"category":{"code":"discussion"},"count":10},{"category":{"code":"job"},"count":7}]},"name":"Data Warehouse/Business Intelligence(ETL,Informatica,Business Objects,Cognos) Professionals"},"membership-state":{"code":"member"},"key":911627},{"show-group-logo-in-profile":false,"group":{"id":1491057,"counts-by-category":{"total":2,"count-for-category":[{"category":{"code":"discussion"},"count":0},{"category":{"code":"job"},"count":0}]},"small-logo-url":"https://media.licdn.com/mpr/mpr/p/2/000/017/1f2/227aef7.png","name":"Remote PeopleSoft DBA / Admins worldwide"},"membership-state":{"code":"member"},"key":1491057},{"show-group-logo-in-profile":true,"group":{"id":22348,"counts-by-category":{"total":2,"count-for-category":[{"category":{"code":"discussion"},"count":0},{"category":{"code":"job"},"count":0}]},"small-logo-url":"https://media.licdn.com/mpr/mpr/p/7/005/013/113/0068b50.png","name":"UOW Alumni"},"membership-state":{"code":"member"},"key":22348},{"show-group-logo-in-profile":false,"group":{"id":3424069,"counts-by-category":{"total":2,"count-for-category":[{"category":{"code":"discussion"},"count":1},{"category":{"code":"job"},"count":0}]},"small-logo-url":"https://media.licdn.com/mpr/mpr/p/1/000/071/2e8/39a16e6.png","name":"Oracle DBA Sênior"},"membership-state":{"code":"member"},"key":3424069},{"show-group-logo-in-profile":false,"group":{"id":51442,"counts-by-category":{"total":2,"count-for-category":[{"category":{"code":"discussion"},"count":0},{"category":{"code":"job"},"count":9}]},"name":"Oracle Core and Apps DBA (Database Administrator)"},"membership-state":{"code":"member"},"key":51442},{"show-group-logo-in-profile":false,"group":{"id":77941,"counts-by-category":{"total":2,"count-for-category":[{"category":{"code":"discussion"},"count":5},{"category":{"code":"job"},"count":6}]},"small-logo-url":"https://media.licdn.com/mpr/mpr/p/3/000/006/3c7/3b4e695.png","name":"Oracle Senior DBA Group"},"membership-state":{"code":"member"},"key":77941},{"show-group-logo-in-profile":true,"group":{"id":4265652,"counts-by-category":{"total":2,"count-for-category":[{"category":{"code":"discussion"},"count":0},{"category":{"code":"job"},"count":0}]},"small-logo-url":"https://media.licdn.com/mpr/mpr/p/4/000/12d/0d0/0286c4e.png","name":"UOW Careers Central"},"membership-state":{"code":"member"},"key":4265652},{"show-group-logo-in-profile":true,"group":{"id":129784,"counts-by-category":{"total":2,"count-for-category":[{"category":{"code":"discussion"},"count":0},{"category":{"code":"job"},"count":0}]},"name":"Illawarra IT"},"membership-state":{"code":"member"},"key":129784}];
		
		/*
		//get groups I am a member in
		WebService.invokeGETRequest('linkedin/getGroups', {}, function(data) {
			var result = angular.fromJson(data);
			
			if (result['responseCode'] != '200'){
				toaster.pop('error', result['responseCode'], result['responseBody']);
				deferred.reject(result['responseBody']);
			} else{
				$scope.groups = angular.fromJson(result['responseBody'])['group-memberships']['group-membership'];
				deferred.resolve($scope.groups);
			}
		}, function(data){
			toaster.pop('error', 'Uh,oh', "Can't connect to server");
		});
		*/
		deferred.resolve($scope.groups);
		return deferred.promise;
	};
	
	$scope.getSuggestedGroups = function(){
		var deferred = $q.defer();
		
		WebService.invokeGETRequest('linkedin/getSuggestedGroups', {}, function(data) {
			var result = angular.fromJson(data);
			
			if (result['responseCode'] != '200'){
				toaster.pop('error', result['responseCode'], result['responseBody']);
				deferred.reject(result['responseBody']);
			} else{
				$scope.suggestedGroups = angular.fromJson(result['responseBody'])['groups']['group'];
				deferred.resolve($scope.groups);
			}
			
		}, function(data){
			toaster.pop('error', 'Uh,oh', "Can't connect to server");
		});
		
		return deferred.promise;
	};
	
	$scope.getGroupUpdates = function(){
		//get the groups I am a member in
		$scope.getMyGroups().then(function(){
			$scope.displayController("if_group_updates");
			$scope.module['title'] = "Recent Discussions";
			
			var key = '';
			for(i=0; i < $scope.groups.length; i++){
				key = key + $scope.groups[i]['key'] + ',';
			}
			
			$scope.getGroupDiscussions(key).then(function(data){
				var result = angular.fromJson(data);
				$scope.groupDisscussions = [];
				
				for(i=0; i< $scope.groups.length;i++){
					var length = result[i]['posts']['count'];
					
					for(j=0; j< length; j++){
						result[i]['posts']['post'][j]['group'] = $scope.groups[i];
						result[i]['posts']['post'][j]['timestamp'] = result[i]['posts']['post'][j]['creation-timestamp'];
						result[i]['posts']['post'][j]['title'] = result[i]['posts']['post'][j]['title'].replace($scope.spc,"'"); 
						//result[i]['posts']['post'][j]['summary'] = result[i]['posts']['post'][j]['summary'].replace("&#39;","'"); 
						$scope.groupDisscussions.push(result[i]['posts']['post'][j]);
					}
				}
				
				$scope.displayController("if_group_updates");
				$scope.module['title'] = "Recent Discussions";
			});
		});
		
		//get suggested groups
		$scope.getSuggestedGroups().then(function(){
			$scope.displayController("if_group_updates");
			$scope.module['title'] = "Recent Discussions";
		});
	};
	
	$scope.getGroupDiscussions = function(key){
		var deferred = $q.defer();
		var params = {
				"key": key,
				"type": "lastest", // or "popularity";
		};

		WebService.invokeGETRequest('linkedin/getGroupDiscussions', params, function(data) {
			var result = angular.fromJson(data);	
			
			if (result['responseCode'] != '200'){
				toaster.pop('error', result['responseCode'], result['responseBody']);
				deferred.reject(result['responseBody']);
			} else{
				$scope.groupDisscussionTmp = angular.fromJson(result['responseBody']);
				deferred.resolve($scope.groupDisscussionTmp);
			}
			
		}, function(data){
			toaster.pop('error', 'Uh,oh', "Can't connect to server");
		});
		return deferred.promise;
	};
	
	$scope.addDisscussion = function(){
		var params = {
				"key": $scope.discussion['group'],
				"content": 	"<post>"
						  + "<title>" + $scope.discussion['title'] + "</title>"
						  + "<summary>" + $scope.discussion['title'] + "</summary>"
						  + "</post>"
		};
		
		WebService.invokeGETRequest('linkedin/postDiscussion', params, function(data) {
			var result = angular.fromJson(data);
			
			if (result['responseCode'] != '200'){
				toaster.pop('error', result['responseCode'], result['responseBody']);
			} else{
				$scope.discussion = {};
				toaster.pop('success', result['responseCode'], result['responseBody']);
			}
		}, function(data){
			toaster.pop('error', 'Uh,oh', "Can't connect to server");
		});
	};
	
	$scope.addComment = function(key, type){
		var deferred=  $q.defer();
		var params = {
			"key": key,
			"content":$scope.comment['comment'],
			"type":type,
		};

		WebService.invokeGETRequest('linkedin/addComment', params, function(data) {
			var result = angular.fromJson(data);
			
			if (result['responseCode'] == '200'){
				toaster.pop('success', 'Congrats',result['responseBody']);
			}else{
				toaster.pop('error', result['responseCode'],result['responseBody']);
			}
		});
		
		return deferred.promise;
	};
	
	$scope.addLike = function(field ,type, id){
		var deferred=  $q.defer();
		var params = {
			"key": id,
			"type": type,
			"field": field,
		};
		
		WebService.invokeGETRequest('linkedin/addLike', params, function(data) {
			var result = angular.fromJson(data);
			
			if (result['responseCode'] == '200'){
				// change the like status locally
				if (field == 'company'){
					angular.forEach($scope.companiesUpdates, function(update,index){
						var is_going = true;
						if(is_going){
							if (update['update-key'] == id){
								$scope.companiesUpdates[index]['is-liked'] = (type == 'like' ? 'true' : 'false');
								is_going = false;
							}
						}
					});
				}else if (field == 'discussion'){
					angular.forEach($scope.groupDisscussions, function(discussion,index){
						var is_going = true;
						if(is_going){
							if (discussion['id'] == id){
								$scope.groupDisscussions[index]['relation-to-viewer']['is-liked'] = (type == 'like' ? 'true' : 'false');
								is_going = false;
							}
						}
					});
				}else if(field == 'network'){
					toaster.pop('success', 'Congrats',result['responseBody']);
					angular.forEach($scope.networkUpdates, function(update,index){
						var is_going = true;
						if(is_going){
							if (update['update-key'] == id){
								$scope.networkUpdates[index]['is-liked'] = (type == 'like' ? 'true' : 'false');
								is_going = false;
							}
						}
					});
				}
				
				toaster.pop('success', 'Congrats',result['responseBody']);
			}else{
				toaster.pop('error', result['responseCode'],result['responseBody']);
			}
		});
		
		return deferred.promise;
	};
	
	$scope.addFollow = function(type, id){
		var params = {
			"key": id,
			"type": type,
		};

		WebService.invokeGETRequest('linkedin/addFollow', params, function(data) {
			var result = angular.fromJson(data);

			if (result['responseCode'] == '200'){
				// change the follow status locally
				angular.forEach($scope.groupDisscussions, function(discussion,index){
					var is_going = true;
					if(is_going){
						if (discussion['id'] == id){
							$scope.groupDisscussions[index]['relation-to-viewer']['is-following'] = (type == 'follow' ? 'true' : 'false');
							is_going = false;
						}
					}
				});
				
				toaster.pop('success', 'Congrats',result['responseBody']);
			}else{
				toaster.pop('error', result['responseCode'],result['responseBody']);
			}
		});

	};
	
	$scope.addShare = function(update){
		var template = {
				'size':null,
				'controller':'ViewController',
				'data': {
					"update": update,
					"groups": null,
				},
				
				'url': "modules/linkedin/LinkedinSharePopup.html",
			};	
		
		$scope.getMyGroups().then(function(data){
			template['data']['groups'] = data;
			$scope.popUpView(template);
		}, function(data){
			toaster.pop('error', 'Uh,oh', 'Get groups failed');
			$scope.popUpView(template);
		});
		
	};
	
	$scope.viewCompany = function(companyID){
		var template = {
				'size':'lg',
				'controller':'ViewController',
				'data': {
					"company": null,
				},
				
				'url': "modules/linkedin/company/LinkedinCompanyViewPopup.html",
			};	
		
		WebService.invokeGETRequest('linkedin/getCompanyDetail', {"companyId":companyID}, function(data) {
			var result = angular.fromJson(data);
			
			if (result['responseCode'] == '200'){
				template['data']['company'] = angular.fromJson(result['responseBody'])['company'];
				//toaster.pop('success', result['responseCode'],result['responseBody']['company']);
				$scope.popUpView(template);
			}else{
				toaster.pop('error', result['responseCode'],result['responseBody']);
			}
		});
	}; 
	
	$scope.getMyNetworkUpdates = function(start, count){
		var params = {
			"start": start == 'null' ? '0' : start,
			"count": count == 'null' ? '10': count,
		};
		
		
		WebService.invokeGETRequest('linkedin/getNetworkUpdates', params, function(data) {
			var result = angular.fromJson(data);
			
			if (result['responseCode'] == '200'){
				$scope.module['title'] = "My Network";
				$scope.displayController("if_my_networks");
				$scope.networkUpdates = angular.fromJson(result['responseBody'])['updates']['update'];
			}else{
				toaster.pop('error', result['responseCode'],result['responseBody']);
			}
			
		}, function(data){
			toaster.pop('error', 'Uh,oh', "Can't connect to server");
		});
		
		$scope.getMyNetworkStatistic();
		
	};
	
	$scope.getMyNetworkStatistic = function(){
		WebService.invokeGETRequest('linkedin/getNetworkStatistic', {}, function(data) {
			var result = angular.fromJson(data);
			
			if (result['responseCode'] == '200'){
				$scope.networkStatistic = angular.fromJson(result['responseBody']);
			}else{
				toaster.pop('error', result['responseCode'],result['responseBody']);
			}
			
		}, function(data){
			toaster.pop('error', 'Uh,oh', "Can't connect to server");
		});
	};
	
	$scope.postNetworkUpdate = function(content){
		var params = {
				"content":	  "<activity locale=\"en_US\">"
								+ "<content-type>linkedin-html</content-type>"
								+ "<body>" + content + "</body>"
							+ "</activity>"
		};
		
		WebService.invokeGETRequest('linkedin/postNetworkUpdates', params, function(data) {
			var result = angular.fromJson(data);
			
			if (result['responseCode'] == '200'){
				toaster.pop('success', result['responseCode'],result['responseBody']);
			}else{
				toaster.pop('error', result['responseCode'],result['responseBody']);
			}
			
		}, function(data){
			toaster.pop('error', 'Uh,oh', "Can't connect to server");
		});
	};
	
	$scope.popUpView = function(template){
		var open = $modal.open({
			templateUrl : template['url'],
			controller : template['controller'],
			size:template['size'],
			resolve : {
				data : function() {
					return template['data'];
				}
			}
		});
		
		return open.result;
	};
	
	$scope.search = function(type, key){
		var params = {
				"type": type,
				"keywords":  "keywords=" + key['keyWord']
							+ (key['firstName'] ? "&first-name=" + key['firstName']:"")
							+ (key['lastName'] ? "&last-name=" + key['lastName']:"") 
							+ (key['title'] ? "&title=" + key['title']:"") 
							+ (key['company'] ? "&company-name=" + key['company']:"")
							+ (key['school'] ? "&school-name=" + key['school']:"")
							+ (key['post'] ? "&postal-code=" + key['post']:"")
		};
		
		WebService.invokeGETRequest('linkedin/search', params, function(data) {
			var result = angular.fromJson(data);
			
			if (result['responseCode'] == '200'){
				$scope.peopleSearchResult = angular.fromJson(result['responseBody']);
			}else{
				toaster.pop('error', result['responseCode'],result['responseBody']);
			}
			
		}, function(data){
			toaster.pop('error', 'Uh,oh', "Can't connect to server");
		});
	};
	
	// chenge the content by class id;
	function replaceInfoOfUser(imageLink, name) {
		var img = document.getElementsByClassName('img-circle');
		var headerName = document.getElementsByClassName('hidden-xs');
		var info = document.getElementsByClassName('info'); //only 1 element has this name
		
		for(i=0; i < img.length; i++){
			img[i].src = imageLink;
		}
		
		headerName[1].innerHTML = name;
		info[0].childNodes[1].innerHTML = name;
	}
	
	// function to downlown file and save
	function downloadFile(fileURL, fileName){
		// for non-IE
	    if (!window.ActiveXObject) {
	        var save = document.createElement('a');
	        save.href = fileURL;
	        save.target = '_blank';
	        save.download = fileName || 'unknown';

	        var event = document.createEvent('Event');
	        event.initEvent('click', true, true);
	        save.dispatchEvent(event);
	        (window.URL || window.webkitURL).revokeObjectURL(save.href);
	    }

	    // for IE
	    else if ( !! window.ActiveXObject && document.execCommand)     {
	        var _window = window.open(fileURL, '_blank');
	        _window.document.close();
	        _window.document.execCommand('SaveAs', true, fileName || fileURL)
	        _window.close();
	    }
	}
	
	// Simple function to read URL parameters
	function getUrlParam(name) {

		name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
		var regexS = "[\\?&]" + name + "=([^&#]*)";
		var regex = new RegExp(regexS);
		var results = regex.exec(window.location.href);

		if (results == null)
			return "";
		else
			return results[1];
	};

});

App.register.controller('ViewController', function($scope, $http, $modalInstance, WebService, toaster, data) {
	$scope.update = data ? data['update']: null;
	$scope.company = data ? data['company']: null;
	$scope.groups = data ? data['groups']: null;
	
	$scope.discussion = {
			'title':null,
			'summary':null,
			'group':null,
	};

	if ($scope.update != null){
		var share_type = null;
		var share_name = null;
		switch($scope.update['update-type']){
			case "SHAR":
				share_type = 'person';
				share_name = 'current-share';
				break;
			case "CMPY":
				share_type = 'company-status-update';
				share_name = 'share';
				break;
		};

		$scope.update['title'] = $scope.update['update-content'][share_type][share_name]['content']['title'];
		$scope.update['submitted-image-url'] = $scope.update['update-content'][share_type][share_name]['content']['submitted-image-url'];
		$scope.update['description'] = $scope.update['update-content'][share_type][share_name]['content']['description'];
		$scope.update['submitted-url'] = $scope.update['update-content'][share_type][share_name]['content']['submitted-url'];
		
		$scope.share = {
				"commentTmp": null,
				"comment": null,
				"content": "<content>"
					+ "<title>" + $scope.update['title'] + "</title>" 
					+ "<description>" + $scope.update['description'] + "</description>"
					+ "<submitted-url>" + $scope.update['submitted-url'] + "</submitted-url>"
					+ "<submitted-image-url>" + $scope.update['submitted-image-url'] + "</submitted-image-url>"
					+ "</content>",
				"visibility": "<visibility><code>anyone</code></visibility>",
		};
	}
	
	$scope.postDiscussion = function(content){
		var params = {
				"key": $scope.discussion['group'],
				"content": 	"<post>"
						  + "<title>" + $scope.discussion['title'] + "</title>"
						  + content
						  + "<summary>" + $scope.discussion['title'] + "</summary>"
						  + "</post>"
		};
		
		WebService.invokeGETRequest('linkedin/postDiscussion', params, function(data) {
			var result = angular.fromJson(data);
			
			if (result['responseCode'] != '200'){
				toaster.pop('error', result['responseCode'], result['responseBody']);
			} else{
				$scope.displayController("if_group_updates");
				$scope.module['title'] = "Recent Discussions";
				$scope.discussion = {};
				toaster.pop('success', result['responseCode'], result['responseBody']);
			}
		}, function(data){
			toaster.pop('error', 'Uh,oh', "Can't connect to server");
		});
	};
	
	$scope.addShare = function() {
		$scope.share['comment'] = "<comment>" + $scope.share['commentTmp'] + "</comment>"; 

		WebService.invokeGETRequest('linkedin/addShare', $scope.share, function(data) {
			var result = angular.fromJson(data);
			
			if (result['responseCode'] == '200'){
				toaster.pop('success', 'Congrats',result['responseBody']);
				$modalInstance.close();
			}else{
				toaster.pop('error', result['responseCode'],result['responseBody']);
			}
		});
		
		// Post to group
		if ($scope.discussion['group'] != null & $scope.discussion['title'] != null){
			$scope.postDiscussion($scope.share['content']);
		}
		
	};
	
	$scope.follow = function(type, id){

		WebService.invokeGETRequest('linkedin/followCompany', {"companyId": id, "type": type}, function(data) {
			var result = angular.fromJson(data);
			
			if (result['responseCode'] == '200'){
				toaster.pop('success', 'Congrats',result['responseBody']);
				$modalInstance.close();
			}else{
				toaster.pop('error', result['responseCode'],result['responseBody']);
			}
		});
		
		// Post to group
		if ($scope.discussion['group'] != null & $scope.discussion['title'] != null){
			$scope.postDiscussion($scope.share['content']);
		}
	}
	
	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	};
});
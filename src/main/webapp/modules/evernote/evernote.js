App.register.controller('EvernoteController', function($rootScope, $http,
		$scope, $route, $sce, $location, toaster, WebService, Popup, ModuleProvider) {

	$scope.module = {
		title : 'Evernote Module'
	};

	$scope.hideConnect = false;
	$scope.showNotebooks = true;
	$scope.showNoteContent = false;
	$scope.showEditContent = false;

	$scope.config = function() {

		var token = getUrlParam("token");
		var verifier = getUrlParam("verifier");
		if (token == '' && verifier == '') {
			$scope.hideConnect = false;
			WebService.invokeGETRequest('evernote/config', {}, function(data) {
				$scope.evernoteResult = angular.fromJson(data);
				if ($scope.evernoteResult.authCode == "ALREADY_AUTH") {
					window.location = "/#/evernote/notebooks/";
				}
			}, function(data, status, headers, config) {
				toaster.pop('error', "Uh oh", "Could not get evernote config");
			});
		} else {
			$scope.hideConnect = true;
			var params = {
				"token" : token,
				"verifier" : verifier
			};

			WebService.invokeGETRequest('evernote/redirect', params, function(
					data) {
				$scope.evernoteResult = angular.fromJson(data);
				if ($scope.evernoteResult.accessToken.indexOf("Error") > -1) {
					alert("Error " + $scope.evernoteResult.accessToken);
				} else {
					window.location = "/#/evernote/notebooks/";
				}
			}, function(data, status, headers, config) {
				alert('error' + "Uh oh" + "Could not redirect to evernote" + data + status + headers + config);
			});
		}
	};

	$scope.connect = function() {

		// build the URL for API Authorization
		window.open($scope.evernoteResult.authUrl, "_self", "Evernote", false);
	};

	$scope.notebooks = function(note_id, edit) {

		var id = getUrlParam("id");	
		
		if(note_id == undefined)
			note_id = '';
		
		if (id == '' && note_id == '') {
			$scope.showNotebooks = true;
			WebService.invokeGETRequest('evernote/notebooks', {}, function(data) {
				if (data != "" && data != null) {
					$scope.evernoteNotebooks = angular.fromJson(data);
				} else {
					$scope.evernoteNotebooks = null;
				}

				if ($scope.evernoteNotebooks == null) {
					toaster.pop('error', "Uh oh", "No evernote notebooks");
				}
			}, function(data, status, headers, config) {
				toaster.pop('error', "Uh oh", "Could not get evernote notebooks"
						+ data);
			});
		}
		else if (id != '' && note_id == ''){	
			$scope.showNotebooks = true;
			$scope.showNoteContent = false;
			
			var params = {
				"guid" : id
			};
			
			WebService.invokeGETRequest('evernote/notes', params, function(data) {
				if (data != "" && data != null) {
					var name = getUrlParam("name");
					
					$scope.notebookNotes = angular.fromJson(data);
					$scope.notebookName = decodeURIComponent(name);
					$scope.showNotebooks = false;
				}
				else{
					$scope.notebookNotes = null;
				}
				
				if ($scope.notebookNotes == null) {
					toaster.pop('error', "Uh oh", "No evernote notes for this notebook");
				}					
			}, function(data, status, headers, config) {
				toaster.pop('error', "Uh oh", "Could not get task config");
			});	
		}
		else{
			$scope.showNotebooks = false;
			$scope.showEditContent = false;
			$scope.showNoteContent = false;
								
			var params = {
				"guid" : note_id
			};
			
			WebService.invokeGETRequest('evernote/note', params, function(data) {
				if (data != "" && data != null) {
					$scope.currentNote = angular.fromJson(data);
					
					if($scope.currentNote.resources == null){
						$scope.currentNoteContent = $sce.trustAsHtml($scope.currentNote.content);					
					}
					else if($scope.currentNote.resources.length > 0){
						for (var i = 0; i < $scope.currentNote.resources.length; i++) {
							if($scope.currentNote.resources[i].mime == "image/png"){
								var indexStart = $scope.currentNote.content.indexOf("<en-media");
								var indexEnd = $scope.currentNote.content.indexOf("</en-media>");
								
								var replacementHtml = $scope.currentNote.content.substr(indexStart, 
										indexEnd + 11 - indexStart);
								
								var correctedHtml = $scope.currentNote.content.replace(replacementHtml,
										"<img src='data:image/jpeg;base64,"+ 
										$scope.currentNote.resources[i].data.body +"' />");
								
								$scope.currentNoteContent = $sce.trustAsHtml(correctedHtml);
							}
							else{
								var fileName = $scope.currentNote.resources[i].attributes.fileName;
								var byteCharacters = atob($scope.currentNote.resources[i].data.body);
								
								var byteNumbers = new Array(byteCharacters.length);
								for (var i = 0; i < byteCharacters.length; i++) {
								    byteNumbers[i] = byteCharacters.charCodeAt(i);
								}
								
								var byteArray = new Uint8Array(byteNumbers);
								
								var a = window.document.createElement('a');
								a.href = window.URL.createObjectURL(new Blob(
										[ byteArray ],
										{
											type : 'application/octet-stream'
										}));	
								
								var fileHtmlDownload = "<a href='"+ a +
										 "' download='"+ fileName +"'><b>"+ fileName +"</b></a>";
																
								var indexStart = $scope.currentNote.content.indexOf("<en-media");
								var indexEnd = $scope.currentNote.content.indexOf("</en-media>");
								
								var replacementHtml = $scope.currentNote.content.substr(indexStart, 
										indexEnd + 11 - indexStart);
								
								var correctedHtml = $scope.currentNote.content.replace(replacementHtml,
										fileHtmlDownload);

								// Append anchor to body.
								$scope.currentNoteContent = $sce.trustAsHtml(correctedHtml);																
							}
						}
					}
				}
				else{
					$scope.currentNote = null;
				}
				
				if ($scope.currentNote == null) {
					toaster.pop('error', "Uh oh", "No evernote content for this note");
				}
				
				if(edit){
					$scope.showEditContent = true;
				}
				else{
					$scope.showNoteContent = true;
				}
			}, function(data, status, headers, config) {
				toaster.pop('error', "Uh oh", "Could not get task config");
			});	
		}			
	};
	
	$scope.updateNote = function() {
		if ($scope.currentNote == undefined)
			return;
			
		var content = $('#some-textarea').val().replace(/<br>/g, "<br/>");
		content = content.replace(/&nbsp;/g, "");
		
		var params = {
			"guid" : $scope.currentNote.guid,
			"content" : content
		};
		
		WebService.invokePOSTRequest('evernote/note', params, function(data) {
			$scope.postUpdateNote = data;
		
			if ($scope.postUpdateNote == "NO_AUTH_TOKEN") {
				window.location = "/#/evernote";
			} else if ($scope.postUpdateNote.indexOf("Error") > -1) {
				alert("Error " + $scope.postUpdateNote);
			} else {
				toaster.pop('Update the note sucessful', "Yeah",
						"Your note has been updated succesfully");
			}
			
			$scope.showNotebooks = false;
			$scope.showEditContent = false;
			$scope.showNoteContent = false;
		}, function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not post tweet");
		});			
	};

	// Simple function to read URL parameters
	function getUrlParam(name) {

		name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
		var regexS = "[\\?&]" + name + "=([^&#]*)";
		var regex = new RegExp(regexS);
		var results = regex.exec(window.location.href);

		if (results == null)
			return "";
		else
			return results[1];
	}
});
App.register.controller('ViewModifyGroupsController', function ($http, $scope, $route, $location, WebService, ModuleProvider, Popup, toaster) {
    $scope.module = {
        title: 'Manage Groups'
    };

    $scope.allGroups = [];

    $scope.listAllGroups = function () {

        WebService.invokePOSTRequest('user/viewAllGroups', {}, function (data) {
            $scope.allGroups = angular.fromJson(data);
        }, function (data) {
            toaster.pop('error', "Uh oh", "Could not retrieve roles");
        });
    };

    // remove user
    $scope.removeGroup = function (data) {
        Popup.show('Delete Group', 'Deleting the group would delete all permissions assigned to that group. Are you sure?', 'yesno', 'medium')

		.then(function (result) {
		    if (result == 'yes') {
		        var param = {
		            'name': data.name
		        };

		        WebService.invokePOSTRequest('user/deleteAGroup', param, function (data) {
		            $scope.listAllGroups();
		            toaster.pop('success', "Success", "Successfully deleted group");
		        }, function (data, status, headers, config) {
		            toaster.pop('error', "Uh oh", "Could not delete group");
		        });
		    }
		});
    };

    $scope.saveGroup = function (r, data) {
        // Create new role
        if (r.unsaved) {

            var param = {
                'name': data.name,
                'description': data.description || '',
            };

            WebService.invokePOSTRequest('user/createAGroup', param, function (data) {
                $scope.listAllGroups();
                toaster.pop('success', "Success", "Successfully created group");
            }, function (data, status, headers, config) {
                toaster.pop('error', "Uh oh", "Could not create group");
            });
        }
            // Update role
        else {
            var param = {
                'id': r.id,
                'name': data.name,
                'description': data.description || ''
            };

            WebService.invokePOSTRequest('user/updateAGroup', param, function (data, status, headers) {
                $scope.listAllGroups();
                toaster.pop('success', "Success", "Successfully updated group");
            }, function (data, status, headers, config) {
                toaster.pop('error', "Uh oh", "Could not update group");
            });
        }
    };

    $scope.addNewGroup = function () {
        $scope.inserted = {
            name: null,
            description: null,
            unsaved: true
        };
        $scope.allGroups.push($scope.inserted);
    };

    $scope.validateName = function (data) {
        if (!data || data.length <= 0) {
            return "Specify group name";
        }
    };

    $scope.cancel = function (rowform, data) {
        rowform.$cancel();
    };

    $scope.listAllGroups();
});
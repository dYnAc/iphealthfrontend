var App = angular
		.module('Trunk', [ 'ngRoute', 'angular-loading-bar', 'ui.bootstrap',
				'Trunk.providers', 'ngCookies', 'ngAnimate', 'ngSanitize',
				'toaster', 'nvd3ChartDirectives', 'xeditable', 'ngTagsInput',
				'ngTable', 'ui.sortable', 'base64', 'textAngular', 'ui.select',
				'ui.bootstrap.datetimepicker', 'oc.lazyLoad', 'facebook', 
				'directive.g+signin', 'ngLinkedIn' ]);

App.run(function($location, $rootScope, ModuleProvider, Session,
		editableOptions) {

	// set ng-table theme
	editableOptions.theme = 'bs3';

	// Intercept page navigation and detect login checks
	$rootScope.$on('$routeChangeStart', function(event, currRoute, prevRoute) {

		if (!currRoute.freeAccess && !Session.loggedIn) {
			var branchName = Session.branchUniqueName || 'undefined';

			$location.path('/login/' + branchName);
			return;
		}

		if (!currRoute.freeAccess && Session.loggedIn) {
			return;
		}

		if (currRoute.freeAccess && Session.loggedIn) {
			$location.path('/dashboard');
			return;
		}
	});

	// http://stackoverflow.com/questions/14898296/how-to-unsubscribe-to-a-broadcast-event-in-angularjs-how-to-remove-function-reg
	// Custom $off function to un-register the listener.
	$rootScope.$off = function(name, listener) {
		var namedListeners = this.$$listeners[name];
		if (namedListeners) {
			// Loop through the array of named listeners and remove them from
			// the array.
			for (var i = 0; i < namedListeners.length; i++) {
				if (namedListeners[i] === listener) {
					return namedListeners.splice(i, 1);
				}
			}
		}
	};
});

App.config(function(controllerProvider, $locationProvider, $routeProvider,
		$controllerProvider, $compileProvider, $filterProvider, $provide,
		$httpProvider) {

	App.register = {
		routeProvider : $routeProvider,
		service : $provide.service,
		factory : $provide.factory,
		constant : $provide.constant,
		filter : $filterProvider.register,
		directive : $compileProvider.directive,
		controller : $controllerProvider.register,
		modulecontroller : controllerProvider.register
	};

	$locationProvider.html5Mode(false);

	$routeProvider.when('/login/:branchUniqueName', {
		templateUrl : 'login.html',
		controller : 'LoginController',
		freeAccess : true
	});

	$routeProvider.when('/createtenant', {
		templateUrl : 'createtenant.html',
		controller : 'CreateTenantController',
		freeAccess : true
	});

	$routeProvider.when('/dashboard', {
		templateUrl : 'dashboard.html',
		controller : 'DashboardController'
	});

	$routeProvider.otherwise({
		redirectTo : '/login/undefined'
	});
});

App.config([ '$ocLazyLoadProvider', function($ocLazyLoadProvider) {
	$ocLazyLoadProvider.config({
		asyncLoader : $script
	});
} ]);

App.config([ 'FacebookProvider', function(FacebookProvider) {
	var myAppId = '371277166373962';

	// You can set appId with setApp method
	// FacebookProvider.setAppId('myAppId');

	/**
	 * After setting appId you need to initialize the module. You can pass the
	 * appId on the init method as a shortcut too.
	 */
	FacebookProvider.init(myAppId);

} ]);

App.config([ '$linkedInProvider', function($linkedInProvider) {
   $linkedInProvider.set('appKey', '75p0zfr9zn2v1u');
   $linkedInProvider.set('scope', 'r_basicprofile r_emailaddress');
} ]);
App.controller('TrunkController', function($http, $templateCache, $rootScope, $scope, PageBackground, 
			   ModuleProvider, Themes, WebService, Workflow, Popup, Session, Facebook, $linkedIn) {
	$scope.pageLoading = true;

	Workflow.stopWorkflow();

	$scope.theme = Themes;
	$scope.session = Session;
	$scope.webService = WebService;
	$scope.moduleProvider = ModuleProvider;
	$scope.pageBackground = PageBackground;
	
	/*********************** LINKEDIN LOGIN *************************/
	$scope.logoutLinkedIn = function() {
		$linkedIn.logout();
		//console.log("Logout from linkedIn");
    };   
	/*********************** END LINKEDIN LOGIN *************************/
	
	/*********************** GOOGLE LOGIN *************************/
	$scope.logoutGoogle = function() {		
		gapi.auth.signOut();
		//console.log("Logout from google");
		$rootScope.loggedGoogle = false;
    };   
	/*********************** END GOOGLE LOGIN *************************/
	
	/*********************** FACEBOOK LOGIN *************************/
	$scope.logoutFacebook = function() {
     	Facebook.getLoginStatus(function(response) {
 	      if (response.status == 'connected') {
       		Facebook.logout(function() {   
       			$scope.$apply(function() {    
       				$rootScope.loggedFacebook = false;
	       			//console.log("Logout from facebook");
       			});
       		});
 	      }
 	    });
     };    
     /*********************** END FACEBOOK LOGIN *************************/
     
	$scope.logout = function() {
		Popup.show('Logout', 'Are you sure you want to logout?', 'yesno').then(function(result) {
			if (result == 'yes') {	
				$scope.logoutFacebook();
				$scope.logoutGoogle();
				$scope.logoutLinkedIn();
				ModuleProvider.logout();
			}
		});
	};

	$scope.pageLoading = false;

	// uAdmin START

	$rootScope.style = 'style1';
	$rootScope.theme = 'pink-blue';

	$scope.data = {};
	$scope.effect = '';
	$scope.header = {
		form : false,
		chat : false,
		theme : false,
		footer : true,
		history : false,
		animation : '',
		boxed : '',
		layout_menu : '',
		theme_style : 'style1',
		// header_topbar : 'static',
		menu_style : 'sidebar-default',
		layout_horizontal_menu : '',

		toggle : function(k) {
			switch (k) {
			case 'chat':
				$scope.header.chat = !$scope.header.chat;
				break;
			case 'form':
				$scope.header.form = !$scope.header.form;
				break;
			case 'sitebar':
				$scope.header.header_topbar = $scope.header.header_topbar ? '' : (($scope.header.layout_menu === '') ? 'sidebar-collapsed' : 'right-side-collapsed');
				break;
			case 'theme':
				$scope.header.theme = !$scope.header.theme;
				break;
			case 'history':
				$scope.header.history = !$scope.header.history;
				$scope.header.menu_style = $scope.header.history ? 'sidebar-collapsed' : 'sidebar-default';
				break;
			}
		}
	};

	$scope.$on('$routeChangeSuccess', function(event, current, previous) {

		$scope.header.animation = 'fadeInUp';
		setTimeout(function() {
			$scope.$apply(function() {
				$scope.header.animation = '';
			});
		}, 3000);

		$scope.header.boxed = '';
		$scope.header.footer = true;

		$rootScope.style = 'style1';
		$rootScope.theme = 'blue-grey';
	});

	$scope.style_change = function() {
		$rootScope.style = $scope.header.theme_style;
	};

	$scope.theme_change = function(t) {
		$rootScope.theme = t;
	};

	$(window).scroll(function() {
		if ($(this).scrollTop() > 0) {
			$('.quick-sidebar').css('top', '0');
		} else {
			$('.quick-sidebar').css('top', '50px');
		}
	});
	$('.quick-sidebar > .header-quick-sidebar').slimScroll({
		"height" : $(window).height() - 50,
		'width' : '280px',
		"wheelStep" : 5
	});
	$('#news-ticker-close').click(function(e) {
		$('.news-ticker').remove();
	});

	// uAdmin END
});

App.controller('SideMenuController', function($http, $rootScope, $scope) {

	$scope.menus = {
		'dash' : true,
	};

	$scope.parentMenuClick = function(menu) {
		$scope.closeOthers();

		$scope.menus[menu] = !$scope.menus[menu];
	};

	$scope.generatedParentMenuClick = function(sup) {
		var from = sup.collapse;

		$scope.closeOthers();

		sup.collapse = !from;
	};

	$scope.closeOthers = function() {
		angular.forEach($scope.moduleProvider.sideMenus, function(sm) {
			sm.collapse = true;
		});
	};
});

App.controller('CreateTenantController', function($http, $scope, $location, WebService, toaster) {
	$scope.createTenant = function() {

		if (!$scope.tName || !$scope.fName || !$scope.uName || !$scope.pass || !$scope.tUniqueName) {
			return;
		}

		var params = {
			"tName" : $scope.tName,
			"tUniqueName" : $scope.tUniqueName,
			"fName" : $scope.fName,
			"uName" : $scope.uName,
			"pass" : $scope.pass,
		};

		WebService.invokePOSTRequest('authentication/createTenant', params, function(data) {
			$location.path('/login/' + $scope.tUniqueName);
			toaster.pop('success', "Successfully created tenant", "You can login now. Your branch unique name is " + $scope.tUniqueName);
		}, function(data) {
			toaster.pop('error', "Uh oh", "Could not create tenant");
		}, function() {

		}, true);
	};
});

App.controller('LoginController', function($http, $routeParams, $rootScope, $scope, $location, 
										   ModuleProvider, WebService, Facebook, $linkedIn) {
	$scope.branchUniqueName = $routeParams.branchUniqueName;

	$scope.webService = WebService;
	$scope.moduleProvider = ModuleProvider;
	$scope.specifyBranchUniqueName = false;

	if ($scope.branchUniqueName == 'undefined') {
		$scope.specifyBranchUniqueName = true;
	}

	$scope.login = function(username, password) {
		ModuleProvider.login(username, password, $scope.branchUniqueName);
	};

	$scope.closeAlert = function() {
	};
	
	/********************* LINKED IN LOGIN ************************/
	// Defining user logged status
	$scope.loggedLinkedIn = false;
        
    /**
    * IntentLogin
    */
    $scope.intentLoginLinkedIn = function() {
    	$scope.loginLinkedIn();
    };
    
    /**
    * Login
    */
    $scope.loginLinkedIn = function() {
      if(!IN.User.isAuthorized()){
    	  $linkedIn.authorize(function(response) {
    		  if (response == true) {
    			  $scope.loggedLinkedIn = true;
    			  $scope.meLoginLinkedIn();
	          } 
    	  });
      }
      else {
    	  $scope.loggedLinkedIn = true;
		  $scope.meLoginLinkedIn();
	  }
    };
     
    /**
    * me 
    */
    $scope.meLoginLinkedIn = function() {
    	$linkedIn.meProfile(function(response) {
    		//console.log(JSON.stringify(response));
    		
    		ModuleProvider.loginAlternative(response.values[0].firstName, response.values[0].emailAddress,
    				   			$scope.branchUniqueName);
    	});
     };	
	/**********************END LINKED IN LOGIN ********************/
	
	/*********************** GOOGLE LOGIN *************************/
	 // Defining user logged status
	$rootScope.loggedGoogle = false;
    
    $scope.$on('event:google-plus-signout-success', function (event, authResult) {    	
    	$rootScope.$apply(function() { 
    		$rootScope.loggedGoogle = false;
    	});
	});
    
    $scope.$on('event:google-plus-signin-success', function (event, authResult) {
    	// Send login to server or save into cookie
    	$rootScope.$apply(function() {
    		$rootScope.loggedGoogle = true;
    	});
    	
    	gapi.client.load('plus', 'v1', function(){   	
	    	var request = gapi.client.plus.people.get({
			   'userId': 'me'
		    });
		    request.execute(function(response) {
		      //console.log("successful" + JSON.stringify(response));
		      
			  ModuleProvider.loginAlternative(response.name.givenName, response.emails[0].value,
			   			$scope.branchUniqueName);			  
		    });
    	  }
    	); 
	});
    
	$scope.$on('event:google-plus-signin-failure', function (event, authResult) {
		// Auth failure or signout detected
		$rootScope.$apply(function() {
			$rootScope.loggedGoogle = false;
		});
	});
	/*********************** END GOOGLE LOGIN *************************/	
	
	/*********************** FACEBOOK LOGIN *************************/
    // Defining user logged status
	$rootScope.loggedFacebook = false;
    
    /**
    * Watch for Facebook to be ready.
    * There's also the event that could be used
    */
    $scope.$watch(
      function() {
        return Facebook.isReady();
      },
      function(newVal) {
        if (newVal)
          $scope.facebookReady = true;
      }
    );
        
    Facebook.getLoginStatus(function(response) {
      if (response.status == 'connected') {
    	  $rootScope.loggedFacebook = true;
      }
    });
    
    /**
    * IntentLogin
    */
    $scope.intentLoginFacebook = function() {
    	$scope.loginFacebook();
    };
    
    /**
    * Login
    */
    $scope.loginFacebook = function() {
      if(!$rootScope.loggedFacebook){
	      Facebook.login(function(response) {
    		  if (response.status == 'connected') {
    			  $rootScope.loggedFacebook = true;
    			  $scope.meLoginFacebook();
	          }   
	     });	     
      }
      else {
    	  $rootScope.loggedFacebook = true;
		  $scope.meLoginFacebook();
	  }
    };
     
    /**
    * me 
    */
    $scope.meLoginFacebook = function() {
      Facebook.api('/me', function(response) {	  
   	      /**
	      * Using $scope.$apply since this happens outside angular framework.
	      */		 
	      $scope.$apply(function() {    
		   	  //console.log(JSON.stringify(response));
		   	  
		   	  ModuleProvider.loginAlternative(response.first_name, response.email,
		   			$scope.branchUniqueName);
	      });   
      });
     };
	 /******************* END FACEBOOK LOGIN *******************/
});

App.controller('DashboardController', function($rootScope, $http, $scope, $route, $location) {

	$scope.jmpinstance = {
		'instance' : null
	};

	$scope.next = function() {
		$scope.jmpinstance.instance.jmpress("next");
	};

	$scope.prev = function() {
		$scope.jmpinstance.instance.jmpress("prev");
	};

	$scope.$on('$locationChangeStart', function() {
		$scope.jmpinstance.instance.jmpress('deinit');
	});

});
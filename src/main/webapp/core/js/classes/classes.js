var DEBUG = true;

if (!DEBUG) {
	console = {
		log : function() {
		}
	};
}

/*------------------------------------------------------*/

if (typeof String.prototype.startsWith != 'function') {
	String.prototype.startsWith = function(str) {
		if (str == null)
			return false;
		var i = str.length;
		if (this.length < i)
			return false;
		for (--i; (i >= 0) && (this[i] === str[i]); --i)
			continue;
		return i < 0;
	}
}

String.prototype.capitalize = function() {
	return this.replace(/^./, function(match) {
		return match.toUpperCase();
	});
};

Array.prototype.containsHard = function(v, comparer) {
	var conts = false;

	angular.forEach(this, function(i) {
		if (comparer(i, v)) {
			conts = true;
		}
	});

	return conts;
};

Array.prototype.removeElementHard = function(v, comparer) {
	var index = 0;
	var self = this;

	angular.forEach(this, function(i) {
		if (comparer(i, v)) {
			self.splice(index, 1);
		}

		index = index + 1;
	});
};

Array.prototype.contains = function(v) {
	return this.indexOf(v) > -1;
};

Array.prototype.removeElement = function(v) {
	var index = this.indexOf(v);

	if (index != -1) {
		this.splice(index, 1);
	}
};

Array.prototype.pushsafe = function(v) {
	if (!this.contains(v)) {
		this.push(v);
		return true;
	}

	return false;
};

Array.prototype.pushHard = function(v, comparer) {

	if (this.containsHard(v, comparer) == false) {
		this.push(v);
	}
};

/*------------------------------------------------------*/

function Timer() {
};

Timer.start = function(str) {
	if (DEBUG) {
		console.time(str);
	}
};

Timer.end = function(str) {
	if (DEBUG) {
		console.timeEnd(str);
	}
};
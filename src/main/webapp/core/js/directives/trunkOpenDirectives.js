App.directive('wffeatureidentifier', function($timeout) {
	return {
		link : function(scope, element, attrs) {
			var id = attrs.wffeatureidentifier;

			scope['__id__'] = id;
		}
	};
});

App.directive('ngTrunkInclude', function() {
	return {
		restrict : 'E',
		replace : true,
		scope : {
			params : '=params',
			view : '=view',
			featureincrementedid : '=featureincrementedid'
		},
		controller : function($scope) {
			this.params = $scope.params;
		},
		template : '<div wffeatureidentifier="{{featureincrementedid}}" ng-include="getTemplate()"></div>',
		link : function(scope, element, attrs) {
			scope.getTemplate = function() {
				return scope.view;
			};
		}
	};
});

App.directive('moduleUiEnabler', function(ModuleProvider, Workflow) {
	return {
		restrict : 'E',
		replace : true,
		scope : {
			mname : '@name',
		},
		controller : function($scope) {
			this.scope = $scope;
		},
		link : function(scope, element, attrs) {
			// Show if user has access to module or if executed as work flow
			var show = ModuleProvider.userHasModulePermission(scope.mname) || Workflow.isExecutingWorkflow();

			// Hide if user don't have access to this module
			if (!show) {
				element.remove();
			}
		}
	};
});

App.directive('featureUiEnabler', function(ModuleProvider, Workflow) {
	return {
		restrict : 'E',
		replace : true,
		require : '^moduleUiEnabler',
		scope : {
			fname : '@name',
		},
		link : function(scope, element, attrs, moduleUiEnablerCtrl) {
			// Show if user has access to feature or if executed as work flow
			var show = ModuleProvider.userHasModuleFeaturePermission(moduleUiEnablerCtrl.scope.mname, scope.fname) || Workflow.isExecutingWorkflow();

			// Hide if user don't have access to this feature
			if (!show) {
				element.remove();
			}
		}
	};
});

App.directive('a', function() {
    return {
        restrict: 'E',
        link: function(scope, elem, attrs) {
            if(attrs.ngClick || attrs.href === '' || attrs.href === '#'){
                elem.on('click', function(e){
                    e.preventDefault();
                });
            }
        }
   };
});

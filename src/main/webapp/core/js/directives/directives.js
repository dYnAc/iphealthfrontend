App.directive('ticker', function(safeApply) {
	return {
		restrict : 'A',
		link : function(scope, element, attrs) {

			var increment = function() {

				var nextval = data.value + data.step;
				nextval = nextval >= data.end ? data.end : nextval;

				try {
					safeApply(scope, function() {
						data.value = nextval;
					});
				} catch (e) {
					return;
				}

				if (data.value < data.end) {
					setTimeout(increment, data.delay)
				}
			};

			var dataname = attrs['tickerData'];
			var start = parseInt(attrs['tickerBegin']);
			var end = parseInt(attrs['tickerEnd']);
			var step = parseInt(attrs['tickerStep']);
			var duration = parseInt(attrs['tickerDuration']);

			var delay = duration / ((end - start) / step);

			var data = scope[dataname];
			if (!data) {
				data = {
					'delay' : delay,
					'value' : start,
					'step' : step,
					'end' : end
				};
			}

			scope[dataname] = data;
			setTimeout(increment, delay);
		}
	};
});

App.directive('pageheading', function() {
	return {
		restrict : 'A',
		transclude : true,
		template : '<div class="row page-title-breadcrumb noPadding"><div class="col-lg-12 padding10 leftPadding20">' + '<div class="page-header pull-left"><div class="page-title animated fadeInRight"><span ng-transclude>'
				+ '</span></div></div><paper-ripple fit></paper-ripple></div></div><br /><br />'
	};
});

App.directive('vidloop', function($timeout) {
	return {
		restrict : 'A',
		scope : true,
		link : function(scope, element, attrs) {
			element.bind('loadeddata', function(e) {
				element.bind('onended', function() {
					element.play();
				});
			});
		}
	};
});

App.directive('zoom', function($parse) {
	return {
		restrict : 'A',
		scope : true,
		link : function(scope, element, attrs) {

			var elid = attrs.zoom;
			var canvas = angular.element(elid);

			scope.onchange = function() {
				var zoom = element.val();
				var zoomper = zoom / 100;
				var fontsize = zoom < 75 ? 8.5 : 11;

				scope.$apply(function() {
					scope.zoom = zoom;
				});

				canvas.css({
					'zoom' : zoom + "%",
					'-moz-transform' : 'scale(' + zoomper + ')',
					'-moz-transform-origin' : 'left center',
					'font-size' : fontsize + "px"
				});
			}

			element.bind('mousemove', scope.onchange);
			element.bind('change', scope.onchange);
		}
	};
});

App.directive('jmpress', function($parse) {
	return {
		restrict : 'A',
		scope : true,
		link : function(scope, element, attrs) {

			scope[attrs.jmpress].instance = element;

			element.css({
				'outline' : 'none'
			});

			element.jmpress({

				viewPort : {
					height : false,
					width : false,
					maxScale : 1
				},

				fullscreen : false,

				hash : {
					use : false
				},

				mouse : {
					clickSelects : true
				},

				keyboard : {
					use : true
				},

				animation : {
					transitionDuration : '1s'
				}
			});
		}
	};
});

App.directive('draggabledroppable', function($document, safeApply) {
	return {
		restrict : 'A',
		scope : {
			draghandler : '=',
			drophandler : '=',
			dragendhandler : '=',
			dragginghandler : '=',
			dragdata : '=',
			dropdata : '='
		},
		link : function(scope, element, attrs) {

			// Check if the html element is binded to model
			scope.$watch(function() {
				if (!scope.dragdata.$$element) {
					scope.dragdata.$$element = element;
				}
			});

			if (!(attrs.draggabledroppable == 'true')) {
				return;
			}

			element[0].draggable = true;
			scope.dragstarted = false;

			// Uncomment for debug
			element.bind('click', function() {
				console.log(scope.dragdata);
			});

			scope.handleDrop = function(e) {
				e.preventDefault();
				e.stopPropagation();

				// event
				scope.drophandler(scope.dropdata, e);
			};

			scope.handleDragOver = function(e) {
				e.preventDefault();
				e.dataTransfer.dropEffect = 'link';

				return false;
			};

			scope.handleDragStart = function(e) {
				this.style.opacity = '1.0';

				// event
				scope.draghandler(scope.dragdata, e);
			};

			scope.handleDragEnd = function(e) {
				this.style.opacity = '1.0';
				scope.dragstarted = false;

				// event
				scope.dragendhandler(scope.dragdata, e);
			};

			scope.handleDrag = function(e) {

				var e2 = {
					x : 0,
					y : 0
				};

				if (!scope.dragstarted) {
					scope.canvaspos = element[0].parentElement.parentElement.getBoundingClientRect();

					// use center of drag element as initial point
					e2.x = scope.dragdata.x + (scope.dragdata.width / 2);
					e2.y = scope.dragdata.y + (scope.dragdata.height / 2);

					scope.dragstarted = true;
				} else {
					e2.x = e.x - scope.canvaspos.left;
					e2.y = e.y - scope.canvaspos.top;
				}

				// event
				scope.dragginghandler(scope.dragdata, e2);
			};

			element[0].addEventListener('dragstart', scope.handleDragStart, false);
			element[0].addEventListener('dragend', scope.handleDragEnd, false);

			element[0].addEventListener('drag', scope.handleDrag, false);

			element[0].addEventListener('drop', scope.handleDrop, false);
			element[0].addEventListener('dragover', scope.handleDragOver, false);
		}
	};
});

App.directive('droppable', function() {
	return {
		restrict : 'A',
		scope : {
			drophandler : '=',
			dropdata : '='
		},
		link : function(scope, element, attrs) {
			scope.handleDrop = function(e) {
				e.preventDefault();
				e.stopPropagation();

				// event
				scope.drophandler(scope.dropdata, e);
			};

			scope.handleDragOver = function(e) {
				e.preventDefault();
				e.dataTransfer.dropEffect = 'move';
				return false;
			};

			element[0].addEventListener('drop', scope.handleDrop, false);
			element[0].addEventListener('dragover', scope.handleDragOver, false);
		}
	};
});

App.directive('draggable', function() {
	return {
		restrict : 'A',
		scope : {
			draghandler : '=',
			dragdata : '='
		},
		link : function(scope, element, attrs) {

			scope.handleDragStart = function(e) {
				this.style.opacity = '0.8';

				// event
				scope.draghandler(scope.dragdata, e);
			};

			scope.handleDragEnd = function(e) {
				this.style.opacity = '1.0';
			};

			// TODO: Fix drag and drop on iPad
			scope.handleStart = function(e) {
				e.preventDefault();
			};

			element[0].addEventListener("touchstart", scope.handleStart, false);

			element[0].addEventListener('dragstart', scope.handleDragStart, false);
			element[0].addEventListener('dragend', scope.handleDragEnd, false);
		}
	};
});

App.directive('movable', function($document) {
	return {
		restrict : 'A',
		scope : {
			position : '=',
			move : '@',
			movehandler : '=',
		},
		link : function(scope, element, attr) {

			if (!(attr.movable == 'true')) {
				return;
			}

			var handle = element.find('.' + scope.move);
			var startX = 0, startY = 0, x = scope.position.x || 0, y = scope.position.y || 0;

			handle.css({
				cursor : 'move'
			});

			handle.on('mousedown', function(event) {
				event.preventDefault();

				startX = event.pageX - x;
				startY = event.pageY - y;

				$document.on('mousemove', mousemove);
				$document.on('mouseup', mouseup);

				scope.movehandler('down', scope.position, x, y, 1000);
			});

			function mousemove(event) {
				y = event.pageY - startY;
				x = event.pageX - startX;

				scope.movehandler('move', scope.position, x, y, 1000);
			}

			function mouseup() {
				$document.unbind('mousemove', mousemove);
				$document.unbind('mouseup', mouseup);

				scope.movehandler('up', scope.position, x, y, 0);
			}
		}
	};
});

App.directive('fullscreen', function($document) {
	return {
		restrict : 'A',
		scope : {
			elemid : '@',
			callback : '='
		},
		link : function(scope, element, attrs) {
			var elem = document.querySelector('#' + scope.elemid);

			scope.isFullScreen = function() {
				if (!document.fullscreenElement && !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement) {
					return false;
				} else {
					return true;
				}
			};

			element.bind('click', function() {

				if (!scope.isFullScreen()) {

					if (elem.requestFullscreen) {
						elem.requestFullscreen();
					} else if (elem.msRequestFullscreen) {
						elem.msRequestFullscreen();
					} else if (elem.mozRequestFullScreen) {
						elem.mozRequestFullScreen();
					} else if (elem.webkitRequestFullscreen) {
						elem.webkitRequestFullscreen();
					}

					if (scope.callback) {
						scope.callback(true);
					}
				} else {
					if (document.exitFullscreen) {
						document.exitFullscreen();
					} else if (document.mozCancelFullScreen) {
						document.mozCancelFullScreen();
					} else if (document.webkitExitFullscreen) {
						document.webkitExitFullscreen();
					}

					if (scope.callback) {
						scope.callback(false);
					}
				}
			});

			scope.fullscreenChanged = function(e) {
				if (scope.callback) {
					scope.callback(scope.isFullScreen());
				}
			};

			// Callback for full screen change
			document.addEventListener("fullscreenchange", scope.fullscreenChanged);
			document.addEventListener("mozfullscreenchange", scope.fullscreenChanged);
			document.addEventListener("webkitfullscreenchange", scope.fullscreenChanged);
			document.addEventListener("msfullscreenchange", scope.fullscreenChanged);
		}
	};
});

App.directive('resizable', function($window) {
	return {
		restrict : 'A',
		scope : {
			heightPadding : '=',
			widthPadding : '=',
		},
		replace : false,
		link : function(scope, elem) {

			scope.initializeWindowSize = function() {
				scope.widthPadding = scope.widthPadding ? scope.widthPadding : 0;
				scope.heightPadding = scope.heightPadding ? scope.heightPadding : 0;

				scope.windowHeight = $window.innerHeight - scope.heightPadding;
				scope.windowWidth = $window.innerWidth - scope.widthPadding;

				elem.height(scope.windowHeight);
				// elem.width(scope.windowWidth);
			};

			scope.$watch('heightPadding', function() {
				scope.initializeWindowSize();
			});

			scope.$watch('widthPadding', function() {
				scope.initializeWindowSize();
			});

			angular.element($window).bind('resize', function() {
				scope.initializeWindowSize();
			});

			scope.initializeWindowSize();
		}
	};
});

App.directive('resizer', function($document) {

	return function($scope, $element, $attrs) {

		$element.on('mousedown', function(event) {
			event.preventDefault();

			$document.on('mousemove', mousemove);
			$document.on('mouseup', mouseup);
		});

		function mousemove(event) {

			if ($attrs.resizer == 'vertical') {
				// Handle vertical resizer
				var x = event.pageX;

				if ($attrs.resizerMax && x > $attrs.resizerMax) {
					x = parseInt($attrs.resizerMax);
				}

				$element.css({
					left : x + 'px'
				});

				$($attrs.resizerLeft).css({
					width : x + 'px'
				});
				$($attrs.resizerRight).css({
					left : (x + parseInt($attrs.resizerWidth)) + 'px'
				});

			} else {
				// Handle horizontal resizer
				var y = window.innerHeight - event.pageY;

				$element.css({
					bottom : y + 'px'
				});

				$($attrs.resizerTop).css({
					bottom : (y + parseInt($attrs.resizerHeight)) + 'px'
				});
				$($attrs.resizerBottom).css({
					height : y + 'px'
				});
			}
		}

		function mouseup() {
			$document.unbind('mousemove', mousemove);
			$document.unbind('mouseup', mouseup);
		}
	};
});

// To be added to side bar to fix dynamically added items not expanding issue
App.directive('sideMenuFixer', function() {
	return function(scope, element, attrs) {
		if (scope.$last) {
			window.setTimeout(function() {
				$(".sidebar .treeview").tree();
			}, 500); // wait for a bit before adjusting (Hack alert o.O)
		}
	};
});

App.directive('richTextEditor', function() {
    return {
        restrict : "A",
        require : '?ngModel',
        replace : true,
        transclude : true,
        template : "<div><textarea id='some-textarea' placeholder='Enter text ...' " +
        		   "style='width:80%; height:60%'></textarea></div>",
        link : function(scope, element, attrs, ctrl) {
            var textarea = $('#some-textarea').wysihtml5();
            var editor = textarea.data('wysihtml5').editor;

            // view -> model
            editor.on('change', function() {
                scope.$apply(function() {
                    ctrl.$setViewValue(editor.getValue());
                });
            });
            
            // - similar to above
            scope.$watch(attrs.ngModel, function(newValue, oldValue) {
                textarea.html(newValue);
                editor.setValue(newValue);
            });
            
            // load init value from DOM
            //ctrl.$render();
        }
    };
});
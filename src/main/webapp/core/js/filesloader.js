var FilesLoader = function(loadTrunkCoreFiles) {

	// --------------- CSS START -----------------

	this.bootstrapstyles = [

	// Bootstrap
	"core/lib/bootstrap/css/bootstrap.min.css",

	// Font awesome
	"core/lib/uadmin/vendors/font-awesome/css/font-awesome.min.css",

	// Ionic icons
	"core/lib/ionicons/css/ionicons.min.css" ];

	this.uadminstyles = [

	"core/lib/uadmin/vendors/intro.js/introjs.css",

	"core/lib/uadmin/vendors/calendar/zabuto_calendar.min.css",

	"core/lib/uadmin/vendors/sco.message/sco.message.css",

	"core/lib/uadmin/vendors/animate.css/animate.css",

	"core/lib/uadmin/vendors/iCheck/skins/all.css",

	"core/lib/uadmin/vendors/jquery-notific8/jquery.notific8.min.css",

	"core/lib/uadmin/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.1.1.min.css",

	"core/lib/uadmin/vendors/bootstrap-markdown/css/bootstrap-markdown.min.css",

	// WysiHtml5
	"core/lib/uadmin/vendors/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css",

	"core/lib/uadmin/vendors/summernote/summernote.css",

	"core/lib/uadmin/vendors/ion.rangeSlider/css/ion.rangeSlider.css",

	"core/lib/uadmin/vendors/nouislider/jquery.nouislider.css",

	"core/lib/uadmin/vendors/jquery-nestable/nestable.css",

	"core/lib/uadmin/vendors/jquery-toastr/toastr.min.css",

	"core/lib/uadmin/vendors/jstree/dist/themes/default/style.min.css",

	"core/lib/uadmin/vendors/jquery-treetable/stylesheets/jquery.treetable.css",

	"core/lib/uadmin/vendors/jquery-treetable/stylesheets/jquery.treetable.theme.custom.css",

	"core/lib/uadmin/vendors/bootstrap-datepicker/css/datepicker.css",

	"core/lib/uadmin/vendors/fullcalendar/fullcalendar.css",

	"core/lib/uadmin/vendors/fullcalendar/fullcalendar.print.css",

	"core/lib/uadmin/vendors/lightbox/css/lightbox.css",

	"core/lib/uadmin/vendors/DataTables/media/css/jquery.dataTables.css",

	"core/lib/uadmin/vendors/DataTables/media/css/dataTables.bootstrap.css",

	"core/lib/uadmin/vendors/jquery-tablesorter/themes/blue/style-custom.css",

	"core/lib/uadmin/vendors/DataTables/media/css/dataTables.bootstrap.css",

	"core/lib/uadmin/vendors/bootstrap-datepicker/css/datepicker3.css",

	"core/lib/uadmin/vendors/dropzone/css/dropzone.css",

	"core/lib/uadmin/vendors/jquery-steps/css/jquery.steps.css",

	"core/lib/uadmin/vendors/bootstrap-colorpicker/css/colorpicker.css",

	"core/lib/uadmin/vendors/bootstrap-daterangepicker/daterangepicker-bs3.css",

	"core/lib/uadmin/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css",

	"core/lib/uadmin/vendors/bootstrap-timepicker/css/bootstrap-timepicker.min.css",

	"core/lib/uadmin/vendors/bootstrap-clockface/css/clockface.css",

	"core/lib/uadmin/vendors/bootstrap-switch/css/bootstrap-switch.css",
	
	// Social bootstrap
	
	"core/lib/uadmin/vendors/bootstrap-social/css/bootstrap-social.css",
	
	"core/lib/uadmin/css/style.css",

	"core/lib/uadmin/vendors/jplist/html/css/jplist-custom.css",

	"core/lib/uadmin/vendors/select2/select2-madmin.css",

	"core/lib/uadmin/vendors/bootstrap-select/bootstrap-select.min.css",

	"core/lib/uadmin/vendors/multi-select/css/multi-select-madmin.css",

	"core/lib/uadmin/vendors/x-editable/select2/lib/select2-madmin.css",

	"core/lib/uadmin/vendors/x-editable/bootstrap3-editable/css/bootstrap-editable.css",

	"core/lib/uadmin/vendors/x-editable/inputs-ext/address/address.css",

	"core/lib/ui-select/css/select.min.css" ];

	this.trunkmodulestyles = [

	"core/lib/loader/css/loading-bar.css",

	"core/lib/xeditable/css/xeditable.css",

	"core/lib/toastr/css/toaster.css",

	"core/lib/hint-tooltip/hint.min.css",

	"core/lib/ng-tags-input/css/ng-tags-input.min.css",

	"core/lib/ng-tags-input/css/ng-tags-input.min.css",

	"core/lib/ng-table/css/ng-table.min.css",

	"core/lib/nvd-3/css/nv.d3.min.css",

	"core/lib/ng-tags-input/css/ng-tags-input.bootstrap.css",

	"core/lib/datetimepicker/css/datetimepicker.css" ];

	this.trunkcorestyles = [

	"core/lib/uadmin/css/themes/style1/blue-grey.css",

	"core/lib/uadmin/css/style-responsive.css",

	"core/css/animations.css",

	"core/css/custom.css",

	"core/css/donttouch.css",

	"core/css/transparency.css",

	// "core/css/scrollbar.css"

	];

	// --------------- CSS END -----------------

	// --------------- JS START -----------------

	this.jquery = [

	"core/lib/jquery/jquery.min.js"

	];

	this.jqueryothers = [

	"core/lib/jquery/jquery.min.js",

	"core/lib/bootstrap/js/bootstrap.min.js",

	"core/lib/jquery/jquery-ui.min.js"

	];

	this.angularjs = [

	"core/lib/angular/angular.min.js"

	];

	this.angularjsothers = [

	"core/lib/angular/angular-route.min.js",

	"core/lib/angular/angular-animate.min.js",

	"core/lib/angular/angular-sanitize.min.js",

	"core/lib/angular/angular-cookies.min.js"

	];

	this.trunkmodulescripts = [

	// http://blog.stevenlevithan.com/archives/date-time-format
	"core/lib/dateformat/dateformat.js",

	"core/lib/momentjs/moment.js",
	
	// WysiHtml5
	"core/lib/uadmin/vendors/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js",
	
	// Angular facebook login
	"core/js/logins/angular-facebook.js",
	
	// Angular google + login
	"core/js/logins/google-plus-signin.js",
	
	// Angular linkedin login
	"core/js/logins/ngLinkedIn.js",
	
	// Jmpress
	"core/lib/jmpress/js/jmpress.js",

	// Toaster
	"core/lib/toastr/js/toaster.js",

	"core/lib/angular-bootstrap/ui/ui-bootstrap.min.js",

	"core/lib/uadmin/vendors/slimScroll/jquery.slimscroll.js",

	"core/lib/loader/js/loading-bar.js",

	"core/lib/xeditable/js/xeditable.js",

	"core/lib/ng-table/js/ng-table.min.js",

	"core/lib/ng-tags-input/js/ng-tags-input.min.js",

	"core/lib/ui-sortable/ui-sortable.js",

	// http://dalelotts.github.io/angular-bootstrap-datetimepicker/
	"core/lib/datetimepicker/js/datetimepicker.js",

	// ND3 charts
	"core/lib/d3/js/d3.min.js",

	"core/lib/nvd-3/js/nv.d3.min.js",

	"core/lib/nvd-3/js/angularjs-nvd3-directives.min.js",

	// https://github.com/ninjatronic/angular-base64
	"core/lib/angular-base64/lib/angular-base64.min.js",

	// Textangular
	"core/lib/textangular/js/textAngular-sanitize.min.js",

	"core/lib/textangular/js/textAngular.min.js",

	// UI-Select
	"core/lib/ui-select/js/select.min.js",

	// ScriptJS
	"core/lib/scriptjs/scriptjs.js",

	// http://blog.getelementsbyidea.com/load-a-module-on-demand-with-angularjs/
	// | https://github.com/ocombe/ocLazyLoad
	"core/lib/oclazyload/ocLazyLoad.js" ];

	this.trunkonlyfilters = loadTrunkCoreFiles ? [] : [];
	this.trunkonlyproviders = loadTrunkCoreFiles ? [] : [];
	this.trunkonlyservices = loadTrunkCoreFiles ? [ "core/js/services/trunkServices.js", ] : [];
	this.trunkonlydirectives = loadTrunkCoreFiles ? [ "core/js/directives/trunkDirectives.js", ] : [];
	this.trunkonlycontroller = loadTrunkCoreFiles ? [ "core/js/controllers/trunkControllers.js", ] : [];

	this.trunkcorescripts = [

	"core/js/app.js",

	"core/lib/uadmin/js/main.js",

	"core/js/classes/classes.js",

	"core/js/constants/constants.js",

	"core/js/filters/filters.js", ]

	.concat(this.trunkonlyfilters).concat([

	"core/js/services/services.js", ])

	.concat(this.trunkonlyservices).concat([

	"core/js/providers/providers.js", ])

	.concat(this.trunkonlyproviders).concat([

	"core/js/directives/directives.js",

	"core/js/directives/trunkOpenDirectives.js", ])

	.concat(this.trunkonlydirectives).concat([

	"core/js/controllers/controllers.js", ])

	.concat(this.trunkonlycontroller).concat([

	"core/js/modules/popup/popup.js",

	"core/js/modules/background/background.js",

	"core/js/modules/timezonepicker/picker.js",

	"core/js/modules/radialmenu/radialmenu.js",

	"core/js/modules/featuresettings/featuresettings.js"

	]);

	console.log(this.trunkcorescripts);

	// --------------- JS END -----------------

	this.loadAllCss = function() {
		var s = this;
		head.load([].concat(s.bootstrapstyles, s.uadminstyles, s.trunkmodulestyles, s.trunkcorestyles));
	};

	this.loadAllJs = function() {
		var s = this;

		head.load(s.jquery, function() {
			head.load(s.jqueryothers, function() {

				head.load(s.angularjs, function() {
					head.load(s.angularjsothers, function() {

						head.load(s.trunkmodulescripts, function() {
							head.load(s.trunkcorescripts, function() {

							});
						});
					});
				});
			});
		});
	};
};
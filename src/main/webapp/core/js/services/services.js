App.factory('HtmlUtils', function($sce, $sanitize, $base64) {
	var s = {};

	// TODO: Clean all 'url unsafe' characters. Not just +

	s.escape = function(html) {
		var cleaned = $sce.getTrustedHtml($sanitize(html));

		cleaned = $base64.encode(cleaned);
		cleaned = cleaned.replace(/\+/g, '!@@#');

		return cleaned;
	};

	s.unescape = function(html) {
		html = html ? html : '';
		var cleaned = html.replace(/!@@#/g, '+');

		try {
			cleaned = $base64.decode(cleaned);
		} catch (e) {
		}

		return cleaned;
	};

	s.escape_bak = function(html) {

		var cleaned = $sce.getTrustedHtml($sanitize(html));

		cleaned = cleaned.replace(/&#160;/g, '!#');
		cleaned = cleaned.replace(/&#34;/g, '!@#');
		cleaned = cleaned.replace(/"/g, '!@#');
		cleaned = cleaned.replace(/:/g, '!@@#');
		cleaned = cleaned.replace(/\[/g, '!@@@#');
		cleaned = cleaned.replace(/]/g, '!@@@@#');
		cleaned = cleaned.replace(/{/g, '!@@@@@#');
		cleaned = cleaned.replace(/}/g, '!@@@@@@#');

		return cleaned;
	};

	s.unescape_bak = function(html) {

		html = html.replace(/!#/g, '&nbsp;');
		html = html.replace(/!@#/g, '"');
		html = html.replace(/!@@#/g, ':');
		html = html.replace(/!@@@#/g, '[');
		html = html.replace(/!@@@@#/g, ']');
		html = html.replace(/!@@@@@#/g, '{');
		html = html.replace(/!@@@@@@#/g, '}');

		return html;
	};

	return s;
});

App.factory('Keyboard', function($document, $timeout, keyCodes) {
	var s = {};

	s.keyHandlers = {};

	$document.on("keydown", function(event) {
		var keyDown = s.keyHandlers[event.keyCode];
		if (keyDown) {
			event.preventDefault();
			$timeout(function() {
				keyDown.callback('down', event.keyCode);
			});
		}
	});

	$document.on("keyup", function(event) {
		var keyDown = s.keyHandlers[event.keyCode];
		if (keyDown) {
			event.preventDefault();
			$timeout(function() {
				keyDown.callback('up', event.keyCode);
			});
		}
	});

	s.on = function(keyName, callback) {
		var keyCode = keyCodes[keyName];
		s.keyHandlers[keyCode] = {
			callback : callback
		};
		return s;
	};

	return s;
});

App.factory('safeApply', [ function($rootScope) {
	return function($scope, fn) {
		var phase = $scope.$root.$$phase;
		if (phase == '$apply' || phase == '$digest') {
			if (fn) {
				$scope.$eval(fn);
			}
		} else {
			if (fn) {
				$scope.$apply(fn);
			} else {
				$scope.$apply();
			}
		}
	};
} ]);

App.factory('Session', function($location) {
	var s = {};

	s.loggedIn = false;
	s.branchUniqueName;

	s.storage = localStorage;

	s.setLoggedOut = function() {
		s.loggedIn = false;

		s.storage.setItem('loggedIn', s.loggedIn);
		s.storage.removeItem('loggedInUserDetails');

		$location.path('/login/' + s.branchUniqueName);
	};

	return s;
});

App.factory('ScriptLoader', function() {
	var s = {};

	s.loadScript = function(index, allscripts, callback) {
		if (index < allscripts.length) {
			$script(allscripts[index], function() {
				index = index + 1;
				s.loadScript(index, allscripts, callback);
			});
		} else {
			callback();
		}
	};
	return s;
});

/*
 * Handles loading of modules and UI using the module manifests
 */
App.factory('ModuleProvider', function(WebService, Session, $location, $rootScope, $templateCache, $http, toaster) {

	var s = {};

	s.sideMenus = [];
	s.sideMenusMap = {};
	s.uiPermissions = {};
	s.modFeatMaping = {};

	s.addedHrefPatterns = [];

	s.loggedInUserDetails = null;
	Session.loggedIn = false;

	s.storage = localStorage;

	s.getAvatarUri = function() {
		if (s.loggedInUserDetails && Session.loggedIn) {

			// TODO: Hard coded. Change.

			if (s.loggedInUserDetails.unique_name == 'evri') {
				if (s.loggedInUserDetails.first_name == 'Ashley') {
					return 'core/img/users/ashley.jpg';
				} else if (s.loggedInUserDetails.first_name == 'Justin') {
					return 'core/img/users/justin.jpg';
				}
			}

			return 'core/img/avatar.gif';
		} else {
			return 'core/img/avatar.gif';
		}
	};

	s.loadStatusFromStorage = function() {
		var modules = s.storage.getItem('modules');
		var loggedIn = s.storage.getItem('loggedIn');
		var loggedInUserDetails = s.storage.getItem('loggedInUserDetails');

		if (loggedIn && loggedIn == 'true') {
			Session.loggedIn = true;

			if (modules) { // Load UI from session storage
				s.processModules(angular.fromJson(modules));
			}

			if (loggedInUserDetails) {
				var details = angular.fromJson(loggedInUserDetails);
				s.loggedInUserDetails = details;
				Session.branchUniqueName = details.unique_name;
			}

		} else {
			Session.loggedIn = false;
		}
	};

	s.login = function(username, password, branchUniqueName) {

		if (!username || !password || !branchUniqueName) {
			return;
		}

		var params = {
			"username" : username,
			"password" : password,
			"branchUniqueName" : branchUniqueName
		};

		WebService.invokePOSTRequest('authentication/login', params, function(data) {

			$location.path('/dashboard');
			Session.loggedIn = true;

			Session.branchUniqueName = branchUniqueName;
			s.storage.setItem('loggedIn', Session.loggedIn);
			s.storage.setItem('modules', angular.toJson(data));

			s.processModules(angular.fromJson(data));

			s.fetchLoggedInUserDetails();
		}, function(data) {
			toaster.pop('error', "Could not login", "Could not login using the provided credentials");
		}, function() {
		}, true);
	};
	
	s.loginAlternative = function(name, email, branchUniqueName) {

		if (!name || !email || !branchUniqueName) {
			return;
		}

		//console.log(guid + firstName + lastName + branchUniqueName);
		
		var params = {
			"name" : name,
			"email" : email,
			"branchName" : branchUniqueName
	    };

		WebService.invokePOSTRequest("alternativeLogin/verify", params, function(data) {

			$location.path('/dashboard');
			Session.loggedIn = true;

			Session.branchUniqueName = branchUniqueName;
			s.storage.setItem('loggedIn', Session.loggedIn);
			s.storage.setItem('modules', angular.toJson(data));

			s.processModules(angular.fromJson(data));

			s.fetchLoggedInUserDetails();
		}, function(data) {
			toaster.pop('error', "Could not login", "Could not login using the provided credentials");
		}, function() {
		}, true);
	};

	s.fetchLoggedInUserDetails = function() {
		WebService.invokePOSTRequest('authentication/loggedInDetails', {}, function(data) {
			s.loggedInUserDetails = angular.fromJson(data);

			var bname = s.loggedInUserDetails.branch_name;
			if (bname.length > 15) {
				var pre = bname.substring(0, 6);
				var post = bname.substring(bname.length - 6, bname.length);

				bname = pre + "..." + post;
			}

			s.loggedInUserDetails.banch_name_short = bname;

			s.storage.setItem('loggedInUserDetails', angular.toJson(data));
		}, function(data) {
		});
	};

	s.logout = function() {
		WebService.invokePOSTRequest('authentication/logout', {}, function(data) {
			Session.setLoggedOut();
		}, function(data) {
		});
	};

	s.checkIfLoggedIn = function() {

		WebService.invokePOSTRequest('authentication/isLoggedIn', {}, function(data) {
			var serverLoggedIn = angular.fromJson(data);

			if (Session.loggedIn && !serverLoggedIn) {
				s.logout();
				toaster.pop('error', "Took too long", "Looks like your session has expired");
			}
		}, function(data) {
		});
	};

	/*
	 * Returns 'sideMenu' object based on id specified in the manifest
	 */
	s.getSideMenuItem = function(id) {
		return s.sideMenusMap[id];
	};

	s.userHasModulePermission = function(modId) {
		return modId in s.modFeatMaping;
	};

	s.userHasModuleFeaturePermission = function(modId, featId) {
		if (s.userHasModulePermission(modId)) {

			var found = false;
			angular.forEach(s.modFeatMaping[modId], function(feat) {
				if (!found) {
					if (feat == featId) {
						found = true;
					}
				}
			});

			return found;
		} else {
			return false;
		}
	};

	s.preLoadPartials = function(ui, sideMenu) {

		// Load the partials
		$http.get(sideMenu.template, {
			cache : $templateCache
		});

		// Load the JS files
		$script(ui.files, function() {
		});
	};

	/*
	 * Adds URLs in the manifest to route provider
	 */
	s.addRouteToProvider = function(ui, sideMenu) {
		if (sideMenu.href) {

			var when = (!sideMenu.hrefPattern || sideMenu.hrefPattern == "") ? sideMenu.href : sideMenu.hrefPattern;

			if (s.addedHrefPatterns.contains(when)) {
				return;
			} else {
				s.addedHrefPatterns.push(when);
			}

			// s.preLoadPartials(ui, sideMenu);

			App.register.routeProvider.when(when, {
				templateUrl : sideMenu.template,
				controller : sideMenu.controller,
				resolve : {
					load : function($q, $route, $rootScope, $ocLazyLoad) {

						/*
						 * Module as self contained angularJs module
						 * (https://github.com/ocombe/ocLazyLoad)
						 */
						if (ui.angularModuleName) {

							var deferred = $q.defer();

							head.load(ui.cssFiles);

							// User ScriptJS to resolve module definer file
							$script([ ui.angularModuleFile ], function() {

								var lazyloaded = $ocLazyLoad.load({
									name : ui.angularModuleName,
									files : ui.jsFiles
								});

								lazyloaded.then(function() {
									deferred.resolve();
								});
							});

							return deferred.promise;
						}

						// angularModuleFile

						// Module hooks into default trunk angularJs module
						else {
							var deferred = $q.defer();

							head.load(ui.cssFiles);

							// User ScriptJS to resolve external files
							$script(ui.jsFiles, function() {
								deferred.resolve();
							});

							return deferred.promise;
						}
					}
				}
			});

		}
	};

	/*
	 * Converts relative route URLs to ones with # for angularJs. Appends a node
	 * as a copy
	 */
	s.createHashUrl = function(itemWithUrl) {
		if (itemWithUrl.href) {
			itemWithUrl.hrefHash = '#' + itemWithUrl.href;
			itemWithUrl.collapse = true;
		} else {
			itemWithUrl.collapse = true;
		}
	};

	/*
	 * Processes sub menus in module manifest
	 */
	s.processSideMenu = function(ui, sideMenu) {
		sideMenu.hasSubItems = sideMenu.sublinks && sideMenu.sublinks.length > 0;

		if (sideMenu.hasSubItems) {

			s.createHashUrl(sideMenu);

			angular.forEach(sideMenu.sublinks, function(link) {
				s.createHashUrl(link);
				s.addRouteToProvider(ui, link);
			});
		} else {
			s.createHashUrl(sideMenu);
			s.addRouteToProvider(ui, sideMenu);
		}
	};

	/*
	 * Processes module manifest
	 */
	s.processModules = function(uiPermissions) {
		s.sideMenus = [];
		s.sideMenusMap = {};
		s.uiPermissions = uiPermissions;

		angular.forEach(uiPermissions, function(uiPermission) {
			angular.forEach(uiPermission.ui.sidemenus, function(sideMenu) {
				if (sideMenu.id) {
					// Adds mapping between id and sideMenu object
					s.sideMenusMap[sideMenu.id] = sideMenu;
				}

				s.processSideMenu(uiPermission.ui, sideMenu);

				s.sideMenus.push(sideMenu);
			});

			s.modFeatMaping[uiPermission.moduleId] = uiPermission.featureIds;
		});
	};

	s.loadStatusFromStorage();// Load logged in status from session storage
	s.checkIfLoggedIn();

	return s;
});

App.factory('Themes', function($rootScope) {

	var s = {};

	s.blue = "skin-blue";
	s.black = "skin-black";

	s.layout = "";
	s.theme = s.blue;

	if (!window.localStorage.getItem('theme')) {
		window.localStorage.setItem('theme', s.blue);
	}

	s.resetLayout = function() {
		s.theme = window.localStorage.getItem('theme');
		// s.layout = Session.isLoggedIn() ? "fixed" : "";
	};

	s.switchTheme = function() {
		window.localStorage.setItem('theme', s.theme == s.blue ? s.black : s.blue);
		s.resetLayout();
	};

	s.resetLayout();

	return s;
});

App.factory('Workflow', function($rootScope) {

	var s = {};

	s.stepId = -1;
	s.actionId = -1;
	s.workflowId = -1;
	s.workflowName = null;
	s.executingWorkflow = false;

	s.startWorkflow = function(id) {
		s.workflowId = id;
		s.executingWorkflow = true;
	};

	s.stopWorkflow = function() {
		s.stepId = -1;
		s.actionId = -1;
		s.workflowId = -1;
		s.workflowName = null;
		s.executingWorkflow = false;
	};

	s.setStepId = function(id) {
		s.stepId = id;
	};

	s.setActionId = function(id) {
		s.actionId = id;
	};

	s.setWorkflowName = function(name) {
		s.workflowName = name;
	};

	s.isExecutingWorkflow = function() {
		return s.executingWorkflow && s.workflowId != -1;
	};

	return s;
});

App.factory('WebService', function($rootScope, $http, $location, $filter, $window, $route, toaster, Workflow, Session) {

	var s = {};

	s.count = 0;
	s.loading = false;

	s.devurl = $window.webServiceDevUrl;
	s.deployedurl = $window.webServiceDeployedUrl;

	// Pick service URL based on development or deployed version
	s.url = ($location.absUrl() + "").startsWith('http://localhost:') ? s.devurl : s.deployedurl;

	s.errors = {};

	s.updateLoadingStatus = function() {
		s.loading = s.count <= 0 ? false : true;
	};

	s.increaseCount = function() {
		s.count = s.count + 1;
		s.updateLoadingStatus();
	};

	s.decreaseCount = function() {
		s.count = s.count > 0 ? s.count - 1 : s.count;
		s.updateLoadingStatus();
	};

	s.getStringFromParams = function(params) {
		var finalString = '';

		// In a work flow, so indicate to server
		if (Workflow.isExecutingWorkflow()) {
			params['trunkCurrentStepId'] = Workflow.stepId;
			params['trunkCurrentActionId'] = Workflow.actionId;
			params['trunkCurrentWorkflowId'] = Workflow.workflowId;
			params['trunkCurrentWorkflowName'] = Workflow.workflowName;

			var scope = params['$scope'];

			if (scope) {
				// Feature that invoked this request
				var featureIncId = scope.$parent['__id__'];
				params['trunkCurrentWorkflowInvokedFeature'] = featureIncId;
			}
		}

		// Add common request parameters here

		for ( var key in params) {
			if (!key.startsWith('$')) {
				finalString += key + '=' + s.getProcessedValue(params[key]) + '&';
			}
		}

		finalString = encodeURI(finalString);

		return finalString;
	};

	s.getProcessedValue = function(value) {

		// Uncomment to replace backslashes with spaces

		var finalString = value ? angular.toJson(value).replace(/\\/g, "") : value;

		if (value) {
			if (finalString.charAt(0) == "\"") {
				finalString = finalString.substring(1, finalString.length - 1);
			}
		}

		return finalString;
	};

	s.invokeRequest = function(params, successDelegate, failureDelegate, failureDelegate2, unauthenticated) {

		if (typeof unauthenticated == 'undefined') {
			unauthenticated = false;
		}

		s.increaseCount();

		$http(params)

		.success(function(data, status, headers, config) {
			s.decreaseCount();

			// Response as part of work flow action, so refresh view
			if (Workflow.isExecutingWorkflow()) {
				var wfc = headers('workFlowCompleted');
				var wfr = headers('workFlowActionSuccess');

				if (wfr == 'true' || wfr == 'false') {
					$rootScope.$broadcast('workFlowActionSuccess', wfr == 'true');
				}

				if (wfc == 'true' || wfc == 'false') {
					$rootScope.$broadcast('workFlowCompleted', wfc == 'true');
				}
			}

			successDelegate(data, status, headers, config);
		})

		.error(function(data, status, headers, config) {
			s.decreaseCount();

			var error = headers('errorMessage');
			var errorType = headers('errorMessageType');

			if (!unauthenticated) {

				if (!Session.loggedIn) {
					return;
				}

				if (status == 401)// Not logged in
				{
					Session.setLoggedOut();
					toaster.pop('error', "Uh oh", error);

					return;
				}
			}

			if (error) {

				if (error == "You're not logged in") {
					toaster.clear();
				}

				var showerror = true;
				if (s.errors[error]) {
					var lastinvoked = s.errors[error];

					var diff = new Date().getTime() - lastinvoked.getTime();

					if (diff < 2000) {
						showerror = false;
					} else {
						s.errors[error] = new Date();
					}
				} else {
					s.errors[error] = new Date();
				}

				if (showerror) {
					toaster.pop(errorType || 'error', "Uh oh", error);
				}

				if (failureDelegate2) {
					failureDelegate2(data, status, headers, config);
				}
			} else {
				failureDelegate(data, status, headers, config);
			}

			// Response as part of work flow action, so refresh view
			if (Workflow.isExecutingWorkflow()) {
				var wfr = headers('workFlowActionAlreadyDone');

				if (wfr == 'true' || wfr == 'false') {
					$rootScope.$broadcast('workFlowActionAlreadyDone', wfr == 'true');
				}
			}

		});
	};

	s.invokePOSTRequest = function(methodName, paramss, successDelegate, failureDelegate, failureDelegate2, heads, unauthenticated) {
		
		s.invokeRequest({
			method : "POST",
			withCredentials : true,
			url : s.url + methodName,
			data : s.getStringFromParams(paramss),
			headers : {
				'Content-Type' : "application/x-www-form-urlencoded"
			}
		}, successDelegate, failureDelegate, failureDelegate2, unauthenticated);
	};
	
	s.invokePOSTRequestFile = function(methodName, paramss, successDelegate, failureDelegate, failureDelegate2, unauthenticated) {	

		s.invokeRequest({
			method : "POST",
			withCredentials : true,
			url : s.url + methodName,
			data : paramss,
			headers: { 'Content-Type': undefined },
			transformRequest: function(data, headersGetterFunction) {
                return data;
			}
		}, successDelegate, failureDelegate, failureDelegate2, unauthenticated);
	};
	
	s.invokePOSTRequestObject = function(methodName, paramss, successDelegate, failureDelegate, failureDelegate2, unauthenticated) {
		
		s.invokeRequest({
			method : "POST",
			withCredentials : true,
			url : s.url + methodName,
			data : paramss,
			headers: { 'Content-Type': undefined },			
		}, successDelegate, failureDelegate, failureDelegate2, unauthenticated);		
	};

	s.invokeGETRequest = function(methodName, paramss, successDelegate, failureDelegate, failureDelegate2, unauthenticated, resType) {
		if (typeof resType == 'undefined') {
			resType = "text";
		}
		
		s.invokeRequest({
			method : "GET",
			withCredentials : true,
			responseType : resType,
			url : s.url + methodName + "?" + s.getStringFromParams(paramss)
		}, successDelegate, failureDelegate, failureDelegate2, unauthenticated);
	};

	s.get404Message = function(data, defaultMessage) {

		var startIndex = data.lastIndexOf('<h2>Error') + 14;
		if (startIndex <= 10) {
			return defaultMessage.capitalize();
		} else {
			var endIndex = data.lastIndexOf('</h2>');
			var errorMessage = data.substring(startIndex, endIndex);

			if (errorMessage == '<') {
				return defaultMessage.capitalize();
			}

			return errorMessage.capitalize();
		}
	};

	return s;
});
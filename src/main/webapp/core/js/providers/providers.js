﻿var AppProviders = angular.module('Trunk.providers', []);

AppProviders.provider('controller', function() {
	var s = {};

	s.$get = function() {
		return s;
	};

	return s;
});
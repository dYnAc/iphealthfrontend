App.controller('AppController', function($scope, $rootScope, $routeParams, $location) {
	$rootScope.style = 'style1';
	$rootScope.theme = 'pink-blue';

	$scope.data = {};
	$scope.effect = '';
	$scope.header = {
		form : false,
		chat : false,
		theme : false,
		footer : true,
		history : false,
		animation : '',
		boxed : '',
		layout_menu : '',
		theme_style : 'style1',
		header_topbar : 'static',
		menu_style : 'sidebar-default',
		layout_horizontal_menu : '',

		toggle : function(k) {
			switch (k) {
			case 'chat':
				$scope.header.chat = !$scope.header.chat;
				break;
			case 'form':
				$scope.header.form = !$scope.header.form;
				break;
			case 'sitebar':
				$scope.header.header_topbar = $scope.header.header_topbar ? '' : (($scope.header.layout_menu === '') ? 'sidebar-collapsed' : 'right-side-collapsed');
				break;
			case 'theme':
				$scope.header.theme = !$scope.header.theme;
				break;
			case 'history':
				$scope.header.history = !$scope.header.history;
				$scope.header.menu_style = $scope.header.history ? 'sidebar-collapsed' : 'sidebar-default';
				break;
			}
		}
	};

	$scope.$on('$routeChangeSuccess', function(event, current, previous) {
		$scope.header.animation = 'fadeInUp';
		setTimeout(function() {
			$scope.header.animation = '';
		}, 100);

		$scope.header.boxed = '';
		$scope.header.footer = true;

		$rootScope.style = 'style1';
		$rootScope.theme = 'pink-blue';
	});

	$scope.style_change = function() {
		$rootScope.style = $scope.header.theme_style;
	};

	$scope.theme_change = function(t) {
		$rootScope.theme = t;
	};

	$(window).scroll(function() {
		if ($(this).scrollTop() > 0) {
			$('.quick-sidebar').css('top', '0');
		} else {
			$('.quick-sidebar').css('top', '50px');
		}
	});
	$('.quick-sidebar > .header-quick-sidebar').slimScroll({
		"height" : $(window).height() - 50,
		'width' : '280px',
		"wheelStep" : 5
	});
	$('#news-ticker-close').click(function(e) {
		$('.news-ticker').remove();
	});
});

App.directive("ngAccordion", function($parse, $compile) {
	return {
		link : function($scope, element, attributes) {
			$scope._accordion = {
				status : [],
				collapse : {}
			};

			$scope._accordion.collapse = function(i) {
				for (var j = 0; j < $scope._accordion.status.length; j++) {
					if (i == j)
						continue;
					$scope._accordion.status[j] = true;
				}
				$scope._accordion.status[i] = !$scope._accordion.status[i];
			};

			$(">div", attributes.$$element).each(function(index, item) {
				$scope._accordion.status[index] = true;
				$(">.panel-heading>a", item).attr({
					'ng-click' : '_accordion.collapse(' + index + ')',
					'index' : index
				});
				$(">.panel-collapse", item).attr({
					'collapse' : '_accordion.status[' + index + ']',
					'index' : index
				});
			});

			element.html($compile(element.html())($scope));
		}
	};
});

App.directive("ngAnimation", function($parse, $compile) {
	return {
		link : function($scope, element, attributes) {
			$scope._animation_change = function(v) {
				$scope.header.effect = v;
			};

			attributes.$$element.find('button').each(function(index, value) {
				$(this).attr({
					'ng-click' : "_animation_change('" + $(this).attr('data-value') + "')"
				});
			});

			element.html($compile(element.html())($scope));
		}
	};
});

App.directive("ngMenu", function($parse, $compile) {
	return {
		link : function($scope, element, attributes) {

			$scope._menu = {
				status : [],
				collapse : {},
				hover : []
			};

			$scope._menu.mouseleave = function() {
				for (var j = 0; j < $scope._menu.hover.length; j++) {
					$scope._menu.hover[j] = '';
				}
			};

			$scope._menu.mouseover = function(i) {
				for (var j = 0; j < $scope._menu.hover.length; j++) {
					$scope._menu.hover[j] = '';
				}
				$scope._menu.hover[i] = 'nav-hover';
			};

			$scope._menu.collapse = function(i) {
				$scope._menu.status[i] = !$scope._menu.status[i];

				var current = attributes.$$element.find('a[index=' + i + ']');

				current.parent('li').addClass('active').siblings().removeClass('active').children('ul').each(function() {
					$scope._menu.status[$(this).attr('index')] = true;
				});

				if (current.hasClass('btn-fullscreen')) {
					if (!document.fullscreenElement && !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement) {
						if (document.documentElement.requestFullscreen) {
							document.documentElement.requestFullscreen();
						} else if (document.documentElement.msRequestFullscreen) {
							document.documentElement.msRequestFullscreen();
						} else if (document.documentElement.mozRequestFullScreen) {
							document.documentElement.mozRequestFullScreen();
						} else if (document.documentElement.webkitRequestFullscreen) {
							document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
						}
					} else {
						if (document.exitFullscreen) {
							document.exitFullscreen();
						} else if (document.msExitFullscreen) {
							document.msExitFullscreen();
						} else if (document.mozCancelFullScreen) {
							document.mozCancelFullScreen();
						} else if (document.webkitExitFullscreen) {
							document.webkitExitFullscreen();
						}
					}
				}

			};

			attributes.$$element.find('li').children('a').each(function(index, value) {
				$scope._menu.status[index] = true;
				$(this).attr({
					'ng-click' : '_menu.collapse(' + index + ')',
					'index' : index
				});
				$('>ul', $(this).parent('li')).attr({
					'collapse' : '_menu.status[' + index + ']',
					'index' : index
				});
			});

			$(">li", attributes.$$element).each(function(index, value) {
				$scope._menu.hover[index] = '';
				$(this).attr({
					'ng-mouseleave' : '_menu.mouseleave()',
					'ng-mouseover' : '_menu.mouseover(' + index + ')',
					'ng-class' : '_menu.hover[' + index + ']'
				});
			});

			element.html($compile(element.html())($scope));
		}
	};
});

App.directive("scrollSpy", function($window) {
	return {
		restrict : 'A',
		controller : function($scope) {
			$scope.spies = [];
			this.addSpy = function(spyObj) {
				$scope.spies.push(spyObj);
			};
		},
		link : function(scope, elem, attrs) {
			var spyElems;
			spyElems = [];

			scope.$watch('spies', function(spies) {
				var spy, _i, _len, _results;
				_results = [];

				for (_i = 0, _len = spies.length; _i < _len; _i++) {
					spy = spies[_i];

					if (spyElems[spy.id] === null) {
						_results.push(spyElems[spy.id] = elem.find('#' + spy.id));
					}
				}
				return _results;
			});

			$($window).scroll(function() {
				var highlightSpy, pos, spy, _i, _len, _ref;
				highlightSpy = null;
				_ref = scope.spies;

				// cycle through `spy` elements to find which to highlight
				for (_i = 0, _len = _ref.length; _i < _len; _i++) {
					spy = _ref[_i];
					spy.out();

					// catch case where a `spy` does not have an associated `id`
					// anchor
					if (spyElems === null || spyElems[spy.id] === null || spyElems[spy.id].offset() === undefined) {
						continue;
					}

					if ((pos = spyElems[spy.id].offset().top) - $window.scrollY <= 0) {
						// the window has been scrolled past the top of a spy
						// element
						spy.pos = pos;

						if (highlightSpy === null) {
							highlightSpy = spy;
						}
						if (highlightSpy.pos < spy.pos) {
							highlightSpy = spy;
						}
					}
				}

				// select the last `spy` if the scrollbar is at the bottom of
				// the page
				if ($(window).scrollTop() + $(window).height() >= $(document).height()) {
					spy.pos = pos;
					highlightSpy = spy;
				}

				return highlightSpy !== null ? highlightSpy["in"]() : void 0;
			});
		}
	};
});

App.directive('spy', function($location, $anchorScroll) {
	return {
		restrict : "A",
		require : "^scrollSpy",
		link : function(scope, elem, attrs, affix) {
			elem.click(function() {
				$location.hash(attrs.spy);
				$anchorScroll();
			});

			affix.addSpy({
				id : attrs.spy,
				'in' : function() {
					elem.addClass('active');
				},
				out : function() {
					elem.removeClass('active');
				}
			});
		}
	};
});